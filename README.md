<div align="center">
<img src="game/images/logos/banner_2.svg" width="500">
</div><br>

<div align="center" style="font-size:20px">
<p><a href="https://zitrica.itch.io/academia-de-detectives"><img src="game/images/logos/itch-io.svg" alt="Disponible en itch.io" width=200/></a></p>
</div><br>

<p><em>Has recibido una beca para estudiar en la Academia Doyle, el centro de
formación de detectives más prestigioso del mundo. De la mano de Irene Conan,
profesora y directora de la academia, realizarás un viaje a través de la
historia en el que adquirirás las herramientas necesarias para convertirte en
detective especialista en cinemática.</em></p>

<p><strong>Academia de detectives</strong> es una novela visual educativa
diseñada como complemento didáctico para la enseñanza y el aprendizaje de la
cinemática en la asignatura de Física y Química en 4º de ESO.</p>

<p>Creada por Juan José Rosa Cánovas como parte del Trabajo de Fin de Máster en
el <a href="https://masteres.ugr.es/profesorado/" target="_blank">MAES</a> de
la Universidad de Granada durante el curso 2023-2024.</p>


### Código fuente y licencias
<p><strong>Academia de detectives</strong> ha sido desarrollada con <code><a
href="https://github.com/renpy/renpy" target="_blank">Ren'Py</a></code> haciendo uso de
recursos audiovisuales con licencias <a
href="https://creativecommons.org/share-your-work/cclicenses/"
target="_blank">Creative Commons</a>&nbsp;y otras licencias permisivas. La
autoría de estos materiales se detalla en los créditos del videojuego.</p>

<p>El código fuente se distribuye de forma gratuita como software
libre con licencia <a href="https://www.gnu.org/licenses/gpl-3.0.html"
target="_blank">GPLv3</a> y puede consultarse en el <a
href="https://gitlab.com/zitrica/academia-detectives"
target="_blank">repositorio de GitLab de Zitrica</a> sin necesidad de
descargarlo.</p>


### Disponibilidad en plataformas
<p><strong>Academia de detectives</strong> puede jugarse en ordenadores con sistemas operativos GNU/Linux
y Windows.</p>


### Opciones de accesibilidad
<p> El videojuego cuenta con varias opciones de accesibilidad, entre las que se
encuentra la asistencia por voz.</p>

<p>En Windows se utiliza la API de Microsoft Speech. La configuración del
asistente de voz puede ser modificada en la sección "Texto a voz" de
"Reconocimiento de voz" en el panel de control. </p>

<p>En GNU/Linux es necesario tener instaladas las dependencias <code><a
href="https://github.com/pndurette/gTTS" target="_blank">gTTS</a></code> y
<code><a href="https://github.com/FFmpeg/FFmpeg"
target="_blank">ffplay</a></code>.</p>
