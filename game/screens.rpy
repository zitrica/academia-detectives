﻿################################################################################
## Inicialización
################################################################################

init offset = -1


################################################################################
## Estilos
################################################################################

style default:
    properties gui.text_properties()
    language gui.language

style input:
    properties gui.text_properties("input", accent=True)
    adjust_spacing False

style hyperlink_text:
    properties gui.text_properties("hyperlink", accent=True)
    hover_underline True

style gui_text:
    properties gui.text_properties("interface")


style button:
    properties gui.button_properties("button")

style button_text is gui_text:
    properties gui.text_properties("button")
    yalign 0.5


style label_text is gui_text:
    properties gui.text_properties("label", accent=True)

style prompt_text is gui_text:
    properties gui.text_properties("prompt")


style bar:
    ysize gui.bar_size
    left_bar Frame("gui/bar/left.png", gui.bar_borders, tile=gui.bar_tile)
    right_bar Frame("gui/bar/right.png", gui.bar_borders, tile=gui.bar_tile)

style vbar:
    xsize gui.bar_size
    top_bar Frame("gui/bar/top.png", gui.vbar_borders, tile=gui.bar_tile)
    bottom_bar Frame("gui/bar/bottom.png", gui.vbar_borders, tile=gui.bar_tile)

style scrollbar:
    ysize gui.scrollbar_size
    base_bar Frame("gui/scrollbar/horizontal_[prefix_]bar.png", gui.scrollbar_borders, tile=gui.scrollbar_tile)
    thumb Frame("gui/scrollbar/horizontal_[prefix_]thumb.png", gui.scrollbar_borders, tile=gui.scrollbar_tile)

style vscrollbar:
    xsize gui.scrollbar_size
    base_bar Frame("gui/scrollbar/vertical_[prefix_]bar.png", gui.vscrollbar_borders, tile=gui.scrollbar_tile)
    thumb Frame("gui/scrollbar/vertical_[prefix_]thumb.png", gui.vscrollbar_borders, tile=gui.scrollbar_tile)

style slider:
    ysize gui.slider_size
    base_bar Frame("gui/slider/horizontal_[prefix_]bar.png", gui.slider_borders, tile=gui.slider_tile)
    thumb "gui/slider/horizontal_[prefix_]thumb.png"

style vslider:
    xsize gui.slider_size
    base_bar Frame("gui/slider/vertical_[prefix_]bar.png", gui.vslider_borders, tile=gui.slider_tile)
    thumb "gui/slider/vertical_[prefix_]thumb.png"


style frame:
    padding gui.frame_borders.padding
    background Frame("gui/frame.png", gui.frame_borders, tile=gui.frame_tile)



################################################################################
## Pantallas internas del juego
################################################################################
# Pantalla para cambiar la letra
#

init python:
    global_font_name = "OpenSans-VariableFont_wdth,wght.ttf"
    def globalfont(f):
        return global_font_name

    config.font_transforms["globalfont"] = globalfont
    config.ftfont_scale["OpenSans-VariableFont_wdth,wght.ttf"] = .87

screen accessibility():
    zorder 10
    style_prefix "accessibility"
    modal True
    add "images/splash/bg_accessibility.png"

    vbox:
        ypos 150
        xmaximum 0.7
        ysize 30
        xalign 0.5

        label "{size=*1.5}{font=[global_font_name]} — Opciones de accesibilidad — {/font}{/size}"

    hbox:
        ypos 300

        vbox:
            xpos 300
            xmaximum 0.2

            label "{size=*1.25}{font=[global_font_name]} — Tipo de fuente — {/font}{/size}"

            textbutton "{font=[global_font_name]}Fuentes por defecto{/font}":
                action [Preference("font transform", None), SetField(persistent, "font_label", "default")]
                style_suffix "radio_button"
            textbutton "{font=[global_font_name]}Fuente global\nsin serifa{/font}":
                action [Preference("font transform", "globalfont"), SetField(persistent, "font_label", "global")]
                style_suffix "radio_button"

        vbox:
            xpos 400
            xsize 540

            label "{size=*1.25}{font=[global_font_name]} — Alto contraste — {/font}{/size}"
            textbutton "{font=[global_font_name]}Desactivar{/font}":
                action Preference("high contrast text", "disable")
                style_suffix "radio_button"
            textbutton "{font=[global_font_name]}Activar{/font}":
                action Preference("high contrast text", "enable")
                style_suffix "radio_button"

        vbox:
            xpos 300 
            xsize 540
            label "{size=*1.25}{font=[global_font_name]} — Asistencia por voz — {/font}{/size}" 
            textbutton "{font=[global_font_name]}Desactivar{/font}":
                action Preference("self voicing", "disable") alt "Asistencia por voz desactivar"
                style_suffix "radio_button"
            textbutton "{font=[global_font_name]}Activar{/font}":
                action Preference("self voicing", "enable") alt "Asistencia por voz activar"
                style_suffix "radio_button"

    vbox:
        ypos 600
        xpos 200
        xsize 1520
        label "Texto de ejemplo" xalign 0.5
        null height 2*gui.pref_spacing

        text "{font=OpenRing-Regular.ttf}La configuración que escojas puede ser modificada más adelante en el menú del juego.{/font}"
        null height 1.5*gui.pref_spacing
        text "{font=OpenRing-Regular.ttf}Esta fuente se utiliza para la interfaz y las carátulas.{/font}"
        null height 1.5*gui.pref_spacing
        text "{font=CourierPrime-Regular.ttf}Esta fuente se utiliza para los diálogos y las resoluciones de los casos.{/font}"

    vbox:
        ysize 120.0
        xalign 0.5
        ypos 940

        textbutton "Confirmar":
            text_idle_color "#ffffff"
            text_hover_color "#ffffff"
            action Return()

style accessibility_label is gui_label
style accessibility_label_text is gui_label_text
style accessibility_button is gui_button
style accessibility_button_text is gui_button_text

style accessibility_button is gui_button:
    properties gui.button_properties("accessibility_button")

style accessibility_button_text is gui_button_text:
    properties gui.text_properties("accessibility_button")
    idle_color "#FFE75F"
    hover_color "#FFE75F"
    hover_underline True
    selected_hover_underline False


# Pantalla personalizada para saltar splash screen.
screen skip_splashscreen:
    imagebutton:
        if renpy.variant("pc"):
            idle "images/splash/skip_button_pc.png"
        elif renpy.variant("mobile"):
            idle "images/splash/skip_button_mobile.png"
        xalign 0.98
        yalign 0.98
        action [Hide(), Jump("before_main_menu")]
        activate_sound("audio/light_switch.ogg")


## Pantalla de diálogo #########################################################
##
## La pantalla de diálogo muestra el diálogo al jugador. Acepta dos parámetros,
## 'who' y 'what', es decir, el nombre del personaje que habla y el texto que ha
## de ser mostrado respectivamente. (El parámetro 'who' puede ser 'None' si no
## se da ningún nombre.)
##
## Esta pantalla debe crear un texto visualizable con id "what" que Ren'Py usa
## para gestionar la visualización del texto. Puede crear también visualizables
## con id "who" y id "window" para aplicar propiedades de estilo.
##
## https://www.renpy.org/doc/html/screen_special.html#say

screen say(who, what):
    style_prefix "say"

    window:
        id "window"

        if who is not None:

            window:
                id "namebox"
                style "namebox"
                text who id "who"

        text what id "what"


    ## Si hay una imagen lateral, la muestra encima del texto. No la muestra en
    ## la variante de teléfono - no hay lugar.
    if not renpy.variant("small"):
        add SideImage() xalign 0.0 yalign 1.0


## Permite que el 'namebox' pueda ser estilizado en el objeto 'Character'.
init python:
    config.character_id_prefixes.append('namebox')

style window is default
style say_label is default
style say_dialogue is default
style say_thought is say_dialogue

style namebox is default
style namebox_label is say_label


style window:
    xalign 0.5
    xfill True
    yalign gui.textbox_yalign
    ysize gui.textbox_height

    background Image("gui/textbox.png", xalign=0.5, yalign=1.0)

style namebox:
    xpos gui.name_xpos
    xanchor gui.name_xalign
    xsize gui.namebox_width
    ypos gui.name_ypos
    ysize gui.namebox_height

    background Frame("gui/namebox.png", gui.namebox_borders, tile=gui.namebox_tile, xalign=gui.name_xalign)
    padding gui.namebox_borders.padding

style say_label:
    properties gui.text_properties("name", accent=True)
    xalign gui.name_xalign
    yalign 0.5

style say_dialogue:
    properties gui.text_properties("dialogue")

    xpos gui.dialogue_xpos
    xsize gui.dialogue_width
    ypos gui.dialogue_ypos

    adjust_spacing False

## Pantalla de introducción de texto ###########################################
##
## Pantalla usada para visualizar 'renpy.input'. El parámetro 'prompt' se usa
## para pasar el texto presentado.
##
## Esta pantalla debe crear un displayable 'input' con id "input" para aceptar
## diversos parámetros de entrada.
##
## https://www.renpy.org/doc/html/screen_special.html#input

screen input(prompt):
    style_prefix "input"

    if renpy.variant("mobile"):
        window yalign 0.0:
            vbox:
                xanchor gui.dialogue_text_xalign
                xpos gui.dialogue_xpos
                xsize gui.dialogue_width
                ypos gui.dialogue_ypos

                text prompt style "input_prompt"
                input id "input"
    else:
        window:
            vbox:
                xanchor gui.dialogue_text_xalign
                xpos gui.dialogue_xpos
                xsize gui.dialogue_width
                ypos gui.dialogue_ypos

                text prompt style "input_prompt"
                input id "input"


style input_prompt is default

style input_prompt:
    xalign gui.dialogue_text_xalign
    properties gui.text_properties("input_prompt")
    italic True

style input:
    xalign gui.dialogue_text_xalign
    xmaximum gui.dialogue_width


## Pantalla de menú ############################################################
##
## Esta pantallla presenta las opciones internas al juego de la sentencia
## 'menu'. El parámetro único, 'items', es una lista de objetos, cada uno los
## campos 'caption' y 'action'.
##
## https://www.renpy.org/doc/html/screen_special.html#choice

screen choice(items):
    style_prefix "choice"

    vbox:
        for i in items:
            textbutton i.caption action i.action


style choice_vbox is vbox
style choice_button is button
style choice_button_text is button_text

style choice_vbox:
    xalign 0.5
    ypos 405
    yanchor 0.5

    spacing gui.choice_spacing

style choice_button is default:
    properties gui.button_properties("choice_button")

style choice_button_text is default:
    properties gui.text_properties("choice_button")
    hover_underline False
    hover_color "#FFE75F"


## Pantalla de menú rápido #####################################################
##
## El menú rápido se presenta en el juego para ofrecer fácil acceso a los menus
## externos al juego.

screen quick_menu():

    ## Asegura que esto aparezca en la parte superior de otras pantallas.
    zorder 100

    if quick_menu:

        hbox:
            style_prefix "quick"

            xalign 0.5
            yalign 1.0

            textbutton _("Atrás") action Rollback()
            textbutton _("Historial") action ShowMenu('history')
            textbutton _("Saltar") action Skip() alternate Skip(fast=True, confirm=True)
            textbutton _("Auto") action Preference("auto-forward", "toggle")
            textbutton _("Guardar") action ShowMenu('save')
            textbutton _("Guardar R.") action QuickSave()
            textbutton _("Cargar R.") action QuickLoad()
            textbutton _("Prefs.") action ShowMenu('preferences')


## Este código asegura que la pantalla 'quick_menu' se muestra en el juego,
## mientras el jugador no haya escondido explícitamente la interfaz.
init python:
    config.overlay_screens.append("quick_menu")

default quick_menu = True

style quick_button is default
style quick_button_text is button_text

style quick_button:
    properties gui.button_properties("quick_button")

style quick_button_text:
    properties gui.text_properties("quick_button")
    hover_color "#ffffff"
    hover_underline True


################################################################################
## Principal y Pantalla de menu del juego.
################################################################################

## Pantalla de navegación ######################################################
##
## Esta pantalla está incluída en el menú principal y los menús del juego y
## ofrece navegación a los otros menús y al inicio del juego.

screen navigation():

    vbox:

        style_prefix "navigation"

        box_wrap True

        xpos 150
        yalign 0.5

        spacing gui.navigation_spacing

        if main_menu:

            textbutton _("Comenzar") xsize 268 action Start()

        else:

            textbutton _("Historial") xsize 268 action ShowMenu("history")

            textbutton _("Guardar") xsize 268 action ShowMenu("save")

        textbutton _("Cargar") xsize 268 action ShowMenu("load")

        textbutton _("Opciones") xsize 268 action ShowMenu("preferences")

        if _in_replay:

            textbutton _("Finaliza repetición") xsize 268 action EndReplay(confirm=True)

        elif not main_menu:

            textbutton _("Menú principal") xsize 268 action MainMenu()


        if renpy.variant("pc") or (renpy.variant("web") and not renpy.variant("mobile")):

            ## La ayuda no es necesaria ni relevante en dispositivos móviles.
            textbutton _("Ayuda") xsize 268 action ShowMenu("help")

        textbutton _("Créditos") xsize 268 action ShowMenu("about")

        if renpy.variant("pc"):

            ## El botón de salida está prohibido en iOS y no es necesario en
            ## Android y Web.
            textbutton _("Salir") xsize 268 action Quit(confirm=not main_menu)

        elif renpy.variant("mobile"):
            key "rollback" action Quit(confirm=True)


style navigation_button is gui_button
style navigation_button_text is gui_button_text

style navigation_button:
    size_group "navigation"
    properties gui.button_properties("navigation_button")

style navigation_button_text:
    properties gui.text_properties("navigation_button")


screen navigation_game():

    vbox:

        style_prefix "navigation_game"

        box_wrap True

        xpos 150
        yalign 0.5

        spacing gui.navigation_spacing

        if main_menu:

            textbutton _("Comenzar") xsize 268 action Start()

        else:

            textbutton _("Historial") xsize 268 action ShowMenu("history")

            textbutton _("Guardar") xsize 268 action ShowMenu("save")

        textbutton _("Cargar") xsize 268 action ShowMenu("load")

        textbutton _("Opciones") xsize 268 action ShowMenu("preferences")

        if _in_replay:

            textbutton _("Finaliza repetición") xsize 268 action EndReplay(confirm=True)

        elif not main_menu:

            if renpy.variant("mobile"):
                textbutton _("Menú\nprincipal") xsize 268 action MainMenu()
            else:
                textbutton _("Menú principal") xsize 268 action MainMenu()


        if renpy.variant("pc") or (renpy.variant("web") and not renpy.variant("mobile")):

            ## La ayuda no es necesaria ni relevante en dispositivos móviles.
            textbutton _("Ayuda") xsize 268 action ShowMenu("help")

        textbutton _("Créditos") xsize 268 action ShowMenu("about")

        if renpy.variant("pc"):

            ## El botón de salida está prohibido en iOS y no es necesario en
            ## Android y Web.
            textbutton _("Salir") xsize 268 action Quit(confirm=not main_menu)

        elif renpy.variant("mobile"):
            key "rollback" action Quit(confirm=True)


style navigation_game_button is gui_button
style navigation_game_button_text is gui_button_text

style navigation_game_button:
    size_group "navigation_game"
    properties gui.button_properties("navigation_game_button")

style navigation_game_button_text:
    properties gui.text_properties("navigation_game_button")
    idle_color "#ffffff"
    selected_color "#FFE75F"
    hover_color "#ffffff"
    hover_underline True
    selected_hover_underline False


## Pantalla del menú principal #################################################
##
## Usado para mostrar el menú principal cuando Ren'Py arranca.
##
## https://www.renpy.org/doc/html/screen_special.html#main-menu

screen main_menu():

    ## Esto asegura que cualquier otra pantalla de menu es remplazada.
    tag menu

    add gui.main_menu_background

    ## Este marco vacío oscurece el menu principal.
    frame:
        style "main_menu_frame"

    ## La sentencia 'use' incluye otra pantalla dentro de esta. El contenido
    ## real del menú principal está en la pantalla de navegación.
    use navigation

#    if gui.show_name:
#
#        vbox:
#            style "main_menu_vbox"
#
#            text "[config.name!t]":
#                style "main_menu_title"
#
#            text "[config.version]":
#                style "main_menu_version"


style main_menu_frame is empty
style main_menu_vbox is vbox
style main_menu_text is gui_text
style main_menu_title is main_menu_text
style main_menu_version is main_menu_text

style main_menu_frame:
    xsize 420
    yfill True

    background "gui/overlay/main_menu.png"

style main_menu_vbox:
    xalign 1.0
    xoffset -30
    xmaximum 1200
    yalign 1.0
    yoffset -30

style main_menu_text:
    properties gui.text_properties("main_menu", accent=True)

style main_menu_title:
    properties gui.text_properties("title")

style main_menu_version:
    properties gui.text_properties("version")


## Pantalla del menú del juego #################################################
##
## Esto distribuye la estructura de base del menú del juego. Es llamado con el
## título de la pantalla y presenta el fondo, el título y la navegación.
##
## El parámetro 'scroll' puede ser 'None', "viewport" o "vpgrid". Se usa esta
## pantalla con uno o más elementos, que son transcluídos (situados) en su
## interior.

screen game_menu(title, scroll=None, yinitial=0.0, spacing=0):

    style_prefix "game_menu"

    if main_menu:
        add gui.main_menu_background
    else:
        add gui.game_menu_background

    frame:
        style "game_menu_outer_frame"

        hbox:

            ## Reservar espacio para la sección de navegación.
            frame:
                style "game_menu_navigation_frame"

            frame:
                style "game_menu_content_frame"

                if scroll == "viewport":

                    viewport:
                        yinitial yinitial
                        scrollbars "vertical"
                        mousewheel True
                        draggable True
                        pagekeys True

                        side_yfill True

                        vbox:
                            spacing spacing

                            transclude

                elif scroll == "vpgrid":

                    vpgrid:
                        cols 1
                        yinitial yinitial

                        scrollbars "vertical"
                        mousewheel True
                        draggable True
                        pagekeys True

                        side_yfill True

                        spacing spacing

                        transclude

                else:

                    transclude

    use navigation_game

    textbutton _("Volver"):
        style "return_button"

        action Return()

    label title

    if main_menu:
        key "game_menu" action ShowMenu("main_menu")



style game_menu_outer_frame is empty
style game_menu_navigation_frame is empty
style game_menu_content_frame is empty
style game_menu_viewport is gui_viewport
style game_menu_side is gui_side
style game_menu_scrollbar is gui_vscrollbar

style game_menu_label is gui_label
style game_menu_label_text is gui_label_text

style return_button is navigation_button
style return_button_text is navigation_button_text

style game_menu_outer_frame:
    bottom_padding 45
    top_padding 180

    background "gui/overlay/game_menu.png"

style game_menu_navigation_frame:
    xsize 420
    yfill True

style game_menu_content_frame:
    left_margin 60
    right_margin 30
    top_margin 15

style game_menu_viewport:
    xsize 1380

style game_menu_vscrollbar:
    unscrollable gui.unscrollable

style game_menu_side:
    spacing 15

style game_menu_label:
    xpos 75
    ysize 180

style game_menu_label_text:
    size gui.title_text_size
    color gui.accent_color
    yalign 0.5

style return_button:
    xpos gui.navigation_xpos
    yalign 1.0
    yoffset -45

style return_button_text:
    properties gui.text_properties("return_button")
    idle_color "#ffffff"
    selected_color "#FFE75F"
    hover_color "#ffffff"
    hover_underline True
    selected_hover_underline False


## Pantalla 'acerca de' ########################################################
##
## Esta pantalla da información sobre los créditos y el copyright del juego y de
## Ren'Py.
##
## No hay nada especial en esta pantalla y por tanto sirve también como ejemplo
## de cómo hacer una pantalla personalizada.

style hyperlink_text:
    font gui.interface_text_font
    properties gui.text_properties("hyperlink", accent=True)
    hover_underline True

screen about():

    tag menu

    ## Esta sentencia 'use' incluye la pantalla 'game_menu' dentro de esta. El
    ## elemento 'vbox' se incluye entonces dentro del 'viewport' al interno de
    ## la pantalla 'game_menu'.
    use game_menu(_("Créditos"), scroll="viewport"):

        style_prefix "about"

        vbox:

            label "— [config.name!t] —\n" xalign 0.5
            text _("Versión [config.version!t].") xalign 0.5
            text _("Código fuente distribuido con licencia {a=}GPLv3{/a}. Disponible en {a=https://www.itch.io.org}Itch.io{/a} y {a=https://gitlab.com/zitrica}GitLab{/a}.\n") xalign 0.5

            text "\n\n"

            label "— Diseño, programación y guión —\n" xalign 0.5
            text _("Juan José Rosa Cánovas\n") xalign 0.5

            text "\n\n"

            label "— Personajes —\n" xalign 0.5
            text _("{a=https://www.deviantart.com/silverhyena/art/Girl-Sprite-413022118}{i}Irene Conan{/i}{/a}") xalign 0.5
            text _("{a=https://www.deviantart.com/silverhyena}SilverHyena{/a}, con licencia {a=https://creativecommons.org/licenses/by-nc-sa/4.0/}CC BY-NC-SA{/a}.\n") xalign 0.5

            text _("{a=https://www.deviantart.com/silverhyena/art/Guy-Sprite-412450628}{i}Lucas{/i}{/a}") xalign 0.5
            text _("{a=https://www.deviantart.com/silverhyena}SilverHyena{/a}, con licencia {a=https://creativecommons.org/licenses/by-nc-sa/4.0/}CC BY-NC-SA{/a}.\n") xalign 0.5

            text "\n\n"

            label "— Otras imágenes —\n" xalign 0.5

            text _("{a=https://unsplash.com/es/fotos/edificio-de-hormigon-marron-BS9w1QCkJys}{i}Entrada de la academia{/i}{/a}") xalign 0.5
            text _("{a=https://unsplash.com/es/@sidem0n}Ruijia Wang{/a}, con licencia {a=https://unsplash.com/es/licencia}Unsplash{/a}.\n") xalign 0.5

            text _("{a=https://unsplash.com/es/fotos/mesa-de-conferencia-de-madera-blanca-y-marron-vfRkE_9wuPo}{i}Aula de clase{/i}{/a}") xalign 0.5
            text _("{a=https://unsplash.com/es/@urusy}Hiroyoshi Urushima{/a}, con licencia {a=https://unsplash.com/es/licencia}Unsplash{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Leo-von-klenze-ideale-ansicht-der-akropolis-und-des-areopag-in-athen.jpg}{i}Ideale Ansicht der Akropolis und des Areopag in Athen{/i}{/a}") xalign 0.5
            text _("Leo von Klence") xalign 0.5
            text _("Fotografía de Bayerische Staatsgemäldesammlungen, con licencia {a=https://creativecommons.org/licenses/by-sa/4.0/}CC BY-SA{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Raffael_058.jpg}{i}Scuola di Atene{/i}{/a}") xalign 0.5
            text _("Raffaello Sanzio") xalign 0.5
            text _("Fotografía de Zenodot Verlagsgesellschaft mbH, con licencia {a=https://www.gnu.org/licenses/fdl-1.3.html}GNU FDL{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Aristotle_with_a_Bust_of_Homer_MET_DP319026.jpg}{i}Aristoteles bij de buste van Homerus{/i}{/a}") xalign 0.5
            text _("Rembrandt Harmenszoon van Rijn") xalign 0.5
            text _("Fotografía de Metropolitan Museum of Art, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://en.m.wikipedia.org/wiki/File:Philosophie-gr%C3%A8ce_Aristotle.jpg}{i}Aristotle (384-322 BC) Statue at the Aristotle University of Thessaloniki, Greece{/i}{/a}") xalign 0.5
            text _("Alvaro Marques Hijazo, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://es.wikipedia.org/wiki/Archivo:Portrait_Pope_Pius_IV.PNG}{i}Ritratto di Papa Pio IV{/i}{/a}") xalign 0.5
            text _("Círculo de Scipione Pulzone, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Andreas_Cellarius_-_Scenographia_Systematis_Copernicani.jpg}{i}Scenographia Systematis Copernicani{/i}{/a}") xalign 0.5
            text _("Andreas Cellarius, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Hackert,_Der_Ponte_a_Mare_in_Pisa_(Toskana),_1799.jpg}{i}The Ponte a Mare in Pisa{/i}{/a}") xalign 0.5
            text _("Jacob Philipp Hackert, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Galileo_Galilei,_1564-1642_RMG_BHC2699.tiff}{i}Galileo Galilei, 1564-1642{/i}{/a}") xalign 0.5
            text _("Francesco Porcia, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://pixabay.com/vectors/man-greek-old-greece-ancient-male-3299537/}{i}Old greek man{/i}{/a}") xalign 0.5
            text _("{a=https://pixabay.com/users/stephanie_curry-4814499/}Stephanie_Curry{/a}, con licencia {a=https://pixabay.com/service/license-summary/}Pixabay{/a}.\n") xalign 0.5

            text _("{a=https://www.svgrepo.com/svg/114699/parthenon}{i}Parthenon{/i}{/a}") xalign 0.5
            text _("{a=https://www.svgrepo.com/}SVG Repo{/a}, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://www.svgrepo.com/svg/490319/satellite}{i}Satellite SVG Vector{/i}{/a}") xalign 0.5
            text _("{a=https://www.svgrepo.com/author/IonutNeagu/}InoutNeagu{/a}, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://www.svgrepo.com/svg/127459/child-with-umbrella}{i}Child With Umbrella{/i}{/a}") xalign 0.5
            text _("{a=https://www.svgrepo.com/}SVG Repo{/a}, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://www.geograph.org.uk/photo/2692290}{i}Statue of Achilles, Hyde Park{/i}{/a}") xalign 0.5
            text _("Richard Westmacott") xalign 0.5
            text _("Fotografía de {a=https://www.geograph.org.uk/profile/32427}Paul Farmer{/a}, con licencia {a=https://creativecommons.org/licenses/by-sa/2.0/}CC BY-SA{/a}.\n") xalign 0.5

            text _("{a=https://artvee.com/dl/olympia-presenting-the-young-alexander-the-great-to-aristotle/}{i}Olympia Presenting The Young Alexander The Great To Aristotle{/i}{/a}") xalign 0.5
            text _("Gerard Hoet, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://upload.wikimedia.org/wikipedia/commons/6/6e/The_School_of_Aristotle.jpg}{i}The School of Aristotle{/i}{/a}") xalign 0.5
            text _("Gustav Adolph Spangenberg, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Putti_Fountain,_Pisa_Cathedral_(Duomo_di_Pisa)_(forefront),_The_Leaning_Tower_of_Pisa_(background),_Piazza_dei_Miracoli_(-Square_of_Miracles-)._Pisa,_Tuscany,_Central_Italy-2.jpg}{i} Angels Statue, Pisa Cathedral, The Leaning Tower of Pisa, Piazza dei Miracoli.{/i}{/a}") xalign 0.5
            text _("Fotografía de {a=http://mstyslav-chernov.com/}Mstyslav Chernov{/a}, con licencia {a=https://creativecommons.org/licenses/by-sa/3.0/deed.en}CC BY-SA 3.0{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Bertini_fresco_of_Galileo_Galilei_and_Doge_of_Venice.jpg}{i}Galileo Galilei showing the Doge of Venice how to use the telescope{/i}{/a}") xalign 0.5
            text _("Giuseppe Bertini, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://en.m.wikipedia.org/wiki/File:Galileo_Donato.jpg}{i}Pittura immaginaria che mostra Galileo Galilei esibendo il suo telescopio a Leonardo Donato{/i}{/a}") xalign 0.5
            text _("Henry-Julien Detouche, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Astronomy;_Galileo_with_his_telescope_in_the_Piazza_San_Marc_Wellcome_V0024831.jpg}{i}Galileo with his telescope in the Piazza San Marc{/i}{/a}") xalign 0.5
            text _("Fotografía por {a=https://wellcomecollection.org/}Wellcome Images{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Emblemata_1624.jpg}{i}Dutch telescope{/i}{/a}") xalign 0.5
            text _("Adriaen van de Venne, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://www.britannica.com/biography/Galileo-Galilei/Telescopic-discoveries#/media/1/224058/2916}{i}Two of Galileo's first telescopes; in the Museo Galileo, Florence{/i}{/a}") xalign 0.5
            text _("Fotografía de {a=https://www.britannica.com/}Encyclopædia Britannica{/a}, con términos de uso especificados {a=https://corporate.britannica.com/termsofuse.html}aquí{/a}.\n") xalign 0.5

            text _("{a=https://unsplash.com/photos/full-moon-covered-by-clouds-LryOyGjm1bo}{i}Dreamy night{/i}{/a}") xalign 0.5
            text _("{a=https://unsplash.com/@aronvisuals}Aaron Visuals{/a}, con licencia {a=https://unsplash.com/es/licencia}Unsplash{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Samuel_Palmer_-_Kornfeld_im_Mondenschein.jpg}{i}Kornfeld im Mondenschein{/i}{/a}") xalign 0.5
            text _("Samuel Palmer, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Heyden_Nil_Gizeh_Pyramids.jpg}{i}The River Nile With The Gizeh Pyramids{/i}{/a}") xalign 0.5
            text _("Otto Heyden, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}\n") xalign 0.5

            text _("{a=https://unsplash.com/photos/a-group-of-moon-phases-QaFhy59sAa4}{i}A group of moon phases{/i}{/a}") xalign 0.5
            text _("{a=https://unsplash.com/@jaedonar}Arnold Rodrigues{a}, con licencia {a=https://unsplash.com/es/licencia}Unsplash{/a}.\n") xalign 0.5

            text _("{a=https://unsplash.com/photos/person-standing-on-shore-7STm0zpYQxI}{i}Person standing on the shore{/i}{/a}") xalign 0.5
            text _("{a=https://unsplash.com/@moonboyz}Bill Fairs{a}, con licencia {a=https://unsplash.com/es/licencia}Unsplash{/a}.\n") xalign 0.5

            text _("{a=https://www.svgrepo.com/svg/307448/turtle-slow-slowly}{i}Turtle Slow Slowly{/i}{/a}") xalign 0.5
            text _("{a=https://www.svgrepo.com/}SVG Repo{/a}, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://www.svgrepo.com/svg/477399/ancient-greek-helmet-1}{i}Ancient Greek Helmet{/i}{/a}") xalign 0.5
            text _("{a=https://www.svgrepo.com/author/Icooon%20Mono/}Icooon Mono{/a}, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Galileo%27s_geometrical_and_military_compass_in_Putnam_Gallery,_2009-11-24.jpg}{i}Galileo's geometrical and military compass in Putnam Gallery{/i}{/a}") xalign 0.5
            text _("Fotografía por {a=http://ragesoss.com/}Sage Ross{/a}, con licencia {a=https://creativecommons.org/licenses/by-sa/4.0}CC BY-SA{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Slide_rule_scales_front.jpg}{i}Scales on the front of a Keuffel and Esser slide rule, model K&E 4181-3{/i}{/a}") xalign 0.5
            text _("Fotografía por {a=https://commons.wikimedia.org/wiki/User:ArnoldReinhold}ArnoldReinhold{/a}, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/w/index.php?search=galileo&title=Special:MediaSearch&go=Go&type=image}{i}Portrait of Galileo Galilei{/i}{/a}") xalign 0.5
            text _("Justus Sustermans, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Galileos_Dialogue_Title_Page.png}{i}Salviati, Sagredo y Simplicio{/i}{/a}") xalign 0.5
            text _("Stefan Della Bella, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Francesco_Guardi_-_The_Entrance_to_the_Arsenal_in_Venice_-_Google_Art_Project.jpg}{i}The Entrance to the Arsenal in Venice{/i}{/a}") xalign 0.5
            text _("Francesco Guardi, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://es.m.wikipedia.org/wiki/Archivo:F%C3%A9lix_Parra_-_Galileo_Demonstrating_the_New_Astronomical_Theories_at_the_University_of_Padua_-_Google_Art_Project.jpg}{i}Galileo en la Universidad de Padua demostrando las nuevas teorías astronómicas{/i}{/a}") xalign 0.5
            text _("Félix Parra, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Galileo_Galilei._Photograph_by_P._Pozzi._Wellcome_V0026426.jpg}{i}The Entrance to the Arsenal in Venice{/i}{/a}") xalign 0.5
            text _("Fotografía por P. Pozzi ({a=http://wellcomeimages.org/}Wellcome Images{/a}), con licencia {a=https://creativecommons.org/licenses/by/4.0}CC BY{/a}.\n") xalign 0.5

            text _("{a=https://www.flickr.com/photos/vin60/6058583612}{i}Mont Saint-Michel en basse mer{/i}{/a}") xalign 0.5
            text _("{a=https://www.flickr.com/photos/vin60/}Vin on the move{/a}, con licencia {a=https://creativecommons.org/licenses/by-nc-nd/2.0/}CC BY-NC-ND 2.0{/a}.\n") xalign 0.5

            text _("{a=https://www.flickr.com/photos/vin60/6058578760/}{i}Mont Saint-Michel en pleine mer{/i}{/a}") xalign 0.5
            text _("{a=https://www.flickr.com/photos/vin60/}Vin on the move{/a}, con licencia {a=https://creativecommons.org/licenses/by-nc-nd/2.0/}CC BY-NC-ND 2.0{/a}.\n") xalign 0.5

            text _("{a=https://unsplash.com/photos/full-moon-on-cloudy-sky-during-nighttime-iHPi43-zVDk}{i}Full moon on cloudy sky during nighttime{/i}{/a}") xalign 0.5
            text _("{a=https://unsplash.com/@jeisblack}Jason Mavrommatis{/a}, con licencia {a=https://unsplash.com/es/licencia}Unsplash{/a}.\n") xalign 0.5

            text _("{a=https://www.svgrepo.com/svg/480769/earth-8}{i}Earth 8 SVG Vector{/i}{/a}") xalign 0.5
            text _("{a=https://www.svgrepo.com/author/Icooon%20Mono/}Icooon Mono{/a}, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://www.svgrepo.com/svg/383941/moon}{i}Moon SVG Vector{/i}{/a}") xalign 0.5
            text _("{a=https://www.svgrepo.com/author/wishforge.games/}wishforge.games{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n") xalign 0.5

            text _("{a=https://www.svgrepo.com/svg/477558/sun-2}{i}Sun 2 SVG Vector{/i}{/a}") xalign 0.5
            text _("{a=https://www.svgrepo.com/author/Icooon%20Mono/}Icooon Mono{/a}, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://vectorportal.com/es/vector/diploma/35147}{i}Stock Diploma Vector And Icon{/i}{/a}") xalign 0.5
            text _("{a=https://vectorportal.com}Vectorportal.com{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:VAB_and_SLS.jpg}{i}Kennedy Space Center{/i}{/a}") xalign 0.5
            text _("Kim Shiflett, con licencia de dominio público.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:190712-F-F3405-008.jpg}{i}{/i}A scale model of the Apollo three-man capsule{/a}") xalign 0.5
            text _("AEDC/PA, con licencia de dominio público.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:American_and_Russian_Chiefs_of_Staff_meet_in_the_Cecilienhof_Palace_in_Potsdam,_Germany,_Site_of_the_Potsdam..._-_NARA_-_198955.jpg}{i}American and Russian Chiefs of Staff meet in the Cecilienhof Palace in Potsdam{/i}{/a}") xalign 0.5
            text _("National Archives and Records Administration, con licencia de dominio público.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:President_Ronald_Reagan_and_Soviet_General_Secretary_Mikhail_Gorbachev_at_the_first_Summit_in_Geneva,_Switzerland.jpg}{i}President Ronald Reagan and Soviet General Secretary Mikhail Gorbachev at the first Summit in Geneva{/i}{/a}") xalign 0.5
            text _("White House Photographic Collection, con {a=https://www.nasa.gov/nasa-brand-center/images-and-media/}licencia de dominio público{/a}.\n") xalign 0.5

            text _("{a=https://www.nasa.gov/wp-content/uploads/2015/03/1958_january_explorer_01_team_0.jpg}{i}Explorer I Architects{/i}{/a}") xalign 0.5
            text _("NASA, con {a=https://www.nasa.gov/nasa-brand-center/images-and-media/}licencia de dominio público{/a}.\n") xalign 0.5

            text _("{a=https://www.nasa.gov/wp-content/uploads/2017/09/sputnik_preflight_1957_hires_0.jpg}{i}Technician making final preparations on Sputnik before launch.{/i}{/a}") xalign 0.5
            text _("NASA, con {a=https://www.nasa.gov/nasa-brand-center/images-and-media/}licencia de dominio público{/a}.\n") xalign 0.5

            text _("{a=https://wikimapia.org/7690844/Monument-to-PS-1-Sputnik-1-the-first-artificial-satellite-of-the-Earth#/photo/5048408}{i}Monument to PS-1 \"Sputnik-1\"{/i}{/a}") xalign 0.5
            text _("{a=https://wikimapia.org/#show=/user/1028093/}Photomaper{/a}, con licencia {a=https://creativecommons.org/licenses/by-sa/3.0/}CC BY-SA 3.0{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Explorer_I_02.jpg}{i}EXPLORER I briefing{/i}{/a}") xalign 0.5
            text _("US Army Aviation and Missile Command, con licencia de dominio público.\n") xalign 0.5

            text _("{a=https://images.nasa.gov/details-GRC-1958-C-48852}{i}NASA Lewis Research Center replaces the NACA Lewis Flight Propulsion Laboratory{/i}{/a}") xalign 0.5
            text _("NASA, con {a=https://www.nasa.gov/nasa-brand-center/images-and-media/}licencia de dominio público{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:The_Soviet_Union_1964_CPA_3089A_souvenir_sheet_(Soviet_Conquest_of_Space._1st_satellite,_1st_pennant_on_the_Moon,_1st_view_of_the_Moon_far_side,_1st_man_in_space,_1st_group_in_space,_1st_woman_in_space).png}{i}Soviet Conquest of Space{/i}{/a}") xalign 0.5
            text _("Post of the Soviet Union, con licencia de dominio público.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Yuri_Gagarin_with_awards_(2).jpg}{i}Yuri Gagarin with awards{/i}{/a}") xalign 0.5
            text _("CCCP, Mil.ru, con licencia {a=https://creativecommons.org/licenses/by/4.0/deed.en}CC BY{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:RIAN_archive_612748_Valentina_Tereshkova.jpg}{i}Valentina Tereshkova{/i}{/a}") xalign 0.5
            text _("Alexander Mokletsov, con licencia {a=https://creativecommons.org/licenses/by-sa/3.0/deed.en}CC-BY-SA 3.0{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Pioneer_IV_flight_spare_02.jpg}{i}Pioneer IV spacecraft{/i}{/a}") xalign 0.5
            text _("Eric Long, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/deed.en}CC0{/a}.\n") xalign 0.5

            text _("{a=https://www.nasa.gov/learning-resources/for-kids-and-students/who-was-alan-shepard-grades-k-4/}{i}Who was Alan Shepard?{/i}{/a}") xalign 0.5
            text _("NASA, con {a=https://www.nasa.gov/nasa-brand-center/images-and-media/}licencia de dominio público{/a}.\n") xalign 0.5

            text _("{a=https://www.nasa.gov/image-article/launch-of-sa-1-2/}{i}Launch of SA-1{/i}{/a}") xalign 0.5
            text _("NASA, con {a=https://www.nasa.gov/nasa-brand-center/images-and-media/}licencia de dominio público{/a}.\n") xalign 0.5

            text _("{a=https://www.nasa.gov/mission/apollo-8/}{i}The famous Earthrise photograph from Apollo 8{/i}{/a}") xalign 0.5
            text _("NASA, con {a=https://www.nasa.gov/nasa-brand-center/images-and-media/}licencia de dominio público{/a}.\n") xalign 0.5

            text _("{a=https://www.flickr.com/photos/iip-photo-archive/46713054544}{i}Apollo 11{/i}{/a}") xalign 0.5
            text _("NASA, con {a=https://www.nasa.gov/nasa-brand-center/images-and-media/}licencia de dominio público{/a}.\n") xalign 0.5

            text _("{a=https://www.nasa.gov/mission/apollo-11/}{i}Astronaut Buzz Aldrin{/i}{/a}") xalign 0.5
            text _("NASA, con {a=https://www.nasa.gov/nasa-brand-center/images-and-media/}licencia de dominio público{/a}.\n") xalign 0.5

            text _("{a=https://www.nasa.gov/directorates/somd/space-communications-navigation-program/antennas-of-the-dsn/}{i}34-meter Antenna{/i}{/a}") xalign 0.5
            text _("NASA, con {a=https://www.nasa.gov/nasa-brand-center/images-and-media/}licencia de dominio público{/a}.\n") xalign 0.5

            text _("{a=https://www.flickr.com/photos/nasacommons/26902590949}{i}Baja California and Mexico{/i}{/a}") xalign 0.5
            text _("NASA, con {a=https://www.nasa.gov/nasa-brand-center/images-and-media/}licencia de dominio público{/a}.\n") xalign 0.5

            text _("{a=https://www.esa.int/ESA_Multimedia/Images/2017/03/Geostationary_orbit}{i}Geostationary orbit{/i}{/a}") xalign 0.5
            text _("ESA/ID&Sense/ONiRiXEL, con licencia {a=https://creativecommons.org/licenses/by-sa/3.0/igo/}CC BY-SA 3.0 IGO{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:INTELSAT_I_(Early_Bird).jpg}{i}INTELSAT I (Early Bird){/i}{/a}") xalign 0.5
            text _("NASA, con {a=https://www.nasa.gov/nasa-brand-center/images-and-media/}licencia de dominio público{/a}.\n") xalign 0.5

            text _("{a=https://www.esa.int/Applications/Connectivity_and_Secure_Communications/Orbits}{i}EDRS-A in geostationary orbit{/i}{/a}") xalign 0.5
            text _("ESA, con {a=https://www.esa.int/ESA_Multimedia/Terms_and_conditions_of_use_of_images_and_videos_available_on_the_esa_website}licencia estándar ESA{/a}.\n") xalign 0.5

            text _("{a=https://commons.wikimedia.org/wiki/File:Indo-Pacific_biogeographic_region_map-en.svg}{i}Indo-Pacific biogeographic region{/i}{/a}") xalign 0.5
            text _("{a=https://commons.wikimedia.org/wiki/User:Sting}Eric Gaba{/a}, con diversas licencias {a=https://commons.wikimedia.org/wiki/File:Indo-Pacific_biogeographic_region_map-en.svg#Licensing}GNU y Creative Commons{/a}.\n") xalign 0.5

            text _("{a=https://www.nasa.gov/organizations/otps/nasa-study-provides-new-look-at-orbital-debris-potential-solutions/}{i}Simulation of orbital debris around Earth demonstrating the object population in the geosynchronous region{/i}{/a}") xalign 0.5
            text _("NASA, con {a=https://www.nasa.gov/nasa-brand-center/images-and-media/}licencia de dominio público{/a}.\n") xalign 0.5

            text "\n\n"

            label "— Música y efectos de sonido —\n" xalign 0.5
            text _("{i}Moonlight hall{/i}") xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n") xalign 0.5

            text _("{i}Parisian{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Duet musette{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Mysterioso march{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}To the Ends{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Fairytale Waltz{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Midnight Tale{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Morning{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Suonatore di Luto{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Angevin 70{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}The Parting{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Smoking Gun{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Bittersweet{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}The Path of the Goblin King{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Devonshire Waltz Andante{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Thief in the Night{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Nightdreams{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Bossa Antigua{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Space X-plorers{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Late Night Radio{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}SCP-x2x (Unseen Presence){/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Screen Saver{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Half Mystery{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Leaving Home{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Send for the Horses{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Chill Wave{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Space Jazz{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Jellyfish in Space{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{i}Your Call{/i}")xalign 0.5
            text _("{a=https://incompetech.com}Kevin MacLeod{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{a=https://freesound.org/people/kevp888/sounds/690190/}{i}Crowd outdoor{/i}{/a}")xalign 0.5
            text _("{a=https://freesound.org/people/kevp888/}Kevin Lucen{/a}, con licencia {a=https://creativecommons.org/licenses/by/4.0/}CC BY{/a}.\n")xalign 0.5

            text _("{a=https://bigsoundbank.com/sound-2842-typewriter-key.html}{i}Typewriter key{/i}{/a}") xalign 0.5
            text _("{a=https://josephsardin.fr/}Joseph Sardin{/a}, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n")xalign 0.5

            text _("{a=https://freesound.org/people/nicholasdaryl/sounds/563355/}{i}Celesta Chime{/i}{/a}") xalign 0.5
            text _("{a=https://freesound.org/people/nicholasdaryl/}nicholasdaryl{/a}, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n")xalign 0.5

            text _("{a=https://pixabay.com/es/sound-effects/light-switch-106505/}{i}Light switch{/i}{/a}") xalign 0.5
            text _("{a=https://pixabay.com/es/users/pixabay-1/}Pixabay{/a}, con licencia {a=https://pixabay.com/service/license-summary/}Pixabay{/a}.\n") xalign 0.5

            text _("{a=https://pixabay.com/sound-effects/game-level-complete-143022/}{i}Game Level Complete{/i}{/a}") xalign 0.5
            text _("{a=https://pixabay.com/users/universfield-28281460/}Universfield{/a}, con licencia {a=https://pixabay.com/service/license-summary/}Pixabay{/a}.\n") xalign 0.5

            text _("{a=https://pixabay.com/es/sound-effects/spotlight-91359/}{i}Spotlight{/i}{/a}") xalign 0.5
            text _("{a=https://pixabay.com/es/users/pixabay-1/}Pixabay{/a}, con licencia {a=https://pixabay.com/service/license-summary/}Pixabay{/a}.\n") xalign 0.5

            text _("{a=https://freesound.org/people/Breviceps/sounds/458398/}{i}Balloon Pop / Christmas cracker / Confetti Cannon{/i}{/a}") xalign 0.5
            text _("{a=https://freesound.org/people/Breviceps/}Breviceps{/a}, con licencia {a=https://creativecommons.org/publicdomain/zero/1.0/}CC0{/a}.\n") xalign 0.5

            text _("{a=https://pixabay.com/es/sound-effects/success-fanfare-trumpets-6185/}{i}Success Fanfare Trumpets{/i}{/a}") xalign 0.5
            text _("{a=https://pixabay.com/es/users/pixabay-1/}Pixabay{/a}, con licencia {a=https://pixabay.com/service/license-summary/}Pixabay{/a}.\n") xalign 0.5

            text _("{a=https://pixabay.com/es/sound-effects/cheering-and-clapping-crowd-1-5995/}{i}Cheering and clapping crowd 1{/i}{/a}") xalign 0.5
            text _("{a=https://pixabay.com/es/users/pixabay-1/}Pixabay{/a}, con licencia {a=https://pixabay.com/service/license-summary/}Pixabay{/a}.\n") xalign 0.5

            text _("{a=https://pixabay.com/es/sound-effects/eilegeiya-111391/}{i}Eilegeiya{/i}{/a}") xalign 0.5
            text _("{a=https://pixabay.com/es/users/samuelfrancisjohnson-1207793/}SamuelFrancisJohnson{/a}, con licencia {a=https://pixabay.com/service/license-summary/}Pixabay{/a}.\n") xalign 0.5


            text "\n\n"

            label "— Referencias —\n" xalign 0.5

            text _("{i}{color=#FFE75F}Capítulo 1: Entre paradojas{/color}{/i}\n\n") xalign 0.5

            text _("· Aristóteles. (2016).  {i}Física{/i}. Gredos.\n") first_indent 100
            text _("· Solana-Dueso, J. (2008). Los filósofos griegos y sus escuelas. {i}Arbor{/i}, {i}184{/i}(731), 413–422. {a=https://doi.org/10.3989/arbor.2008.i731.192}https://doi.org/10.3989/arbor.2008.i731.192{/a}.\n") first_indent 100
            text "\n"

            text _("{i}{color=#FFE75F}Capítulo 2: Y, sin embargo, se mueve{/color}{/i}\n\n") xalign 0.5

            text _("· Azcárate, C. (2007). La nueva ciencia del movimiento de Galileo: Una génesis difícil. {i}Enseñanza de Las Ciencias. Revista de Investigación y Experiencias Didácticas{/i}, {i}2{/i}(3), 203–208. {a=https://doi.org/10.5565/rev/ensciencias.5350}https://doi.org/10.5565/rev/ensciencias.5350{/a}.\n") first_indent 100
            text _("· Bombal-Gordón, F. (2014). Galileo Galilei: Un hombre contra la oscuridad. {i}Revista de La Real Academia de Ciencias Exactas, Físicas y Naturales{/i}, {i}107{/i}(1), 55–78.\n") first_indent 100
            text _("· Drake, S. (1995). Galileo at work: His scientific biography. Dover Publ.\n") first_indent 100
            text _("· Galilei, G., Isnardi, T., & Villasante, J. S. R. (2003). {i}Diálogos acerca de dos nuevas ciencias{/i}. Losada.\n") first_indent 100
            text "\n"

            text _("{i}{color=#FFE75F}Capítulo 3: A la luna venidera{/color}{/i}\n\n") xalign 0.5

            text _("· Artola, R. (2019). {i}La carrera espacial: Del Sputnik al Apollo 11{/i}. Alianza Editorial.\n") first_indent 100
            text _("· ESA Space Debris Office. (2024). {i}ESA’s Annual Space Environment Report{/i}. European Space Agency [[ESA]. {a=https://www.sdo.esoc.esa.int/environment_report/Space_Environment_Report_latest.pdf}https://www.sdo.esoc.esa.int/environment_report/Space_Environment_Report_latest.pdf{/a}.\n") first_indent 100
            text _("· Gaston, K. J., Anderson, K., Shutler, J. D., Brewin, R. J., & Yan, X. (2023). Environmental impacts of increasing numbers of artificial space objects. {i}Frontiers in Ecology and the Environment{/i}, {i}21{/i}(6), 289–296. {a=https://doi.org/10.1002/fee.2624}https://doi.org/10.1002/fee.2624{/a}.\n") first_indent 100
            text _("· Lull-García, José. (2020). La astronomía en el antiguo Egipto. Universidad de València, Servicio de Publicaciones.\n") first_indent 100
            text _("· National Aeronautics and Space Administration [[NASA]. (n.d.a). {i}Global Fire Map{/i}. NASA Fire Information for Resource Management System. {a=https://firms.modaps.eosdis.nasa.gov/map}https://firms.modaps.eosdis.nasa.gov/map{/a}.\n") first_indent 100
            text _("· National Aeronautics and Space Administration [[NASA]. (n.d.b). {i}Where Do Old Satellites Go When They Die?{/i} NASA Place Place. {a=https://spaceplace.nasa.gov/spacecraft-graveyard}https://spaceplace.nasa.gov/spacecraft-graveyard{/a}.\n") first_indent 100
            text _("· National Geographic. (2022). {i}¿Qué fue la Guerra Fría?{/i} National Geographic. {a=https://www.nationalgeographic.es/historia/que-fue-la-guerra-fria}https://www.nationalgeographic.es/historia/que-fue-la-guerra-fria{/a}.\n") first_indent 100
            text _("· National Oceanic and Atmospheric Administration [[NOAA]. (n.d.). {i}How frequent are tides?{/i} National Ocean Service. {a=https://oceanservice.noaa.gov/facts/tidefrequency.html}https://oceanservice.noaa.gov/facts/tidefrequency.html{/a}.\n") first_indent 100
            text _("· Pratt, T., & Allnutt, J. E. (2020). {i}Satellite communications{/i} (3ª ed.). John Wiley & Sons, Inc.\n") first_indent 100

            text "\n\n"

            label "— Software —\n" xalign 0.5
            text _("Desarrollado con {a=https://www.renpy.org/}Ren'Py{/a} (v[renpy.version_only]).\n") xalign 0.5
            text _("{a=https://www.renpy.org/}Ren'Py{/a} es un motor de novela visual que contiene {i}software{/i} libre sujeto a diversas licencias, incluidas las licencias MIT y LGPL. La lista completa de {i}software{/i}, con enlaces al código fuente completo, puede encontrarse {a=https://www.renpy.org/doc/html/license.html}aquí{/a}.\n") xalign 0.5

            text "\n\n"
            text _("Un videojuego de\n") xalign 0.5
            add "logos/logo_zitrica.png" xalign 0.5

            ## 'gui.about' se ajusta habitualmente en 'options.rpy'.
            if gui.about:
                text "[gui.about!t]\n"



style about_label is gui_label
style about_label_text is gui_label_text
style about_text is gui_text

style about_label_text:
    size gui.label_text_size


## Pantallas de carga y grabación ##############################################
##
## Estas pantallas permiten al jugador grabar el juego y cargarlo de nuevo. Como
## comparten casi todos los elementos, ambas están implementadas en una tercera
## pantalla: 'file_slots'.
##
## https://www.renpy.org/doc/html/screen_special.html#save https://
## www.renpy.org/doc/html/screen_special.html#load

screen save():

    tag menu

    use file_slots(_("Guardar"))


screen load():

    tag menu

    use file_slots(_("Cargar"))


screen file_slots(title):

    default page_name_value = FilePageNameInputValue(pattern=_("Página {}"), auto=_("Grabación automática"), quick=_("Grabación rápida"))

    use game_menu(title):

        fixed:

            ## Esto asegura que 'input' recibe el evento 'enter' antes que otros
            ## botones.
            order_reverse True

            ## El nombre de la pagina, se puede editar haciendo clic en el
            ## botón.
            button:
                style "page_label"

                key_events True
                xalign 0.5
                action page_name_value.Toggle()

                input:
                    style "page_label_text"
                    value page_name_value

            ## La cuadrícula de huecos de guardado.
            grid gui.file_slot_cols gui.file_slot_rows:
                style_prefix "slot"

                xalign 0.5
                yalign 0.5

                spacing gui.slot_spacing

                for i in range(gui.file_slot_cols * gui.file_slot_rows):

                    $ slot = i + 1

                    button:
                        action FileAction(slot)

                        has vbox

                        add FileScreenshot(slot) xalign 0.5

                        text FileTime(slot, format=_("{#file_time}%A, %d de %B %Y, %H:%M"), empty=_("vacío")):
                            style "slot_time_text"

                        text FileSaveName(slot):
                            style "slot_name_text"

                        key "save_delete" action FileDelete(slot)

            ## Botones de acceso a otras páginas
            vbox:
                style_prefix "page"

                xalign 0.5
                yalign 1.0

                hbox:
                    xalign 0.5

                    spacing gui.page_spacing

                    textbutton _("<") action FilePagePrevious()

                    if config.has_autosave:
                        textbutton _("{#auto_page}A") action FilePage("auto")

                    if config.has_quicksave:
                        textbutton _("{#quick_page}R") action FilePage("quick")

                    ## range(1, 10) da los números del 1 al 9.
                    for page in range(1, 10):
                        textbutton "[page]" action FilePage(page)

                    textbutton _(">") action FilePageNext()

#                if config.has_sync:
#                    if CurrentScreenName() == "save":
#                        textbutton _("Subir Sync"):
#                            action UploadSync()
#                            xalign 0.5
#                    else:
#                        textbutton _("Descargar Sync"):
#                            action DownloadSync()
#                            xalign 0.5


style page_label is gui_label
style page_label_text is gui_label_text
style page_button is gui_button
style page_button_text is gui_button_text

style slot_button is gui_button
style slot_button_text is gui_button_text
style slot_time_text is slot_button_text
style slot_name_text is slot_button_text

style page_label:
    xpadding 75
    ypadding 5

style page_label_text:
    textalign 0.5
    layout "subtitle"
    hover_color gui.hover_color

style page_button:
    properties gui.button_properties("page_button")

style page_button_text:
    properties gui.text_properties("page_button")
    idle_color "#ffffff"
    selected_color "#FFE75F"
    hover_color "#ffffff"
    hover_underline True
    selected_hover_underline False

style slot_button:
    properties gui.button_properties("slot_button")

style slot_button_text:
    properties gui.text_properties("slot_button")
    idle_color "#ffffff"
    selected_color "#FFE75F"
    hover_color "#ffffff"
    hover_underline True
    selected_hover_underline False


## Pantalla de preferencias ####################################################
##
## La pantalla de preferencias permite al jugador configurar el juego a su
## gusto.
##
## https://www.renpy.org/doc/html/screen_special.html#preferences

screen preferences():

    tag menu

    use game_menu(_("Opciones"), scroll="viewport"):

        vbox:

            hbox:
                if renpy.variant("pc") or renpy.variant("web"):

                    vbox:
                        style_prefix "radio"
                        label _("Pantalla")
                        textbutton _("Ventana") action Preference("display", "window")
                        textbutton _("Pantalla\ncompleta") action Preference("display", "fullscreen")

                vbox:
                    style_prefix "check"
                    label _("Saltar")
                    textbutton _("Texto no visto") action Preference("skip", "toggle")
                    textbutton _("Tras elecciones") action Preference("after choices", "toggle")
                    textbutton _("Transiciones") action InvertSelected(Preference("transitions", "toggle"))

                ## Aquí se pueden añadir 'vboxes' adicionales del tipo
                ## "radio_pref" o "check_pref" para nuevas preferencias.

            null height (4 * gui.pref_spacing)

            hbox:
                style_prefix "slider"
                box_wrap True

                vbox:

                    label _("Veloc. texto")

                    bar value Preference("text speed")

                    label _("Veloc. autoavance")

                    bar value Preference("auto-forward time")

                vbox:

                    if config.has_music:
                        label _("Volumen música")

                        hbox:
                            bar value Preference("music volume")

                    if config.has_sound:

                        label _("Volumen sonido")

                        hbox:
                            bar value Preference("sound volume")

                            if config.sample_sound:
                                textbutton _("Prueba") action Play("sound", config.sample_sound)

                    if config.has_music or config.has_sound or config.has_voice:
                        null height gui.pref_spacing

                        textbutton _("Silenciar todo"):
                            action Preference("all mute", "toggle")
                            style "mute_all_button"

            null height (4 * gui.pref_spacing)

        vbox:
            label "{size=*1.2}{noalt}Accesibilidad{/noalt}{/size}"
            null height  (2 * gui.pref_spacing)

            hbox:
                style_prefix "slider"
                box_wrap True

                vbox:
                    label "Tipo de fuente"

                    null height 10

                    textbutton "Fuentes por defecto":
                        action [Preference("font transform", None), SetField(persistent, "font_label", "default")]
                        style_suffix "radio_button"
                    textbutton _("Fuente global sin serifa"):
                        action [Preference("font transform", "globalfont"), SetField(persistent, "font_label", "global")]
                        style_suffix "radio_button"

                    null height 10

                vbox:
                    label "Alto contraste"

                    null height 10

                    textbutton _("Enable"):
                        action Preference("high contrast text", "enable")
                        style_suffix "radio_button"

                    textbutton _("Disable"):
                        action Preference("high contrast text", "disable")
                        style_suffix "radio_button"

            null height (2 * gui.pref_spacing)

            hbox:
                style_prefix "slider"
                box_wrap True

                vbox:
                    label _("Text Size Scaling")
                    null height 10

                    bar value Preference("font size")

                    textbutton _("Reset"):
                        alt "Reiniciar el tamaño del texto"
                        action Preference("font size", 1.0)
                        text_selected_idle_color gui.accent_color
                        text_selected_hover_color gui.accent_color
                        text_idle_color "#ffffff"
                        text_hover_color "#ffffff"
                        text_hover_underline True
                        text_selected_hover_underline False

                    null height 10

                vbox:
                    label _("Line Spacing Scaling")
                    null height 10

                    bar value Preference("font line spacing")

                    textbutton _("Reset"):
                        alt "Reiniciar el espacio de línea"
                        action Preference("font line spacing", 1.0)
                        text_selected_idle_color gui.accent_color
                        text_selected_hover_color gui.accent_color
                        text_idle_color "#ffffff"
                        text_hover_color "#ffffff"
                        text_hover_underline True
                        text_selected_hover_underline False

                    null height 10

            null height (2 * gui.pref_spacing)


            vbox:
                label "{noalt}Asistencia por voz{/noalt}"

                null height 10

                textbutton "Desactivar":
                    action Preference("self voicing", "disable") alt "Asistencia por voz desactivar"
                    style_suffix "radio_button"

                textbutton "Activar":
                    action Preference("self voicing", "enable") alt "Asistencia por voz activar"
                    style_suffix "radio_button"

                null height 10
        
            hbox:
                style_prefix "slider"
                box_wrap True

                vbox:
                    label _("Voice Volume")

                    null height 10

                    bar value Preference("voice volume")

                    null height 10

                vbox:
                    label _("Caída de volumen de voz")

                    null height 10

                    bar value Preference("self voicing volume drop")

            vbox:
                null height  (2 * gui.pref_spacing)
                text _("{font=OpenRing-Regular.ttf}Parte de las opciones de esta sección están destinadas a mejorar la accesibilidad.\nPara utilizar la asistencia por voz, es necesario satisfacer los requerimientos del sistema operativo.\nEn la medida de lo posible, se recomienda mantener valores de tamaño y espaciado del texto similares a los originales.{/font}")


style pref_label is gui_label
style pref_label_text is gui_label_text
style pref_vbox is vbox

style radio_label is pref_label
style radio_label_text is pref_label_text
style radio_button is navigation_game_button
style radio_button_text is navigation_game_button_text
style radio_vbox is pref_vbox

style check_label is pref_label
style check_label_text is pref_label_text
style check_button is navigation_game_button
style check_button_text is navigation_game_button_text
style check_vbox is pref_vbox

style slider_label is pref_label
style slider_label_text is pref_label_text
style slider_slider is gui_slider
style slider_button is gui_button
style slider_button_text is gui_button_text
style slider_pref_vbox is pref_vbox

style mute_all_button is check_button
style mute_all_button_text is check_button_text

style pref_label:
    top_margin gui.pref_spacing
    bottom_margin 3

style pref_label_text:
    yalign 1.0

style pref_vbox:
    xsize 338

style radio_vbox:
    spacing gui.pref_button_spacing

style radio_button:
    properties gui.button_properties("radio_button")
    foreground "gui/button/radio_[prefix_]foreground.png"

style radio_button_text:
    properties gui.text_properties("radio_button")

style check_vbox:
    spacing gui.pref_button_spacing

style check_button:
    properties gui.button_properties("check_button")
    foreground "gui/button/check_[prefix_]foreground.png"

style check_button_text:
    properties gui.text_properties("check_button")

style slider_slider:
    xsize 525

style slider_button:
    properties gui.button_properties("slider_button")
    yalign 0.5
    left_margin 15

style slider_button_text:
    properties gui.text_properties("slider_button")

style slider_vbox:
    xsize 675


## Pantalla de historial #######################################################
##
## Esta pantalla presenta el historial de diálogo al jugador, almacenado en
## '_history_list'.
##
## https://www.renpy.org/doc/html/history.html

screen history():

    tag menu

    ## Evita la predicción de esta pantalla, que podría ser demasiado grande.
    predict False

    use game_menu(_("Historial"), scroll=("vpgrid" if gui.history_height else "viewport"), yinitial=1.0, spacing=gui.history_spacing):

        style_prefix "history"

        for h in _history_list:

            window:

                ## Esto distribuye los elementos apropiadamente si
                ## 'history_height' es 'None'.
                has fixed:
                    yfit True

                if h.who:

                    label h.who:
                        style "history_name"
                        substitute False

                        ## Toma el color del texto 'who' de 'Character', si ha
                        ## sido establecido.
                        if "color" in h.who_args:
                            text_color h.who_args["color"]

                $ what = renpy.filter_text_tags(h.what, allow=gui.history_allow_tags)
                text what:
                    substitute False

        if not _history_list:
            label _("El historial está vacío.")


## Esto determina qué etiquetas se permiten en la pantalla de historial.

define gui.history_allow_tags = { "alt", "noalt", "rt", "rb", "art" }


style history_window is empty

style history_name is gui_label
style history_name_text is gui_label_text
style history_text is gui_text

style history_label is gui_label
style history_label_text is gui_label_text

style history_window:
    xfill True
    ysize gui.history_height

style history_name:
    xpos gui.history_name_xpos
    xanchor gui.history_name_xalign
    ypos gui.history_name_ypos
    xsize gui.history_name_width

style history_name_text:
    min_width gui.history_name_width
    textalign gui.history_name_xalign

style history_text:
    xpos gui.history_text_xpos
    ypos gui.history_text_ypos
    xanchor gui.history_text_xalign
    xsize gui.history_text_width
    min_width gui.history_text_width
    textalign gui.history_text_xalign
    layout ("subtitle" if gui.history_text_xalign else "tex")

style history_label:
    xfill True

style history_label_text:
    xalign 0.5


## Pantalla de ayuda ###########################################################
##
## Una pantalla que da información sobre el uso del teclado y el ratón. Usa
## otras pantallas con el contenido de la ayuda ('keyboard_help', 'mouse_help',
## y 'gamepad_help').

screen help():

    tag menu

    default device = "keyboard"

    use game_menu(_("Ayuda"), scroll="viewport"):

        style_prefix "help"

        vbox:
            spacing 23

            hbox:

                textbutton _("Teclado") action SetScreenVariable("device", "keyboard")
                textbutton _("Ratón") action SetScreenVariable("device", "mouse")

                if GamepadExists():
                    textbutton _("Mando") action SetScreenVariable("device", "gamepad")

            if device == "keyboard":
                use keyboard_help
            elif device == "mouse":
                use mouse_help
            elif device == "gamepad":
                use gamepad_help


screen keyboard_help():

    hbox:
        label _("Intro")
        text _("Avanza el diálogo y activa la interfaz.")

    hbox:
        label _("Espacio")
        text _("Avanza el diálogo sin seleccionar opciones.")

    hbox:
        label _("Teclas de flecha")
        text _("Navega la interfaz.")

    hbox:
        label _("Escape")
        text _("Accede al menú del juego.")

    hbox:
        label _("Ctrl")
        text _("Salta el diálogo mientras se presiona.")

    hbox:
        label _("Tabulador")
        text _("Activa/desactiva el salto de diálogo.")

    hbox:
        label _("Av. pág.")
        text _("Retrocede al diálogo anterior.")

    hbox:
        label _("Re. pág.")
        text _("Avanza hacia el diálogo siguiente.")

    hbox:
        label "H"
        text _("Oculta la interfaz.")

    hbox:
        label "S"
        text _("Captura la pantalla.")

    hbox:
        label "V"
        text _("Activa/desactiva la asistencia por {a=https://www.renpy.org/l/voicing}voz-automática{/a}.")


screen mouse_help():

    hbox:
        label _("Clic izquierdo")
        text _("Avanza el diálogo y activa la interfaz.")

    hbox:
        label _("Clic medio")
        text _("Oculta la interfaz.")

    hbox:
        label _("Clic derecho")
        text _("Accede al menú del juego.")

    hbox:
        label _("Rueda del ratón arriba")
        text _("Retrocede al diálogo anterior.")

    hbox:
        label _("Rueda del ratón abajo")
        text _("Avanza hacia el diálogo siguiente.")


screen gamepad_help():

    hbox:
        label _("Gatillo derecho\nA/Botón inferior")
        text _("Avanza el diálogo y activa la interfaz.")

    hbox:
        label _("Gatillo izquierdo\nBotón sup. frontal izq.")
        text _("Retrocede al diálogo anterior.")

    hbox:
        label _("Botón sup. frontal der.")
        text _("Avanza hacia el diálogo siguiente.")

    hbox:
        label _("D-Pad, Sticks")
        text _("Navega la interfaz.")

    hbox:
        label _("Inicio, Guía, B/Botón Derecho")
        text _("Accede al menú del juego.")

    hbox:
        label _("Y/Botón superior")
        text _("Oculta la interfaz.")

    textbutton _("Calibrar") action GamepadCalibrate()


style help_button is gui_button
style help_button_text is gui_button_text
style help_label is gui_label
style help_label_text is gui_label_text
style help_text is gui_text

style help_button:
    properties gui.button_properties("help_button")
    xmargin 12

style help_button_text:
    properties gui.text_properties("help_button")

style help_label:
    xsize 375
    right_padding 30

style help_label_text:
    size gui.text_size
    xalign 1.0
    textalign 1.0



################################################################################
## Pantallas adicionales
################################################################################

## Pantallas de carátulas #######################################################
# Capítulos
screen chapter_cover(chapter_number, chapter_title, chapter_desc):
    style_prefix "chapter_cover"
    add "images/covers/cv_background_chapters.png"

    vbox:
        ypos 104
        ysize 492
        xpos 166
        xsize 1589

        label "Capítulo [chapter_number]:" ypos 74 xalign 0.5
        label "{size=*0.74}[chapter_title]{/size}" xalign 0.5 ypos 30

    vbox:
        ypos 596
        ysize 484
        xsize 1477
        xpos 221

        label "{size=*0.33}[chapter_desc]{/size}":
            yalign 0.5
            xalign 0.5

style chapter_cover_label is gui_label
style chapter_cover_label_text is gui_label_text

style chapter_cover_label_text:
    properties gui.text_properties("gui_label")
    color gui.idle_color
    size 150
    textalign 0.5

# Trabajos de investigación
screen research_work_cover(first_block, second_block, third_block):
    style_prefix "research_work_cover"
    add "images/covers/cv_background_cases.png"

    vbox:
        ypos 65
        ysize 241
        xpos 0
        xsize 1920

        label "· Trabajo de investigación ·" yalign 0.5 xalign 0.5

    vbox:
        ypos 306
        ysize 209
        xpos 243
        xsize 1433

        label "{size=*0.5}[first_block]{/size}" yalign 0.5 xalign 0.5

    vbox:
        ypos 546
        ysize 229
        xpos 243
        xsize 1433

        label "{size=*0.5}[second_block]{/size}" yalign 0.5 xalign 0.5

    vbox:
        ypos 804
        ysize 209
        xpos 243
        xsize 1433

        label "{size=*0.5}[third_block]{/size}" yalign 0.5 xalign 0.5
#
#        label "{size=*0.33}[chapter_desc]{/size}":
#            yalign 0.5
#            xalign 0.5

style research_work_cover_label is gui_label
style research_work_cover_label_text is gui_label_text

style research_work_cover_label_text:
    properties gui.text_properties("gui_label")
    color "fffbb1"
    size 100
    textalign 0.5

# Casos (solo presentación)
screen case_cover(case_number, first_block, second_block):
    style_prefix "case_cover"
    add "images/covers/cv_background_cases.png"

    vbox:
        ypos 65
        ysize 241
        xpos 0
        xsize 1920

        label "· Caso [case_number] ·" yalign 0.5 xalign 0.5

    vbox:
        ypos 400
        ysize 209
        xpos 160
        xsize 1600

        label "{size=*0.6}[first_block]{/size}" yalign 0.5 xalign 0.5

    vbox:
        ypos 650
        ysize 229
        xpos 160
        xsize 1600

        label "{size=*0.6}[second_block]{/size}" yalign 0.5 xalign 0.5


style case_cover_label is gui_label
style case_cover_label_text is gui_label_text

style case_cover_label_text:
    properties gui.text_properties("gui_label")
    color "fffbb1"
    size 100
    textalign 0.5

## Pantalla de confirmación ####################################################
##
## Ren'Py llama la pantalla de confirmación para presentar al jugador preguntas
## de sí o no.
##
## https://www.renpy.org/doc/html/screen_special.html#confirm

screen confirm(message, yes_action, no_action):

    ## Asegura que otras pantallas no reciban entrada mientras se muestra esta
    ## pantalla.
    modal True

    zorder 200

    style_prefix "confirm"

    add "gui/overlay/confirm.png"

    frame:

        vbox:
            xalign .5
            yalign .5
            spacing 45

            label _(message):
                style "confirm_prompt"
                xalign 0.5

            hbox:
                xalign 0.5
                spacing 150

                textbutton _("Sí") action yes_action
                textbutton _("No") action no_action

    ## Clic derecho o escape responden "no".
    key "game_menu" action no_action


style confirm_frame is gui_frame
style confirm_prompt is gui_prompt
style confirm_prompt_text is gui_prompt_text
style confirm_button is gui_medium_button
style confirm_button_text is gui_medium_button_text

style confirm_frame:
    background Frame([ "gui/confirm_frame.png", "gui/frame.png"], gui.confirm_frame_borders, tile=gui.frame_tile)
    padding gui.confirm_frame_borders.padding
    xalign .5
    yalign .5

style confirm_prompt_text:
    textalign 0.5
    layout "subtitle"
    color "#FFE75F"

style confirm_button:
    properties gui.button_properties("confirm_button")


style confirm_button_text:
    properties gui.text_properties("confirm_button")
    idle_color "#ffffff"
    hover_color "#ffffff"
    hover_underline True
    selected_hover_underline False



## Pantalla del indicador de salto #############################################
##
## La pantalla de indicador de salto se muestra para indicar que se está
## realizando el salto.
##
## https://www.renpy.org/doc/html/screen_special.html#skip-indicator

screen skip_indicator():

    zorder 100
    style_prefix "skip"

    frame:

        hbox:
            spacing 9

            text _("Saltando")

            text "▸" at delayed_blink(0.0, 1.0) style "skip_triangle"
            text "▸" at delayed_blink(0.2, 1.0) style "skip_triangle"
            text "▸" at delayed_blink(0.4, 1.0) style "skip_triangle"


## Esta transformación provoca el parpadeo de las flechas una tras otra.
transform delayed_blink(delay, cycle):
    alpha .5

    pause delay

    block:
        linear .2 alpha 1.0
        pause .2
        linear .2 alpha 0.5
        pause (cycle - .4)
        repeat


style skip_frame is empty
style skip_text is gui_text
style skip_triangle is skip_text

style skip_frame:
    ypos gui.skip_ypos
    background Frame("gui/skip.png", gui.skip_frame_borders, tile=gui.frame_tile)
    padding gui.skip_frame_borders.padding

style skip_text:
    size gui.notify_text_size

style skip_triangle:
    ## Es necesario usar un tipo de letra que contenga el glifo BLACK RIGHT-
    ## POINTING SMALL TRIANGLE.
    font "DejaVuSans.ttf"


## Pantalla de notificación ####################################################
##
## La pantalla de notificación muestra al jugador un mensaje. (Por ejemplo, con
## un guardado rápido o una captura de pantalla.)
##
## https://www.renpy.org/doc/html/screen_special.html#notify-screen

screen notify(message):

    zorder 100
    style_prefix "notify"

    frame at notify_appear:
        text "[message!tq]"

    timer 3.25 action Hide('notify')


transform notify_appear:
    on show:
        alpha 0
        linear .25 alpha 1.0
    on hide:
        linear .5 alpha 0.0


style notify_frame is empty
style notify_text is gui_text

style notify_frame:
    ypos gui.notify_ypos

    background Frame("gui/notify.png", gui.notify_frame_borders, tile=gui.frame_tile)
    padding gui.notify_frame_borders.padding

style notify_text:
    properties gui.text_properties("notify")


## Pantalla NVL ################################################################
##
## Esta pantalla se usa para el diálogo y los menús en modo NVL.
##
## https://www.renpy.org/doc/html/screen_special.html#nvl


screen nvl(dialogue, items=None):

    window:
        style "nvl_window"

        has vbox:
            spacing gui.nvl_spacing

        ## Presenta el diálogo en una 'vpgrid' o una 'vbox'.
        if gui.nvl_height:

            vpgrid:
                cols 1
                yinitial 1.0

                use nvl_dialogue(dialogue)

        else:

            use nvl_dialogue(dialogue)

        ## Presenta el menú, si lo hay. El menú puede ser presentado
        ## incorrectamente si 'config.narrator_menu' está ajustado a 'True'.
        for i in items:

            textbutton i.caption:
                action i.action
                style "nvl_button"

    add SideImage() xalign 0.0 yalign 1.0


screen nvl_dialogue(dialogue):

    for d in dialogue:

        window:
            id d.window_id

            fixed:
                yfit gui.nvl_height is None

                if d.who is not None:

                    text d.who:
                        id d.who_id

                text d.what:
                    id d.what_id


## Esto controla el número máximo de entradas en modo NVL que pueden ser
## mostradas de una vez.
define config.nvl_list_length = gui.nvl_list_length

style nvl_window is default
style nvl_entry is default

style nvl_label is say_label
style nvl_dialogue is say_dialogue

style nvl_button is button
style nvl_button_text is button_text

style nvl_window:
    xfill True
    yfill True

    background "gui/nvl.png"
    padding gui.nvl_borders.padding

style nvl_entry:
    xfill True
    ysize gui.nvl_height

style nvl_label:
    xpos gui.nvl_name_xpos
    xanchor gui.nvl_name_xalign
    ypos gui.nvl_name_ypos
    yanchor 0.0
    xsize gui.nvl_name_width
    min_width gui.nvl_name_width
    textalign gui.nvl_name_xalign

style nvl_dialogue:
    xpos gui.nvl_text_xpos
    xanchor gui.nvl_text_xalign
    ypos gui.nvl_text_ypos
    xsize gui.nvl_text_width
    min_width gui.nvl_text_width
    textalign gui.nvl_text_xalign
    layout ("subtitle" if gui.nvl_text_xalign else "tex")

style nvl_thought:
    xpos gui.nvl_thought_xpos
    xanchor gui.nvl_thought_xalign
    ypos gui.nvl_thought_ypos
    xsize gui.nvl_thought_width
    min_width gui.nvl_thought_width
    textalign gui.nvl_thought_xalign
    layout ("subtitle" if gui.nvl_text_xalign else "tex")

style nvl_button:
    properties gui.button_properties("nvl_button")
    xpos gui.nvl_button_xpos
    xanchor gui.nvl_button_xalign

style nvl_button_text:
    properties gui.text_properties("nvl_button")


## Pantalla de globos ##########################################################
##
## La pantalla de burbujas se utiliza para mostrar el diálogo al jugador cuando
## se utilizan burbujas de diálogo. La pantalla de burbujas toma los mismos
## parámetros que la pantalla "say", debe crear un visualizable con el id de
## "what", y puede crear visualizables con los ids "namebox", "who", y "window".
##
## https://www.renpy.org/doc/html/bubble.html#bubble-screen

screen bubble(who, what):
    style_prefix "bubble"

    window:
        id "window"

        if who is not None:

            window:
                id "namebox"
                style "bubble_namebox"

                text who:
                    id "who"

        text what:
            id "what"

style bubble_window is empty
style bubble_namebox is empty
style bubble_who is default
style bubble_what is default

style bubble_window:
    xpadding 30
    top_padding 5
    bottom_padding 5

style bubble_namebox:
    xalign 0.5

style bubble_who:
    xalign 0.5
    textalign 0.5
    color "#000"

style bubble_what:
    align (0.5, 0.5)
    text_align 0.5
    layout "subtitle"
    color "#000"

define bubble.frame = Frame("gui/bubble.png", 55, 55, 55, 95)
define bubble.thoughtframe = Frame("gui/thoughtbubble.png", 55, 55, 55, 55)

define bubble.properties = {
    "bottom_left" : {
        "window_background" : Transform(bubble.frame, xzoom=1, yzoom=1),
        "window_bottom_padding" : 27,
    },

    "bottom_right" : {
        "window_background" : Transform(bubble.frame, xzoom=-1, yzoom=1),
        "window_bottom_padding" : 27,
    },

    "top_left" : {
        "window_background" : Transform(bubble.frame, xzoom=1, yzoom=-1),
        "window_top_padding" : 27,
    },

    "top_right" : {
        "window_background" : Transform(bubble.frame, xzoom=-1, yzoom=-1),
        "window_top_padding" : 27,
    },

    "thought" : {
        "window_background" : bubble.thoughtframe,
    }
}

define bubble.expand_area = {
    "bottom_left" : (0, 0, 0, 22),
    "bottom_right" : (0, 0, 0, 22),
    "top_left" : (0, 22, 0, 0),
    "top_right" : (0, 22, 0, 0),
    "thought" : (0, 0, 0, 0),
}


#################################
## Pantalla para casos
#################################
init python:
    class VerifyAnswer:
        def __init__(self, bg_case, case_number, true_answer):
            self.bg_case = bg_case
            self.case_number = case_number
            self.true_answer = true_answer

            if self.true_answer >= 0:
                self.true_answer_min = true_answer * 0.975
                self.true_answer_max = true_answer * 1.025
            else:
                self.true_answer_max = true_answer * 0.975
                self.true_answer_min = true_answer * 1.025

        def __call__(self):
            test_answer = renpy.invoke_in_new_context(renpy.call_screen,
                _screen_name="case_input", prompt="Escribe tu respuesta:",
                bg_case=self.bg_case,
                case_number=self.case_number,
                true_answer=self.true_answer)
            is_valid = False
            is_correct = False

            if test_answer == "force_return":
                pass

            else:
                while is_valid is False or is_correct is False:
                    if test_answer == "force_return":
                        break

                    try:
                        test_answer = test_answer.replace(",", ".").replace("'", ".")
                        test_answer = float(test_answer)
                        is_valid = True
                    except:
                        is_valid = False

                    if is_valid is False:
                        test_answer = renpy.invoke_in_new_context(renpy.call_screen,
                            _screen_name="case_input",
                            prompt="Por favor, escribe tu respuesta usando números decimales:",
                            bg_case=self.bg_case,
                            case_number=self.case_number,
                            true_answer=self.true_answer)
                    else:

                        if test_answer >= self.true_answer_min and test_answer <= self.true_answer_max:
                            is_correct = True
                        else:
                            is_correct = False

                        if is_correct is False:
                            test_answer = renpy.invoke_in_new_context(renpy.call_screen,
                                _screen_name="case_input",
                                prompt="La respuesta es incorrecta, inténtalo de nuevo:",
                                bg_case=self.bg_case,
                                case_number=self.case_number,
                                true_answer=self.true_answer)
                            is_valid = False
                            is_correct = False
                        else:
                            return True


screen case(bg_case, case_number, true_answer, case_desc):
    zorder 10
    style_prefix "case"
    modal True
    add  bg_case
    if renpy.variant("mobile"):
        add "images/covers/cv_case_template_mobile.png"
    else:
        add "images/covers/cv_case_template.png"

    text "":
        alt case_desc

    vbox:
        xsize 460.0
        ysize 120.0
        xalign 0.5
        ypos 24

        label _("{noalt}Caso [case_number]{/noalt}")

    vbox:
        xsize 306.0
        ysize 86.0
        xpos 1490
        if renpy.variant("mobile"):
            ypos 41
        else:
            ypos 980

        textbutton _("Resolver") action VerifyAnswer(bg_case, case_number, true_answer)


style case_label is gui_label
style case_label_text is gui_label_text
style case_button is gui_button
style case_button_text is gui_button_text

style case_label:
    properties gui.text_properties("case_label")
    xalign 0.5
    yalign 0.5

style case_label_text:
    properties gui.text_properties("case_label")

style case_button:
    properties gui.button_properties("case_button")
    xalign 0.5
    yalign 0.5

style case_button_text:
    properties gui.text_properties("case_button")
    idle_color "#ffffff"
    selected_color "#FFE75F"
    hover_color "#ffffff"
    hover_underline True
    selected_hover_underline False


screen case_input(prompt, bg_case, case_number, true_answer):
    zorder 10
    style_prefix "case_input"
    add bg_case 
    add "gui/overlay/answer.png"

    if renpy.variant("mobile"):
        add "images/covers/cv_case_template_mobile.png"
    else:
        add "images/covers/cv_case_template.png"

    modal True

    vbox:
        xsize 460.0
        ysize 120.0
        xalign 0.5
        ypos 24

        label _("{noalt}Caso [case_number]{/noalt}")

    vbox:
        xsize 306.0
        ysize 86.0
        xpos 1490
        if renpy.variant("mobile"):
            ypos 41
        else:
            ypos 980

        textbutton _("Volver") action [Return("force_return"), Show("case", bg_case=bg_case, case_number=case_number, true_answer=true_answer)]

    vbox:
        xanchor gui.dialogue_text_xalign
        xpos gui.dialogue_xpos
        xsize gui.dialogue_width
        if renpy.variant("mobile"):
            ypos 150
        else:
            ypos 400

        text "[prompt]\n" style "case_input_prompt"
        input id "case_input" length 32 xalign 0.5


style case_input_prompt is default

style case_input_prompt:
    properties gui.text_properties("case_input_prompt")
    italic True
    xalign 0.5

style input:
    xmaximum gui.dialogue_width

style case_input_label is case_label
style case_input_label_text is case_label_text
style case_input_button is case_button
style case_input_button_text is case_button_text




################################################################################
## Variantes móviles
################################################################################

style pref_vbox:
    variant "medium"
    xsize 675

## Ya que puede carecer de ratón, se reempleza el menú rápido con una versión
## con menos botones y más grandes, más fáciles de tocar.
screen quick_menu():
    variant "touch"

    zorder 100

    if quick_menu:
        key "rollback" action Quit(confirm=not main_menu)

        hbox:
            style_prefix "quick"

            xalign 0.5
            yalign 0.0

            textbutton _("Atrás") action Rollback()
            textbutton _("Saltar") action Skip() alternate Skip(fast=True, confirm=True)
            textbutton _("Auto") action Preference("auto-forward", "toggle")
            textbutton _("Menú") action ShowMenu()



style window:
    variant "small"
    background "gui/phone/textbox.png"

style radio_button:
    variant "small"
    foreground "gui/phone/button/radio_[prefix_]foreground.png"

style check_button:
    variant "small"
    foreground "gui/phone/button/check_[prefix_]foreground.png"

style nvl_window:
    variant "small"
    background "gui/phone/nvl.png"

style main_menu_frame:
    variant "small"
    background "gui/phone/overlay/main_menu.png"

style navigation_button_text:
    variant "small"
    size 70

style navigation_game_button_text:
    variant "small"
    size 70

style game_menu_outer_frame:
    variant "small"
    background "gui/phone/overlay/game_menu.png"

style game_menu_navigation_frame:
    variant "small"
    xsize 510

style game_menu_content_frame:
    variant "small"
    top_margin 0

style pref_vbox:
    variant "small"
    xsize 600

style bar:
    variant "small"
    ysize gui.bar_size
    left_bar Frame("gui/phone/bar/left.png", gui.bar_borders, tile=gui.bar_tile)
    right_bar Frame("gui/phone/bar/right.png", gui.bar_borders, tile=gui.bar_tile)

style vbar:
    variant "small"
    xsize gui.bar_size
    top_bar Frame("gui/phone/bar/top.png", gui.vbar_borders, tile=gui.bar_tile)
    bottom_bar Frame("gui/phone/bar/bottom.png", gui.vbar_borders, tile=gui.bar_tile)

style scrollbar:
    variant "small"
    ysize gui.scrollbar_size
    base_bar Frame("gui/phone/scrollbar/horizontal_[prefix_]bar.png", gui.scrollbar_borders, tile=gui.scrollbar_tile)
    thumb Frame("gui/phone/scrollbar/horizontal_[prefix_]thumb.png", gui.scrollbar_borders, tile=gui.scrollbar_tile)

style vscrollbar:
    variant "small"
    xsize gui.scrollbar_size
    base_bar Frame("gui/phone/scrollbar/vertical_[prefix_]bar.png", gui.vscrollbar_borders, tile=gui.scrollbar_tile)
    thumb Frame("gui/phone/scrollbar/vertical_[prefix_]thumb.png", gui.vscrollbar_borders, tile=gui.scrollbar_tile)

style slider:
    variant "small"
    ysize gui.slider_size
    base_bar Frame("gui/phone/slider/horizontal_[prefix_]bar.png", gui.slider_borders, tile=gui.slider_tile)
    thumb "gui/phone/slider/horizontal_[prefix_]thumb.png"

style vslider:
    variant "small"
    xsize gui.slider_size
    base_bar Frame("gui/phone/slider/vertical_[prefix_]bar.png", gui.vslider_borders, tile=gui.slider_tile)
    thumb "gui/phone/slider/vertical_[prefix_]thumb.png"

style about_text:
    variant "small"
    size 60

style about_label_text:
    variant "small"
    size 50

style slider_vbox:
    variant "small"
    xsize None

style slider_slider:
    variant "small"
    xsize 900

style confirm_prompt_text:
    variant "small"
    size 70

style confirm_button_text:
    variant "small"
    size 70
