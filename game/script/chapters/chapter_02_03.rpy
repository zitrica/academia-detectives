label chapter_02_03:
    stop music
    stop sound
    stop ambient
    scene black

    pause 2

    play music "audio/vertical_movement.ogg" volume 0.6

    pause 1

    scene bg_bertini-telescope at vertical_panning with dissolve

    professor "Tal y como os he dicho antes, aunque {b}Galileo{/b} hizo uso del {b}telescopio{/b} en sus investigaciones, este instrumento no fue invento suyo."
    professor2 "En mayo de 1609, {b}Galileo{/b} recibió una curiosa carta de su amigo {b}Paolo Sarpi{/b}."
    professor2 "En ella se hablaba sobre un instrumento aparecido en Holanda que permitía observar con claridad objetos desde la lejanía."
    professor2 "{b}Sarpi{/b} se refería, efectivamente, al {b}telescopio{/b}."

    pause 1
    scene black with dissolve
    pause 1
    scene bg_dutch-telescope with dissolve
    pause 1
    show ch_professor_smile at left_resize with dissolve
    pause 0.5

    professor2 "La identidad de la persona que lo inventó es materia de discusión incluso hoy en día."
    professor2 "En lo que sí hay consenso es en que la primera patente del instrumento fue solicitada por {b}Hans Lipperhey{/b}."
    professor2 "{b}Hans Lipperhey{/b}, que se ganaba la vida como fabricante de lentes, intentó registrar la patente en La Haya a finales de 1608."
    professor2 "Para desgracia de {b}Hans{/b}, su solicitud le fue denegada tras presentarse otras personas que demostraron haber fabricado instrumentos similares."

    pause 0.5
    hide ch_professor_smile with dissolve
    pause 1
    scene black with dissolve
    pause 1
    scene bg_telescope with dissolve
    pause 1

    professor2 "Para cuando {b}Galileo{/b} recibió la carta de {b}Sarpi{/b}, los ópticos franceses ya vendían el {b}telescopio{/b} en grandes cantidades."
    professor2 "Sin embargo, todavía no había llegado a Italia."
    professor2 "{b}Galileo{/b} entendió rápidamente la potencial utilidad que tendría un instrumento de tales características."
    professor2 "Entonces se propuso mejorarlo utilizando sus conocimientos sobre óptica."
    professor2 "Él mismo se encargó de pulir a mano las lentes necesarias."
    professor2 "Fabricó con ellas unos catalejos que permitían aumentar entre ocho y nueve veces el tamaño del objeto divisado."

    pause 1
    scene black with dissolve
    stop music fadeout 2
    pause 2.5
    play music "audio/inventos.ogg" volume 0.8
    pause 1
    scene bg_telescope-donato with dissolve
    pause 1

    professor2 "{b}Paolo Sarpi{/b} organizó una demostración en el senado de Venecia en la que las personas asistentes quedaron muy impresionadas por las virtudes del aparato."
    professor2 "Sabiendo de las posibles aplicaciones del {b}telescopio{/b} en materia militar, {b}Galileo{/b} ofreció las instrucciones para su fabricación a las autoridades venecianas."
    professor2 "{b}Galileo{/b} fue recompensado con un puesto vitalicio como profesor de matemáticas en la universidad de Padua, donde se encontraba dando clase en aquel momento."
    professor2 "Los posteriores descubrimientos de {b}Galileo{/b} con el {b}telescopio{/b} fueron posibles gracias a las mejoras que introdujo en el aparato."

    show ch_professor_smile at left_resize with dissolve
    professor2 "Por otro lado, {b}Galileo{/b} cuenta en su haber con otros instrumentos que sí fueron inventos originales suyos."
    professor2 "Llegó incluso a obtener una fuente de ingresos complementaria a su salario como profesor de universidad por alguno de ellos."

    show sp_compass at scene_delay_pos1

    professor2 "Hablo de su primer invento comercial, el {b}compás geométrico militar{/b}, fabricado allá por el año 1597."
    professor2 "Este aparato es considerado como el precursor de la {b}regla de cálculo{/b}."
    professor2 "¿Sabéis qué es una {b}regla de cálculo{/b}?"
    menu:
        with dissolve

        "¡Por supuesto!":
            show ch_professor_happy at left_resize
            professor "Olvidaba con quien estoy tratando..."
            hide ch_professor_happy

        "No tengo ni idea.":
            pass

        "Sirve para medir, ¿no?":
            show ch_professor_happy at left_resize
            professor "Jajaja, bueno... Más o menos..."
            hide ch_professor_happy

    show ch_professor_smile at left_resize
    show sp_slide-rule at scene_delay_pos2

    professor "La {b}regla de cálculo{/b} es un instrumento que actúa como una calculadora analógica."
    professor2 "Es similar a una regla ordinaria como la que conocéis, en el sentido de que tiene unas escalas con sus divisiones y su correspondiente numeración."
    professor2 "Sin embargo, el principal atractivo de esta regla no es su utilización para medir longitudes,{w} que también puede hacerlo,{w} sino para realizar cálculos matemáticos."
    professor2 "Permite hacer tanto multiplicaciones y divisiones como cálculos más complejos: operaciones logarítmicas, trigonométricas, raíces cuadradas, etc."

    professor2 "El {b}compás geométrico militar{/b} desarrollado por {b}Galileo{/b} daba la posibilidad de calcular ángulos, obtener raíces cuadradas y cúbicas..."

    hide ch_professor_smile
    show ch_professor_happy at left_resize

    professor2 "¡E incluso determinar la carga adecuada para un cañón!"
    professor2 "¡Y todo esto sin necesidad de tener conocimientos matemáticos!"

    hide ch_professor_happy
    show ch_professor_smile at left_resize

    professor2 "Como podréis imaginar, fue todo un éxito en su momento."

    hide sp_slide-rule
    hide sp_compass
    with dissolve

    pause 1

    show sp_thermoscope at scene_delay_pos3

    professor2 "A {b}Galileo{/b} también se le atribuye el diseño del primer {b}termoscopio{/b}, el padre del {b}termómetro{/b} moderno."
    professor2 "El {b}termoscopio{/b} consistía en un tubo de vidrio con una ampolla en su parte superior."
    professor2 "Colocado sobre un recipiente con agua, tenía la capacidad de determinar los cambios en la temperatura del aire."

    pause 0.5
    hide ch_professor_smile with dissolve
    pause 0.5
    scene black with dissolve
    pause 1
    scene bg_brown_texture with dissolve

    python:
        if persistent.font_label == "global":
            renpy.show("bg_thermoscope_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_thermoscope", at_list=[scene_delay])

    professor2 "El aire del interior de la ampolla se encuentra en equilibrio térmico con el aire del entorno."
    professor2 "Es decir, se encuentra a temperatura ambiente."
    professor2 "A medida que aumenta la temperatura, la presión que ejerce el aire de la ampolla sobre la columna de agua también se incrementa."
    professor2 "En consecuencia, el aire ejerce una fuerza tal que desplaza la columna de agua hacia abajo."
    professor2 "Sucede justo lo contrario cuando la temperatura disminuye."
    professor2 "La presión del aire es menor y la fuerza que desplaza la columna de agua no es tan fuerte."
    professor2 "A mayor temperatura ambiente, menor altura de la columna, y al revés."
    
    show ch_professor_smile at center_resize with dissolve
    pause 0.5

    professor2 "Fue {b}Santorre Santorio{/b}, médico y amigo de {b}Galileo{/b}, quien adaptó el instrumento para uso medicinal añadiéndole una escala."
    professor2 "Esta pequeña adición dio lugar al {b}termómetro{/b}."
    
    pause 1

    professor2 "Tal y como véis, el ingenio de {b}Galileo{/b} no solo se circunscribía a la investigación y desarrollo de teorías científicas."
    professor2 "Además, fue un ingeniero excelente."

    hide ch_professor_smile
    show ch_professor_grin at center_resize

    professor2 "Bien... Es hora de volver a poner a prueba vuestro ingenio."
    professor2 "El siguente caso se compone de dos apartados."
    professor2 "Mostradme qué sois capaces de hacer."

    pause 1
    
    hide ch_professor_grin with dissolve
    
    pause 1

    stop music fadeout 2
    scene black with dissolve

    pause 2.5

    play sound "audio/celesta.ogg"
    $ first_block_case04 = "La imaginación y la creatividad tienen un papel muy importante en el desarrollo de teorías científicas y el diseño de nuevos aparatos."
    $ second_block_case04 = "Utiliza tus conocimientos sobre el movimiento rectilíneo uniformemente acelerado (MRUA) para resolver el siguiente caso sobre movimiento vertical."
    show screen case_cover(4, first_block_case04, second_block_case04)

    pause
    hide screen case_cover with dissolve

    scene black with dissolve

    play music ["<silence 1.0>", "audio/puzzle.ogg"] volume 1

    pause 2

    define case04_01_desc = "Tras la demostración del movimiento de caída libre de Galileo, un niño que había estado observando todo con gran fascinación, se acerca a examinar las balas caídas desde el cielo. Ante la mirada atenta del público que había presenciado la demostración, agarra la bala de mosquete y la lanza hacia arriba en línea recta. Si la bala parte desde una altura de 1 m con una velocidad inicial de 35 kilómetros por hora, ¿cuál es la altura máxima que alcanza? Exprésala en metros. Considera la aceleración igual a -9.8 metros por segundo al cuadrado."

    python:
        if persistent.font_label == "global":
            background_case04_01_path = "images/backgrounds/bg_case04-01_alt.png"
        else:
            background_case04_01_path = "images/backgrounds/bg_case04-01.png"

    call screen case(bg_case=background_case04_01_path,
        case_number="4a", true_answer=5.82, case_desc=case04_01_desc) with dissolve

    stop music

    play sound "audio/correct_answer.ogg"

    pause 2

    play music "audio/vertical_movement.ogg" volume 0.6
    pause 1
    show ch_professor_happy at center_resize
    with dissolve

    professor "¡Has resuelto la primera parte! ¡Bien hecho, {b}[player_name]{/b}!"

    pause 1
    hide ch_professor_happy with dissolve
    pause 0.5
    scene black with dissolve
    pause 0.5
    scene bg_blackboard with dissolve
    pause 1

    professor "Voy a mostraros cómo lo habría resuelto yo."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case04-01_solution_1_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case04-01_solution_1", at_list=[scene_delay])

    professor2 "Escribo toda la información de la que disponemos para tenerla bien a la vista."
    professor2 "Además de la ecuación de la posición en un {b}MRUA{/b}, necesitaremos también la de la velocidad."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case04-01_solution_2_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case04-01_solution_2", at_list=[scene_delay])

    professor2 "La velocidad inicial nos la proporcionan en unidades que no son las del {b}Sistema Internacional{/b}."
    professor2 "Hago la conversión para evitar problemas durante los cálculos."


    python:
        if persistent.font_label == "global":
            renpy.show("bg_case04-01_solution_3_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case04-01_solution_3", at_list=[scene_delay])

    professor2 "Planteo las ecuaciones de movimiento y elimino los términos nulos."
    professor2 "Tengo un sistema de dos ecuaciones con dos incógnitas: la posición final y el tiempo."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case04-01_solution_4_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case04-01_solution_4", at_list=[scene_delay])

    professor2 "Trabajo con la ecuación del tiempo para despejar la incógnita."

    hide bg_case04-01_solution_2
    hide bg_case04-01_solution_2_alt
    hide bg_case04-01_solution_3
    hide bg_case04-01_solution_3_alt
    hide bg_case04-01_solution_4
    hide bg_case04-01_solution_4_alt
    with dissolve
    pause 1

    python:
        if persistent.font_label == "global":
            renpy.hide("bg_case04-01_solution_5_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case04-01_solution_5", at_list=[scene_delay])

    professor2 "La expresión del tiempo la utilizo en la ecuación de la posición."

    python:
        if persistent.font_label == "global":
            renpy.hide("bg_case04-01_solution_6_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case04-01_solution_6", at_list=[scene_delay])

    professor2 "Ahora trabajo un poco con la expresión..."

    python:
        if persistent.font_label == "global":
            renpy.hide("bg_case04-01_solution_7_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case04-01_solution_7", at_list=[scene_delay])

    professor2 "Y finalmente sustituyo los datos."

    pause 1

    show ch_professor_smile at center_resize with dissolve
    professor2 "Habréis podido comprobar que los casos se complican un poco respecto a los planteados para el {b}MRU{/b}."

    hide ch_professor_smile
    show ch_professor_grin at center_resize

    professor2 "Sin embargo, no hay nada que no pueda resolver un análisis inteligente y profundo de la situación."
    professor2 "Vamos con la segunda parte."

    pause 1
    hide ch_professor_grin with dissolve
    pause 1

    stop music fadeout 2
    scene black with dissolve

    pause 2.5

    play music ["<silence 1.0>", "audio/puzzle.ogg"] volume 1

    pause 2

    define case04_02_desc = "Tras la demostración de Galileo del movimiento de caída libre, un niño que había estado observando todo con gran fascinación, se acerca a examinar las balas caídas desde el cielo. Ante la mirada atenta del público que había presenciado la demostración, agarra la bala de mosquete y la lanza hacia arriba en línea recta. Si la bala parte desde una altura de 1 metro con una velocidad inicial de 35 kilómetros por hora, ¿cuánto tiempo transcurre hasta que cae al suelo? Exprésalo en segundos. Considera la aceleración igual a -9.8 metros por segundo al cuadrado."

    python:
        if persistent.font_label == "global":
            background_case04_02_path = "images/backgrounds/bg_case04-02_alt.png"
        else:
            background_case04_02_path = "images/backgrounds/bg_case04-02.png"

    call screen case(bg_case=background_case04_02_path,
        case_number="4b", true_answer=2.08, case_desc=case04_02_desc) with dissolve

    stop music

    play sound "audio/correct_answer.ogg"

    $ renpy.pause(3, hard=True)

    play music "audio/celebration.ogg" noloop
    image celebration = Movie(play="videos/confetti.webm", size=(1920, 1080), loop=False)
    show celebration with flash_shorter

    pause 1

    show ch_professor_happy at center_resize
    with dissolve
    professor "¡Has vuelto a hacerlo, {b}[player_name]{/b}! ¡No me lo puedo creer!"

    pause 1

    hide ch_professor_happy with dissolve

    pause 0.5

    scene black with dissolve

    play music "audio/vertical_movement.ogg" volume 0.6

    pause 1

    scene bg_blackboard with dissolve

    pause 1

    professor "Observad cómo lo habría hecho yo."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case04-02_solution_1_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case04-02_solution_1", at_list=[scene_delay])

    professor2 "Escribo los datos en primer lugar, como de costumbre."
    professor2 "La velocidad ya la convertí a unidades del {b}Sistema Internacional{/b} en el apartado anterior, así que utilizaré ese resultado directamente."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case04-02_solution_2_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case04-02_solution_2", at_list=[scene_delay])

    professor2 "Cuando la bala toca el suelo, su posición es igual a cero."
    professor2 "Partiendo de esto, trabajo con la ecuación de la posición hasta conseguir una ecuación de segundo grado."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case04-02_solution_3_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case04-02_solution_3", at_list=[scene_delay])

    professor2 "Despejo la incógnita del tiempo haciendo uso de la famosa expresión para las ecuaciones de segundo grado."

    hide bg_case04-02_solution_1
    hide bg_case04-02_solution_1_alt
    hide bg_case04-02_solution_2
    hide bg_case04-02_solution_2_alt
    hide bg_case04-02_solution_3
    hide bg_case04-02_solution_3_alt
    with dissolve
    pause 1

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case04-02_solution_4_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case04-02_solution_4", at_list=[scene_delay])

    professor2 "Al estar resolviendo una ecuación de segundo grado, obtengo dos resultados."
    professor2 "Sin embargo, solo uno de ellos es correcto: el que me da un tiempo positivo."

    pause 1

    show ch_professor_smile at center_resize with dissolve
    professor2 "En el oficio de detective, habrá ocasiones en las que tendréis que decidir entre varias soluciones para un mismo caso."
    professor2 "En este, solo una de ellas es válida."
    professor2 "Es muy importante reflexionar sobre la coherencia de los resultados obtenidos para dar con la solución correcta."

    pause 0.5

    hide ch_professor_smile with dissolve

    pause 1
    
    scene black with dissolve
    stop music fadeout 2

    pause 2

    return
