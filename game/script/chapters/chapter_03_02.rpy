label chapter_03_02:
    stop music
    stop sound
    stop ambient
    scene black

    play music "audio/kennedy_space.ogg"

    pause 2

    scene bg_space-center with dissolve
    pause 1
    show ch_professor_smile at left_resize with dissolve
    pause 0.5

    professor "¿Sabéis dónde nos encontramos ahora mismo?"
    professor2 "Estamos en el Centro Espacial John F. Kennedy, un lugar situado al sureste de Estados Unidos, en Florida."
    professor2 "Se utiliza para el lanzamiento de cohetes al espacio."
    professor2 "Fue construido en 1962 con el objetivo inicial de servir al {b}programa Apolo{/b}."

    pause 0.5
    hide ch_professor_smile with dissolve
    pause 1
    scene black with dissolve
    pause 1
    scene bg_apolo-intro at slowzoom with dissolve
    pause 1

    professor2 "En activo desde 1961 hasta 1972, el {b}programa Apolo{/b} fue el programa espacial desarrollado por Estados Unidos..."
    professor2 "... en el contexto de la {b}carrera espacial{/b} contra la Unión Soviética."
    professor2 "Cumplió, por tanto, un papel fundamental en el hito histórico que supuso la llegada del ser humano a la {b}Luna{/b} en julio de 1969."

    pause 1
    show ch_professor_smile at center_resize with dissolve
    pause 0.5

    professor2 "Innovación científica y tecnológica, viajes al espacio, lucha ideológica entre bloques de potencias mundiales en un ambiente de aparente paz tensa..."

    show ch_professor_happy at center_resize
    hide ch_professor_smile

    professor2 "Remontémonos unos años atrás para ver cómo comenzó todo."

    pause 1
    hide ch_professor_happy with dissolve
    pause 1
    stop music fadeout 2
    scene black with dissolve
    pause 2.5

    play music "audio/cold_war.ogg"
    professor2 "Año 1945. Postdam, Alemania."

    pause 1
    scene bg_postdam with dissolve
    pause 0.5

    professor2 "La victoria del bloque de los Aliados (liderado por la Unión Soviética, Estados Unidos y Reino Unido)..."
    professor2 "... contra las potencias del Eje (con la Alemania nazi, Italia y Japón a la cabeza) estaba cerca."
    professor2 "Y con ella, el fin de la {b}Segunda Guerra Mundial{/b}."
    professor2 "Las tres líderes aliadas se reunieron en el Palacio Cecilienhof para establecer los términos en los que se daría fin..."
    professor2 "... a uno de los conflictos bélicos más violentos de la historia."
    professor2 "Lo que entonces se pretendía que fuera el inicio de una paz duradera, únicamente se prolongó durante un año y medio."
    professor2 "Las hostilidades originadas por la repartición del poder sobre un mundo en posguerra..."
    professor2 "... dieron lugar a una {b}Guerra Fría{/b} que no terminaría hasta 40 años más tarde."

    pause 1
    scene black with dissolve
    pause 1
    scene bg_cold-war with dissolve
    pause 1

    professor2 "La {b}Guerra Fría{/b} fue un conflicto geopolítico entre adversarios que defendían dos concepciones muy diferentes del mundo:"
    professor2 "El orden capitalista, representado principalmente por Estados Unidos..."
    professor2 "... y la cosmovisión socialista, con la Unión Soviética como elemento más destacado."
    professor2 "Las cuatro décadas que sucedieron a la {b}Segunda Guerra Mundial{/b} se caracterizaron por la lucha entre estos dos bloques por la hegemonía global."
    professor2 "Aun sin haber un enfrentamiento bélico directo entre las principales contendientes, la pugna tuvo lugar en multitud de frentes:"
    professor2 "El desarrollo armamentístico y militar, las políticas económicas, la propaganda ideológica,..."
    professor2 "... la guerra psicológica, el espionaje, o la innovación científica y tecnológica."
    professor2 "La {b}carrera{/b} por la conquista del {b}espacio{/b} formó parte de esta competencia por mostrar a la humanidad..."
    professor2 "... la superioridad una visión del mundo sobre la otra a través de los logros en el campo de la astronáutica."

    pause 1
    stop music fadeout 2
    scene black with dissolve
    pause 2.5

    play music "audio/space_race.ogg" volume 0.8
    scene bg_space-race at slowzoom with dissolve
    pause 1

    professor2 "El inicio de la {b}carrera espacial{/b} tuvo lugar en 1955 con los anuncios de Estados Unidos y la Unión Soviética de sus intenciones para..."
    professor2 "... la puesta en órbita de los primeros satélites artificiales."
    professor2 "Serían sus contribuciones al {b}Año Geofísico Internacional{/b} que se iba a celebrar entre los años 1957 y 1958."
    professor2 "En esta primera contienda fue la Unión Soviética la que salió victoriosa."

    pause 1
    scene black with dissolve
    pause 1

    professor2 "El 4 de octubre de 1957, el bloque soviético lanzó al espacio el primer satélite artificial de la historia: el {b}Sputnik 1{/b}."

    pause 1
    scene bg_sputnik with dissolve
    pause 1

    professor2 "El {b}Sputnik 1{/b} («compañero» en ruso), con forma esférica, pesaba 84 kg y tenía un diámetro de 58 cm."
    professor2 "Tenía alojados dos emisores de radio que posibilitaban la comunicación con el satélite desde la Tierra."
    professor2 "Para comprobar que la misión había sido satisfactoria, el equipo técnico necesitó esperar 90 minutos desde el lanzamiento."
    professor2 "Este era el tiempo que tardaba el satélite en dar una vuelta alrededor de la Tierra y que..."
    professor2 "... había que aguardar para recibir las ondas de radio emitidas por él."

    pause 1
    scene black with dissolve
    pause 1
    scene bg_sputnik-monument with dissolve
    pause 1
    show ch_professor_normal at right_resize with dissolve
    pause 0.5

    professor2 "El logro soviético causó estupor e histeria en un oponente que hasta entonces se había mostrado incrédulo..."
    professor2 "... ante los anuncios a bombo y platillo de la Unión Soviética sobre su plan."
    professor2 "El {b}Sputnik{/b} representó en aquel momento el fin de la creencia popular de la superioridad estadonunidense en materia científica y tecnológica."
    professor2 "Supuso un duro golpe para el país y dio lugar al cuestionamiento de su forma de vida."
    professor2 "La falta de intervención del estado en la economía, el bajo nivel educativo o el consumismo..."
    professor2 "... fueron algunos aspectos criticados desde dentro."
    professor2 "La primera respuesta estadounidense al {b}Sputnik{/b} fue el intento fallido de poner en órbita el satélite {b}Vanguard{/b}, desarrollado por la marina."
    professor2 "Tras ascender unos metros, el motor del cohete que lo transportaba se paró y cayó al suelo, provocando un incendio entre los restos de la maquinaria."
    professor2 "La desastrosa misión consiguió el efecto contrario al esperado: engrandecer aún más el éxito del {b}Sputnik{/b}."

    pause 1
    hide ch_professor_normal with dissolve
    pause 1
    scene bg_black with dissolve
    pause 1
    scene bg_explorer-1 with dissolve
    pause 1

    professor2 "El primer satélite estadounidense puesto en órbita fue el {b}Explorer 1{/b}, lanzado el 31 de enero de 1958."
    professor2 "Pese a tener una masa menor que el {b}Sputnik{/b}, de unos 14 kg, sirvió para realizar el primer gran hallazgo de la era espacial:"
    professor2 "El descubrimiento de los {b}cinturones de Van Allen{/b}, unos cinturones de alta radiación que rodean la Tierra."
    professor2 "Recibieron ese nombre del científico estadounidense que teorizó sobre su existencia."

    pause 1
    scene bg_black with dissolve
    pause 1
    scene bg_nasa with dissolve
    pause 1

    professor2 "Desde el punto de vista estadounidense, una de las consecuencias más importantes del logro del {b}Sputnik{/b} fue la creación de una agencia espacial de carácter civil."
    professor2 "Hasta 1958, la competencia espacial estadounidense se repartía entre varias ramas del ejército."
    professor2 "La creación de la agencia espacial tenía como objetivo concentrar en una única institución todos los esfuerzos del país en esta materia."
    professor2 "Fue así como, el 1 de octubre de 1958, nació la {b}Administración Nacional de Aeronáutica y del Espacio{/b}, conocida como {b}NASA{/b} por sus siglas en inglés."

    pause 1
    stop music fadeout 2
    scene black with dissolve
    pause 2.5

    play music "audio/soviet_space.ogg"
    scene bg_soviet-space with dissolve
    pause 1
    show ch_professor_normal at right_resize with dissolve
    pause 0.5

    professor2 "El éxito del {b}Sputnik 1{/b} dio inicio a la edad de oro del programa espacial soviético."
    professor2 "Se sucedieron varias misiones espaciales muy importantes en un periodo muy corto de tiempo, junto a otros tantos experimentos fallidos."
    professor2 "El 3 de noviembre de 1957, el {b}Spunik 2{/b} puso en órbita al primer pasajero."
    professor2 "Se trata la perra {b}Laika{/b}, que falleció tras la inserción en la órbita."
    professor2 "{b}Luna 1{/b}, una sonda soviética lanzada en 1959, fue el primer objeto creado por el ser humano que escapó de la gravedad de la Tierra." 
    professor2 "Esta sonda permitió descubrir, no solo que la {b}Luna{/b} no tenía campo magnético, sino también que el Sol emitía un flujo de partículas cargadas eléctricamente."
    professor2 "A este flujo se le dio el nombre de {b}viento solar{/b}."
    professor2 "Además, se lanzó al espacio la nave {b}Sputnik 5{/b} en 1960."
    professor2 "{b}Sputnik 5{/b} fue la primera misión espacial que traía de vuelta con vida tras un día en el espacio a los seres transportados en el interior del cohete:"
    professor2 "Las perras {b}Belka{/b} y {b}Strelka{/b}."

    pause 1
    hide ch_professor_normal with dissolve
    pause 1
    scene bg_black with dissolve
    pause 1
    scene bg_gagarin-tereshkova with dissolve
    pause 1

    professor2 "La Unión Soviética también fue pionera en el envío del primer ser humano al espacio."
    professor2 "El astronauta soviético {b}Yuri Gagarin{/b} partió a bordo del {b}Vostok 1{/b} desde Baikonous, hoy en la actual Kazajistán, en 1961."
    professor2 "Regresó tras 108 minutos de vuelo al completar una órbita al planeta Tierra."
    professor2 "Además, también envió a la primera mujer astronauta al espacio en 1963."
    professor2 "{b}Valentina Tereskhova{/b} dio en solitario un total de 48 vueltas en la órbita terrestre durante tres días."
    professor2 "Actualmente sigue siendo la única mujer que ha viajado en solitario al espacio.{w}¡Menuda hazaña!"

    pause 1
    stop music fadeout 2
    scene bg_black with dissolve
    pause 2.5
    
    play music "audio/apolo.ogg" volume 0.7
    scene bg_pioneer with dissolve
    pause 1

    professor2 "Durante estos años, Estados Unidos no se quedó de brazos cruzados y siguió trabajando en el perfeccionamiento de su tecnología..."
    professor2 "... y en la preparación de su cuerpo científico y astronauta."
    professor2 "Lanzó al espacio diferentes satélites meteorológicos y de telecomunicaciones."
    professor2 "La primera sonda norteamericana que sobrevoló la {b}Luna{/b}, a unos 60,000 km de distancia, fue la {b}Pioneer 4{/b}."
    professor2 "Fué lanzada en noviembre de 1959, unos meses después del satélite soviético {b}Luna 1{/b}."

    pause 1
    scene bg_black with dissolve
    pause 1
    scene bg_shepard with dissolve
    pause 1

    professor2 "Estados Unidos también realizó varias expediciones espaciales tripuladas, tanto con animales como con seres humanos."
    professor2 "{b}Alan Shepard{/b} se convirtión en el primer estadounidense en visitar el espacio en un vuelo suborbital de 15 minutos en 1963."
    professor2 "Sin embargo, el mayor hito del programa espacial estadounidense,..."
    professor2 "... que asestaría un duro golpe al bloque soviético en la pugna por la conquista del espacio, estaba aún por llegar."

    pause 1
    scene bg_black with dissolve
    pause 1
    scene bg_apolo-saturn with dissolve
    pause 1

    professor2 "El {b}programa Apolo{/b}, que comenzó a existir como proyecto a mediados de 1960, tenía tres objetivos principales:"
    professor2 "El primero era llevar al primer ser humano a la {b}Luna{/b} y traerlo sano y salvo."
    professor2 "El segundo consistía en colocar a Estados Unidos en una posición predominante en la conquista del espacio."
    professor2 "El tercero era la creación de un programa de exploración de la {b}Luna{/b} y desarrollar la capacidad de trabajar en un ambiente lunar."

    show ch_professor_normal at center_resize with dissolve

    professor2 "La primera misión tripulada, con el nombre de {b}Apolo 1{/b}, estaba prevista para febrero de 1967."
    professor2 "No llegó a tener lugar por el grave suceso que se dio durante los ensayos en los días previos al lanzamiento."
    professor2 "Hubo un cortocircuito en el módulo de mando que provocó un incendio en el que murieron tres astronautas."
    professor2 "Aquella catástrofe conllevó un retraso en las actividades previstas en el programa."
    professor2 "Se realizó una profunda investigación y se corrigieron los errores detectados."
    professor2 "Al contrario de lo que se podría pensar, este suceso no disminuyó el apoyo recibido por parte de la opinión pública."

    pause 1
    hide ch_professor_normal with dissolve
    pause 1
    scene bg_black with dissolve
    pause 1
    scene bg_apolo-8 with dissolve
    pause 1

    professor2 "Un tiempo más tarde, en diciembre de 1968, el {b}Apolo 8{/b} fue la primera misión en llevar al ser humano a la órbita lunar."
    professor2 "Los astronaunas de la nave fueron las primeras personas en observar la Tierra desde la {b}Luna{/b}."
    professor2 "El {b}Apolo 8{/b} fue la misión más importante de la primera década en el espacio..."
    professor2 "... y marcó el momento en el que Estados Unidos se situó a la cabeza de la {b}carrera espacial{/b}."

    pause 1
    stop music fadeout 2
    scene bg_black with dissolve
    pause 2.5

    play music "audio/moon.ogg" volume 0.8
    scene bg_3-apolo-11 with dissolve
    pause 1

    professor2 "El 16 de julio de 1969 se produjo el lanzamiento del {b}Apolo 11{/b}, la quinta misión tripulada del {b}programa Apolo{/b}."
    professor2 "Esta misión tenía el cometido de llevar a los primeros seres humanos a la {b}Luna{/b} y devolverlos sanos y salvos."
    professor2 "{b}Neil Armstrong{/b} y  {b}Edward E. Aldrin{/b}, tripulantes del módulo lunar, alunizaron en la superficie de la {b}Luna{/b} cuatro días después."
    professor2 "En la órbita lunar, {b}Michael Collins{/b} esperaba en el otro módulo de la nave a que finalizase la expedición de sus compañeros."

    pause 1
    scene bg_black with dissolve
    pause 1
    scene bg_apolo-11 with dissolve
    pause 1

    professor2 "A las 22:56 del 20 de julio de 1969, {b}Neil Armstrong{/b} se convierte en el primer ser humano en poner pie en la {b}Luna{/b}."
    professor2 "Fue entonces cuando pronunció su célebre frase:"
    professor2 "«Este es un pequeño paso para el hombre, gran salto para la humanidad»."
    professor2 "Tras recoger muestras e instalar algunos instrumentos en la superficie lunar, iniciaron el regreso a casa."
    professor2 "El resto{w} es historia."

    show ch_professor_normal at center_resize with dissolve

    professor2 "{b}Soyuz{/b}, el equivalente soviético al {b}programa Apolo{/b}, que se desarrolló paralelamente, quedó empañado por el último éxito logrado por la llegada a la {b}Luna{/b}."
    professor2 "La carrera finalizó y Estados Unidos llegó primera a la meta."

    pause 1
    hide ch_professor_normal with dissolve
    pause 1
    stop music fadeout 2
    scene bg_black with dissolve
    pause 2.5
    
    play music "audio/subproducts.ogg"
    scene bg_antenna with dissolve
    pause 1

    professor2 "La {b}carrera espacial{/b} se produjo en un contexto de confrontación entre dos visiones del mundo con sus prestigios en juego."
    professor2 "Esto permitió una financiación sin precedentes por ambas partes."

    pause 1
    show ch_professor_smile at center_resize with dissolve
    pause 0.5

    professor2 "La investigación necesaria para realizar todas las misiones que se llevaron a cabo..."
    professor2 "... tuvo muchos subproductos que hoy son importantísimos en nuestras vidas."
    professor2 "El {b}satélite artificial{/b}, herramienta indispensable para las telecomunicaciones y la geolocalización, es uno de ellos."
    professor2 "También multitud de aplicaciones en medicina, navegación, meteorología, aparatos sin cableado, productos criogénicos..."
    professor2 "En fin, la lista es interminable."

    pause 0.5
    hide ch_professor_smile
    show ch_professor_grin at center_resize
    pause 0.5

    professor2 "¿Qué os parece si, tras esta charla, intentáis resolver un caso que os quiero proponer?"

    menu:
        with dissolve

        "¡Por supuesto!":
            pass

        "Supongo...":
            pass

    professor2 "¡Estupendo!"
    professor2 "Tiene dos partes, vamos con la primera."

    pause 1
    
    hide ch_professor_grin with dissolve
    
    pause 1

    stop music fadeout 2
    scene black with dissolve

    pause 2.5

    play sound "audio/celesta.ogg"
    $ first_block_case06 = "La Luna espera, solitaria en el cielo, una visita desde el planeta vecino."
    $ second_block_case06 = "Utiliza tus conocimientos sobre el movimiento circular uniforme (MCU) para resolver el siguiente caso sobre el movimiento de la Luna."
    show screen case_cover(6, first_block_case06, second_block_case06)

    pause
    hide screen case_cover with dissolve

    play music ["<silence 1.0>", "audio/puzzle.ogg"] volume 1

    pause 2

    define case06_01_desc = "Planeta Tierra, 16 de julio de 1969. La última fase de la misión Apolo 11 está a punto de comenzar. El despegue es inminente. Para poder llegar a la Luna, es imprescindible conocer su posición de forma precisa en todo momento. Si la Luna tarda 27.32 días en dar una vuelta a la Tierra, ¿qué distancia angular recorre en dos semanas? Exprésala en radianes."

    python:
        if persistent.font_label == "global":
            background_case06_01_path = "images/backgrounds/bg_case06-01_alt.png"
        else:
            background_case06_01_path = "images/backgrounds/bg_case06-01.png"

    call screen case(bg_case=background_case06_01_path,
        case_number="6a", true_answer=3.22, case_desc=case06_01_desc) with dissolve

    stop music

    play sound "audio/correct_answer.ogg"

    $ renpy.pause(3, hard=True)
    play music "audio/subproducts.ogg"

    pause 1

    show ch_professor_happy at center_resize
    with dissolve
    professor "¡Genial, {b}[player_name]{/b}! ¡Es correcto!"

    pause 0.5

    hide ch_professor_happy with dissolve

    pause 1

    scene black with dissolve


    pause 1

    scene bg_blackboard with dissolve

    pause 1

    professor "Aquí tenéis mi resolución para esta primera parte del caso."
    professor2 "Como siempre os digo, no es necesario que hayáis seguido un camino exactamente igual al mío."
    professor2 "Basta con haber aplicado los conceptos teóricos adecuadamente."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case06-01_solution_1_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case06-01_solution_1", at_list=[scene_delay])

    professor2 "En primer lugar, escribo la ecuación de la posición del {b}MCU{/b} y la información de la que dispongo."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case06-01_solution_2_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case06-01_solution_2", at_list=[scene_delay])

    professor2 "Como conocemos su periodo de rotación, podemos calcular la velocidad angular de la {b}Luna{/b}."
    professor2 "Es suficiente con recordar que una vuelta equivale a 2π rad."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case06-01_solution_3_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case06-01_solution_3", at_list=[scene_delay])

    professor2 "Ahora expresamos la velocidad angular en las unidades apropiadas."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case06-01_solution_4_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case06-01_solution_4", at_list=[scene_delay])

    professor2 "Finalmente sustituimos todos los datos en la ecuación de movimiento."
    professor2 "Obtenemos así la distancia angular que nos pedían."

    pause 1

    show ch_professor_smile at center_resize with dissolve
    pause 1

    professor2 "Aunque siempre es recomendable trabajar con unidades del {b}Sistema Internacional{/b}, en este caso es más cómodo utilizar otras diferentes."

    hide ch_professor_smile 
    show ch_professor_happy at center_resize

    professor2 "¡Hay que mantener la concentración para no equivocarse en la utilización de las unidades!"
    pause 1

    hide ch_professor_happy
    show ch_professor_grin at center_resize
    professor2 "Queda una segunda parte."
    professor2 "¡Vamos con ello!"

    hide ch_professor_grin with dissolve

    pause 1

    stop music fadeout 2
    scene black with dissolve

    pause 2.5

    play music ["<silence 1.0>", "audio/puzzle.ogg"] volume 1

    pause 2

    define case06_02_desc = "Planeta Tierra, 16 de julio de 1969. La última fase de la misión Apolo 11 está a punto de comenzar. El despegue es inminente. Para poder llegar a la Luna, es imprescindible conocer su posición de forma precisa en todo momento. Si la Luna tarda 27.32 días en dar una vuelta a la Tierra, ¿cuál es su velocidad lineal? Exprésala en kilómetros por hora."

    python:
        if persistent.font_label == "global":
            background_case06_02_path = "images/backgrounds/bg_case06-02_alt.png"
        else:
            background_case06_02_path = "images/backgrounds/bg_case06-02.png"

    call screen case(bg_case=background_case06_02_path,
        case_number="6b", true_answer=3683.85, case_desc=case06_02_desc) with dissolve

    stop music

    play sound "audio/correct_answer.ogg"

    $ renpy.pause(3, hard=True)

    play music "audio/celebration.ogg" noloop
    image celebration = Movie(play="videos/confetti.webm", size=(1920, 1080), loop=False)
    show celebration with flash_shorter

    pause 1

    show ch_professor_happy at center_resize
    with dissolve
    professor "¡Lo has vuelto a conseguir, {b}[player_name]{/b}! ¡Otra vez!"

    pause 1
    hide ch_professor_happy with dissolve
    pause 1

    scene black with dissolve
    play music "audio/subproducts.ogg"
    pause 1
    scene bg_blackboard with dissolve
    pause 1

    professor "Venga, aquí va mi resolución."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case06-02_solution_1_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case06-02_solution_1", at_list=[scene_delay])

    professor2 "En primer lugar, los datos y la ecuación que conecta la velocidad lineal con la angular."
    professor2 "Como la {b}Luna{/b} sigue un {b}MCU{/b}, puedo relacionar ambas magnitudes únicamente a través del radio de la trayectoria."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case06-02_solution_2_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case06-02_solution_2", at_list=[scene_delay])

    professor2 "Como ya calculé el valor de la velocidad angular en la primera parte del caso, puedo utilizarlo ahora."
    professor2 "Sin embargo, para poder trabajar con él debo transformar antes sus unidades."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case06-02_solution_3_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case06-02_solution_3", at_list=[scene_delay])

    professor2 "Si sustituyo en la ecuación, obtengo la velocidad lineal de la {b}Luna{/b}."

    pause 1

    show ch_professor_smile at center_resize with dissolve
    professor2 "Se mueve muy rápidamente."
    professor2 "Es increíble, ¿verdad?"

    pause 0.5

    hide ch_professor_smile with dissolve

    pause 1
    
    scene black with dissolve
    stop music fadeout 2

    pause 2
