label chapter_03_01:
    stop music
    stop sound
    stop ambient

    scene black
    pause 2

    #---- Preámbulo capítulo 3 ----#

    play music "audio/welcome.ogg" volume 0.5
    play ambient "audio/sample_crowd.ogg" loop volume 0.5
    pause 2
    
    narrator "Tercer día en la {b}Academia [academy_name]{/b}..."

    scene bg_academy_entrance with dissolve

    pause 1

    unknown3 "Pst...{w} Eh..."
    unknown3 "..."
    unknown3 "Pst..."
    unknown3 "..."

    show ch_student_angry at center_resize with hpunch

    student "¡BUUUUUUUUU!"

    hide ch_student_angry
    show ch_student_happy at center_resize

    student2 "¡Jajaja!"
    student2 "Te he vuelto a asustar, ¿verdad?"

    menu:
        with dissolve

        "Jajaja, ¡sí!":
            pass
        "¡Para nada!":
            student2 "¡No me engañas!"
            pass
        "No tienes ninguna gracia":
            student2 "Jajaja, no te pongas así."
            student2 "¡Solo estaba bromeando!"

    hide ch_student_happy
    show ch_student_smile at center_resize
    student2 "¡Buenos días, {b}[player_name]{/b}!"
    student2 "Estoy impaciente por comenzar la nueva clase de hoy."
    student2 "Aún sigo pensando en la sesión de ayer, fue increíble."
    student2 "Todavía me cuesta entender cómo una sola persona es capaz hacer todos los descubrimientos, desarrollos teóricos y experimentos que hizo {b}Galileo{/b}."
    student2 "Y no solo eso, además hizo lo que hizo sin muchas de las herramientas que hoy consideramos esenciales."
    student2 "El uso de la experimentación y las matemáticas en el estudio de la naturaleza era algo realmente extraño por entonces."
    student2 "No había internet, no tenían ordenadores, tampoco calculadoras..."
    student2 "El tío ni siquiera utilizaba el álgebra..."

    hide ch_student_smile
    show ch_student_shock at center_resize

    student2 "¡¿Pero cómo puede ser?!"
    student2 "Menuda locura, es que no me cabe en la cabeza."

    hide ch_student_shock
    show ch_student_concerned at center_resize

    student2 "{b}[player_name]{/b},{w} escúchame,{w} que hacía raíces cuadradas con un compás..."

    menu:
        with dissolve

        "¡Es realmente increíble!":
            hide ch_student_concerned
            show ch_student_happy at center_resize
            student "¡Lo es!"
            pass
        "No está mal, pero tampoco es para tanto.":
            hide ch_student_concerned
            show ch_student_happy at center_resize
            student "Jajaja, ¡cómo eres!"
            pass

    hide ch_student_happy
    show ch_student_smile at center_resize
    student2 "Es fascinante, me habría encantado conocerle."
    student2 "Debe de ser superinteresante hablar con personas que dominen de tal manera una materia, la que sea."
    student2 "Especialmente para conocer cómo afrontaron los problemas que inevitablemente aparecen..."
    student2 "... cuando nos sumergimos en los terrenos inexplorados del conocimiento."
    student2 "De hecho, hace poco vi una película en la que hablaban sobre..."

    hide ch_student_smile
    show ch_student_shock at center_resize

    play sound "audio/sample_bell.ogg" volume 1.1

    pause 3

    hide ch_student_shock
    show ch_student_smile at center_resize

    student2 "¡Hora de entrar!"
    student2 "¿Echamos una carrera?"

    hide ch_student_smile
    show ch_student_grin at center_resize

    student2 "¡Vamos!"

    window hide
    hide ch_student_grin
    with dissolve

    pause 1

    scene bg_black with dissolve

    stop music fadeout 2
    stop ambient fadeout 2

    pause 2

    play music "audio/professor.ogg" volume 0.5

    pause 1

    narrator "En el aula..."

    scene bg_academy_classroom with dissolve
    pause 1
    show ch_professor_happy at center_resize with dissolve
    pause 0.5

    professor "¡Buenos días!"
    professor2 "Os doy la bienvenida a la última jornada de este curso de {b}cinemática{/b}."
    professor2 "En las sesiones anteriores habéis demostrado dominar las teorías del {b}MRU{/b} y {b}MRUA{/b}."
    professor2 "Haciendo uso de vuestros conocimientos y vuestro ingenio, habéis sido capaces de aplicarlas correctamente a la resolución de casos reales." 
    professor2 "Tal y como haría cualquier detective profesional."
    professor2 "Por esta razón, ¡os doy mi enhorabuena!"

    classmates "¡Gracias, profeeeee!{w} \[...\]{w} ¡Genial!{w} \[...\]{w} ¡Vamooooooos!"

    hide ch_professor_happy
    show ch_professor_grin at center_resize

    professor2 "Sin embargo, esta aventura no ha acabado todavía."
    professor2 "Para obtener el galardón de detectives especialistas en {b}cinemática{/b} todavía os queda superar un último escalón."
    professor2 "No me cabe duda de que conseguiréis hacerlo."
    professor2 "Llevo muchos años en esto y sé que, aunque nos encontremos con obstáculos que nos dificulten el camino,..."
    professor2 "... podremos superarlos siempre que dispongamos de los medios necesarios."

    professor2 "Colocaos bien en el asiento y abrochaos los cinturones."
    professor2 "Estamos a punto de despegar."

    pause 1
    window hide
    hide ch_professor_grin
    with dissolve

    pause 1
    scene bg_black with dissolve
    stop sound fadeout 2
    stop music fadeout 2
    pause 3

    #----- Comienzo del capítulo 3. Trabajo de investigación. -----#

    scene black
    play sound "audio/celesta.ogg"
    
    $ chapter03_desc = "Te dispones a recorrer el último tramo de esta larga aventura. Es esta ocasión, desde el espacio.\nEmplea tus conocimientos sobre movimiento circular uniforme (MCU) para demostrar que mereces el título de detective especialista en cinemática."

    show screen chapter_cover("3", "A la luna venidera", chapter03_desc)

    pause
    hide screen chapter_cover with dissolve

    pause 2

    play music "audio/night_1.ogg"
    scene bg_black with dissolve

    pause 1

    professor "A 384,000 kilómetros de distancia de la Tierra, sumida en el silencio más absoluto, se encuentra la {b}Luna{/b}."

    scene bg_moon_mont  with dissolve
    pause 1

    professor2 "Con una imagen cambiante, el único {b}satélite natural{/b} de la Tierra gobierna el cielo y las estrellas durante la noche."
    professor2 "A lo largo de la historia, la {b}Luna{/b} ha sido objeto de creencias y supersticiones de todo tipo."
    professor2 "Ha servido también como inspiración en la creación de obras artísticas..."
    professor2 "Como compañía durante las noches solitarias de insomnio..."
    professor2 "Y, por supuesto, ha sido, y sigue siendo, un interesante objeto de estudio para la ciencia."

    pause 1
    scene black with dissolve
    pause 1
    scene bg_moon_sky at slowzoom with dissolve
    pause 1

    professor2 "La {b}Luna{/b} realiza, de forma aproximada, un {b}movimiento circular uniforme{/b} ({b}MCU{/b})."
    professor2 "La interpretación física de este movimiento es muy similar a la del {b}MRU{/b}."
    professor2 "Es similar en el sentido de que la velocidad con la que gira es constante en módulo, pero siguiendo una trayectoria con forma de circunferencia."
    professor2 "Los fenómenos cíclicos que tienen lugar por el movimiento uniforme de los astros en el cielo..."
    professor2 "... ya eran utilizados por las civilizaciones más antiguas para medir el tiempo."

    pause 1
    show ch_professor_smile at center_resize with dissolve
    pause 0.5

    professor2 "El control del tiempo, en cierta medida, otorgaba poder y la capacidad para organizar a la sociedad."
    professor2 "Su utilidad práctica llevó a la antigua civilización egipcia a interesarse más que ninguna otra por el tiempo."

    pause 0.5
    hide ch_professor_smile with dissolve
    pause 1
    stop music fadeout 2
    scene black with dissolve
    pause 2

    play music "audio/nile.ogg"
    pause 1
    scene bg_nilo with dissolve
    pause 1

    professor2 "La medición del tiempo permitía, por ejemplo, organizar los momentos de siembra y recogida de los cultivos."
    professor2 "También las celebraciones religiosas o el pago de los impuestos."
    professor2 "El {b}calendario{/b} es, por tanto, un instrumento que nació con el fin de safisfacer necesidades de tipo administrativo, religioso y social."
    professor2 "Los habitantes del valle del Nilo ya utilizaban un {b}calendario lunar{/b} mucho antes siquiera de que apareciese la escritura, allá por el V milenio a. C."
    professor2 "El {b}calendario lunar{/b} egipcio utilizaba como fenómeno de referencia el {b}mes sinódico{/b}, que es el periodo de tiempo entre dos fases iguales de la {b}Luna{/b}."

    pause 1
    scene black with dissolve
    pause 1
    scene bg_moon_phases with dissolve
    pause 1

    professor2 "El {b}mes sinódico{/b} dura aproximadamente 29 días y medio, tiempo en el que la {b}Luna{/b} pasa por sus cuatro fases principales:"
    professor2 "{b}Luna{/b} nueva, creciente, llena y menguante."
    professor2 "Asimismo, las {b}fases lunares{/b} podían ser utilizadas para marcar ciclos de tiempo superiores al del día, ya que el periodo entre fase y fase es de unos siete días."
    professor2 "De aquí nace el concepto de semana."
    professor2 "Y es que el {b}calendario lunar{/b} influyó en el {b}calendario civil{/b} que se adoptó más tarde, pues sus meses de 30 días también fueron herencia de los {b}meses lunares{/b}."

    pause 1
    show ch_professor_smile at center_resize with dissolve
    pause 0.5

    professor2 "La antigua civilización egipcia no fue la única que utilizó un {b}calendario lunar{/b} para la medición del tiempo."
    professor2 "Los asirios, babilonios, persas, romanos y otras civilizaciones del Mediterráneo y Oriente Próximo lo utilizaron como principal {b}calendario{/b}."

    hide ch_professor_smile
    show ch_professor_happy at center_resize
    pause 0.5

    professor2 "La {b}Luna{/b} tiene otro rol clave en un fenómeno que podemos observar diariamente en nuestro planeta."
    professor2 "Seguro que os suena..."

    pause 0.5
    hide ch_professor_happy with dissolve
    pause 1
    stop music fadeout 2
    scene black with dissolve
    pause 2

    play music "audio/playa.ogg"
    pause 1
    scene bg_coast with dissolve
    pause 1

    professor2 "Si vivís en zonas costeras, quizás hayáis podido percibir cambios en la altura del mar lo largo de un mismo día."
    professor2 "Se trata de un fenómeno producido por las {b}mareas{/b}."
    professor2 "Las {b}mareas{/b} no son más que olas muy extensas que se mueven a lo largo de los océanos."
    professor2 "Hay dos factores fundamentales que influyen en su formación."
    professor2 "El primero de ellos es la {b}fuerza gravitatoria{/b} que ejerce la {b}Luna{/b} sobre la Tierra."
    professor2 "El segundo es el {b}movimiento de rotación{/b} de la propia Tierra sobre sí misma."

    pause 1
    scene black with dissolve
    pause 1
    scene bg_brown_texture with dissolve

    show bg_mareas_tierra-luna_1 at scene_delay
    show bg_mareas_tierra-luna_2 at scene_delay2
    show bg_mareas_tierra-luna_3 at scene_delay3

    pause 0.5

    professor2 "En el punto de la superficie de la Tierra más próximo a la {b}Luna{/b}, la {b}fuerza gravitatoria{/b} que ejerce el satélite atrae a las masas de agua hacia sí mismo."
    professor2 "En consecuencia, se produce el aumento de la altura del mar."
    professor2 "En el lado opuesto de la Tierra sucede igual por la inercia debida al {b}movimiento rotacional{/b} de nuestro planeta."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_mareas_tierra-luna_4_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_mareas_tierra-luna_4", at_list=[scene_delay])

    professor2 "Este incremento de la altura del mar se conoce como {b}marea alta{/b} o {b}pleamar{/b}."
    professor2 "En cambio, los puntos del planeta unidos por una línea perpendicular a la que une a la {b}Luna{/b} con la Tierra..."
    professor2 "... son los lugares en los que la altura del mar disminuye."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_mareas_tierra-luna_5_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_mareas_tierra-luna_5", at_list=[scene_delay])

    professor2 "Este fenómeno se conoce como {b}marea baja{/b} o {b}bajamar{/b}."
    professor2 "El tiempo aproximado entre dos {b}pleamares{/b} o {b}bajamares{/b} consecutivas es algo mayor a las 12 horas."
    professor2 "El tiempo entre una {b}pleamar{/b} y una {b}bajamar{/b} es más corto, un poco por encima de las seis horas."
    professor2 "Por esta razón podemos observar este fenómeno a lo largo de un mismo día."
    professor2 "Hay otro agente que también interviene en la formación de las {b}mareas{/b}, aunque su papel no es tan importante."

    scene bg_brown_texture with dissolve
    pause 1

    show bg_mareas_sol_1 at scene_delay
    show bg_mareas_sol_2 at scene_delay2
    show bg_mareas_sol_3 at scene_delay3

    professor2 "La {b}fuerza gravitatoria{/b} del {b}Sol{/b} afecta en menor medida a las masas de agua por encontrarse mucho más lejos de la Tierra que la {b}Luna{/b}."
    professor2 "Sin embargo, puede potenciar o disminuir la intensidad de las {b}mareas{/b} en función de dónde estén situadas la {b}Luna{/b} y la Tierra."

    show bg_mareas_sol_4 at scene_delay

    professor2 "Cuando todos los cuerpos se encuentran alineados, durante las lunas llenas o nuevas, la suma de las {b}fuerzas gravitatorias{/b}..."
    professor2 "... genera una {b}fuerza de atracción{/b} que provoca un gran aumento de la altura del mar."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_mareas_sol_5_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_mareas_sol_5", at_list=[scene_delay])

    professor2 "Tiene lugar entonces el fenómeno que se conoce como {b}marea viva{/b}."


    show bg_mareas_sol_6 at scene_delay
    show bg_mareas_sol_7 at scene_delay2
    show bg_mareas_sol_8 at scene_delay3

    professor2 "En cambio, durante los cuartos menguantes o crecientes, sucede lo contrario."

    show bg_mareas_sol_9 at scene_delay

    professor2 "En esta situación, cuando las líneas Tierra-Luna y Tierra-Sol son perpendiculares,..."
    professor2 "... la acción conjunta de las {b}fuerzas gravitatorias{/b} disminuye la intensidad de la {b}marea{/b}."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_mareas_sol_10_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_mareas_sol_10", at_list=[scene_delay])

    professor2 "En esta ocasión, aparece lo que se conoce como {b}marea muerta{/b}."

    pause 1
    scene black with dissolve
    pause 1

    scene bg_mont_saint_michel_hightide with dissolve
    pause 1

    professor2 "Algunas de las {b}mareas{/b} más conocidas son las del Mont Saint Michel, en Normandía, Francia."
    professor2 "¡Puede haber una diferencia de altura entre {b}pleamar{/b} y {b}bajamar{/b} de hasta 15 metros!"
    professor2 "La imagen que estáis viendo ahora es la de una {b}pleamar{/b} en ese mismo lugar."

    hide window with dissolve
    pause 3

    professor2 "Fijaos como queda el paisaje en plena {b}bajamar{/b}."

    pause 1
    scene bg_mont_saint_michel_lowtide with dissolve
    pause 3

    professor2 "Increíble, ¿verdad?"
    
    pause 1
    show ch_professor_smile at center_resize with dissolve
    pause 0.5

    professor2 "Cuando se visita en este tipo de lugares, es muy importante informase sobre las horas a las que se producen las máximas crecidas del mar."
    professor2 "Como podréis imaginar, puede llegar a ser muy peligroso."

    pause 1
    hide ch_professor_smile with dissolve
    pause 1
    stop music fadeout 2
    scene black with dissolve
    pause 3

    play music "audio/night_2.ogg" volume 0.6
    pause 1

    professor2 "Durante toda nuestra vida hemos podido observar a la {b}Luna{/b} presidiendo el cielo durante la noche."

    pause 1
    scene bg_moon_clouds with dissolve
    pause 1

    professor2 "Pero, ¿creéis que ha estado {b}siempre{/b} ahí?"
    professor2 "Sería un poco extraño, ¿verdad?"
    professor2 "Si no es así, entonces...{w} ¿Cuándo se formó nuestro único {b}satélite natural{/b}?"
    professor2 "Es más, ¿cómo sucedió?"
    professor2 "¿Y por qué se considera a la {b}Luna{/b} un {b}satélite{/b} y no un {b}planeta{/b} si, al igual que la Tierra, gira alrededor del Sol?"

    pause 1
    show ch_professor_happy at center_resize with dissolve
    pause 1

    professor2 "Cuántas preguntas, ¿verdad?"
    
    hide ch_professor_happy
    show ch_professor_grin at center_resize

    professor2 "Demostradme una vez más vuestra capacidad investigadora."
    professor2 "¡Adelante!"

    pause 1

    window hide
    hide ch_professor_grin
    with dissolve

    pause 1
    scene black with dissolve
    stop music fadeout 2
    pause 2

    define first_block_research_03 = "La Luna ha estado ahí, sobre nuestras cabezas, en todas y cada una de las noches durante nuestra vida. Sin embargo, no siempre ha sido así." 
    define second_block_research_03 = "¿Cuándo se formó la Luna?\n¿Cuál es la teoría sobre su formación? ¿Hay otras?\n¿Por qué es considerada un satélite y no un planeta?"
    define third_block_research_03 = "Indaga sobre estas preguntas y recopila la información más relevante"
    play sound "audio/celesta.ogg"
    show screen research_work_cover(first_block_research_03, second_block_research_03, third_block_research_03)

    pause
    hide screen research_work_cover with dissolve

    scene black with dissolve

    stop music fadeout 2
    pause 2

    return

