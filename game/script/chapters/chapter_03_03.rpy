label chapter_03_03:
    stop music
    stop sound
    stop ambient
    scene black

    pause 1
    play music "audio/earth.ogg"
    pause 1

    professor "Desde la puesta en órbita del {b}Sputnik 1{/b}, el número de satélites artificiales en el espacio ha crecido enormemente."

    pause 1
    scene bg_earth with dissolve
    pause 1
    show ch_professor_smile at left_resize with dissolve
    pause 1

    professor2 "En la actualidad hay más de 10,000 satélites artificiales en activo merodeando por el espacio."

    hide ch_professor_smile
    show ch_professor_happy at left_resize

    professor2 "Es una auténtica pasada."

    hide ch_professor_happy
    show ch_professor_smile at left_resize

    professor2 "Los satélites artificiales pueden cumplir funciones de todo tipo."
    professor2 "Los satélites de telecomunicaciones, por ejemplo, se encargan de emitir ondas que contienen señales de vídeo y audio."
    professor2 "Estos satélites posibilitan las retransmisiones de televisión y radio o las llamadas de teléfono."
    professor2 "También hay satélites de observación terrestre, que permiten obtener información sobre la composición de la atmósfera, por ejemplo,..."
    professor2 "... o los tipos de vegetación que hay de la superficie de la Tierra."
    professor2 "Este tipo de satélites pueden utilizarse también para la detección y seguimiento de incendios."
    professor2 "También hay satelites meteorológicos para supervisar el tiempo atmosférico..."
    professor2 "Satélites de navegación para la utilización del GPS..."
    professor2 "Satélites para fines militares..."
    professor2 "Sin olvidar los satélites para misiones espaciales con fines científicos."
    professor2 "En fin, la lista es enorme."

    pause 1
    hide ch_professor_smile with dissolve
    pause 1
    scene black with dissolve
    pause 1
    scene bg_satellite-geo with dissolve
    pause 1

    professor2 "Atendiendo al tipo de órbita que realizan, uno de los tipos
    de satélites más importantes son los {b}geoestacionarios{/b}."
    professor2 "Los satélites {b}geoestacionarios{/b} siguen una órbita totalmente circular alrededor del ecuador terrestre y se sitúan siempre sobre el mismo punto del planeta."
    professor2 "Esto quiere decir que, si pudiesemos divisar ahora mismo un {b}satélite geoestacionario{/b} sobre nuestras cabezas,..."
    professor2 "... nos parecería que está suspendido inmóvil en el cielo porque se mueve exactamente a la misma velocidad con la que rota la Tierra."
    professor2 "De ahí viene el nombre de {b}geoestacionario{/b}: su posición es estacionaria (invariante) vista desde la Tierra."

    show ch_professor_smile at center_resize with dissolve

    professor2 "Los satélites {b}geoestacionarios{/b} cumplen funciones que posibilitan las telecomunicaciones."
    professor2 "Actualmente se utilizan sobre todo para retransmisión de televisión."
    professor2 "Sin embargo, su cometido inicial fue el de permitir llamadas telefónicas de larga distancia."
    professor2 "Hoy en día también se ocupan de las llamadas telefónicas internacionales y regionales y de proporcionar acceso a internet,..."

    hide ch_professor_smile
    show ch_professor_happy at center_resize

    professor2 "... pero en menor medida si comparamos con el uso televisivo."

    pause 1
    hide ch_professor_happy with dissolve
    pause 1

    stop music fadeout 2
    scene black with dissolve
    pause 2.5

    play music "audio/intelsat.ogg"
    pause 1
    scene bg_early-bird at slowzoom with dissolve
    pause 1

    professor2 "El primer satélite {b}geoestacionario{/b}, el {b}Intelsat 1{/b}, fue enviado al espacio en 1965."
    professor2 "Su objetivo era proporcionar servicio de telefonía a través del océano Atlántico."
    professor2 "Si hoy podemos comunicarnos por llamada telefónica con personas de cualquier parte del planeta,..."
    professor2 "... es en parte gracias a los aspectos tecnológicos innovadores que introdujo el {b}Intelsat 1{/b}."
    professor2 "Actualmente no sigue en funcionamiento, fue desactivado en 1969."

    pause 1
    show ch_professor_smile at center_resize with dissolve
    pause 1

    professor2 "¿Sabéis qué ocurre con los satélites una vez finaliza la misión para la que fueron diseñados?"

    menu:
        with dissolve
        "¡Por supuesto!":
            hide ch_professor_smile
            show ch_professor_happy at center_resize

            professor2 "¡Jajaja!"
            professor2 "¡Jamás debería dudar de mis detectives!"

            pause 1
            hide ch_professor_happy with dissolve

        "Nunca me lo había preguntado.":
            pause 1
            hide ch_professor_smile with dissolve

        "¿Se caen al suelo?":
            professor2 "En ocasiones sí se provocan caídas controladas desde las estaciones de operaciones, pero no siempre es así."

            pause 1
            hide ch_professor_smile with dissolve

    pause 1
    scene black with dissolve
    pause 1
    scene bg_satellite-orbit with dissolve
    pause 1

    professor2 "Hay varios destinos posibles para estos satélites dependiendo de cómo de grande sea la distancia a la que se encuentran de nuestro planeta."
    professor2 "En las órbitas más cercanas, se espera a que acaben el combustible y se dejan caer a la Tierra."
    professor2 "Los satélites entran en combustión al contactar con la atmósfera terrestre debido a su alta velocidad y a la fricción con el aire caliente."
    professor2 "Los más pequeños desaparecen sin dejar rastro."
    professor2 "En cambio, los satélites más grandes no se destruyen totalmente al entrar en la atmósfera."
    professor2 "En estos casos, se realizan descensos controlados para asegurar que la caída no ponga en peligro a nadie."

    pause 1
    scene black with dissolve
    pause 1
    scene bg_brown_texture with dissolve
    pause 0.5
    show bg_world-map_01 with dissolve

    professor2 "El lugar al que se envían se conoce como el {b}cementerio de naves espaciales{/b}."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_world-map_02_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_world-map_02", at_list=[scene_delay])

    professor2 "Situado en el {b}Punto Nemo{/b}, el en océano Pacífico Sur, es el lugar más alejado de cualquier tierra en el planeta."
    professor2 "Su lejanía de la civilización lo convierte en el lugar más apropiado para esto."

    pause 1
    show ch_professor_smile at center_resize with dissolve
    pause 1

    professor2 "La gestión es diferente para los satélites que describen órbitas más lejanas a la Tierra."
    professor2 "Estos satélites se suelen desviar intencionadamente a lo que se conocen como {b}órbitas cementerio{/b}."
    professor2 "Se trata de órbitas muy lejanas de la Tierra, a más de 300,000 kilómetros de distancia."

    pause 1
    hide ch_professor_smile with dissolve
    pause 1
    scene black with dissolve
    pause 1
    scene bg_orbital_debris at slowzoom with dissolve

    professor2 "El elevado número de satélites que se lanzan cada año puede plantear serios problemas en la gestión de los desechos."
    professor2 "Al impacto medioambiental de los que caen a la Tierra o se queman en la atmósfera hay que añadir los que se convierten en basura espacial."
    professor2 "Estos últimos congestionan el espacio y podrían llegar a poner en peligro las expediciones de futuras generaciones."
    professor2 "Por tanto, conviene que los gobiernos, instituciones científicas y otras entidades hagan un uso responsable de ellos."

    pause 1
    stop music fadeout 2
    scene black with dissolve
    pause 2.5
    play music "audio/tramo_final.ogg"
    pause 1
    show ch_professor_grin at center_resize with dissolve
    pause 1

    professor2 "Ha llegado el momento."
    professor2 "El camino recorrido hasta aquí ha sido arduo, pero ya os encontráis en la recta final."
    professor2 "Os voy a presentar el último de los casos de este curso de {b}cinemática{/b}."
    professor2 "Consta de tres partes."
    professor2 "De resolverlo, recibiréis como galardón el título de detectives especialistas en la materia."
    professor2 "Este título garantiza que disponeis de los conocimientos necesarios para resolver..."
    professor2 "... un gran abanico de situaciones, problemas y enigmas utilizando las teorías del movimiento."

    professor2 "Tenéis toda mi confianza."
    professor2 "¡Adelante!"

    pause 1
    hide ch_professor_grin with dissolve
    pause 1

    stop music fadeout 2
    scene black with dissolve
    pause 2.5

    play sound "audio/celesta.ogg"
    $ first_block_case07 = "El esfuerzo dedicado al estudio suele tener su recompensa. Estás más cerca de conseguirlo que nunca."
    $ second_block_case07 = "Utiliza tus conocimientos sobre el movimiento circular uniforme (MCU) para resolver un último caso sobre movimientos satelitales."
    show screen case_cover(7, first_block_case07, second_block_case07)

    pause
    hide screen case_cover with dissolve

    scene black with dissolve

    play music ["<silence 1.0>", "audio/puzzle.ogg"] volume 1

    pause 2.5

    define case07_01_desc = "El Intelsat 1 fue el primer satélite geoestacionario enviado al espacio. Lanzado en 1965, su principal cometido era proporcionar servicio de telefonía a través del océano Atlántico. Si realizaba un MCU con una velocidad de 9500 kilómetros por hora, ¿a qué distancia se encontraba de la Tierra? Exprésala en kilómetros."

    python:
        if persistent.font_label == "global":
            background_case07_01_path = "images/backgrounds/bg_case07-01_alt.png"
        else:
            background_case07_01_path = "images/backgrounds/bg_case07-01.png"

    call screen case(bg_case=background_case07_01_path,
        case_number="7a", true_answer=36538, case_desc=case07_01_desc) with dissolve

    stop music

    play sound "audio/correct_answer.ogg"

    $ renpy.pause(3, hard=True)
    play music "audio/tramo_final.ogg"

    pause 1
    show ch_professor_smile at center_resize
    with dissolve

    professor "¡Muy bien hecho, {b}[player_name]{/b}!"

    pause 1
    hide ch_professor_smile with dissolve
    pause 1

    scene black with dissolve
    pause 1
    scene bg_blackboard with dissolve
    pause 1

    professor "Vamos con la resolución de este primer apartado."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case07-01_solution_1_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case07-01_solution_1", at_list=[scene_delay])

    professor2 "En primer lugar, escribo los datos y la ecuación que conecta la velocidad lineal con la angular."
    professor2 "El {b}Intelsat 1{/b} sigue un {b}MCU{/b} con un periodo igual al de rotación de la Tierra."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case07-01_solution_2_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case07-01_solution_2", at_list=[scene_delay])

    professor2 "Por tanto, puedo calcular la velocidad angular teniendo en cuenta que da una vuelta en 24 horas."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case07-01_solution_3_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case07-01_solution_3", at_list=[scene_delay])

    professor2 "Sustituyendo los datos obtengo la distancia a la que se encuentra el satélite."

    pause 1
    show ch_professor_grin at center_resize with dissolve
    pause 1

    professor2 "¡Vamos con la segunda parte!"

    pause 1
    hide ch_professor_grin with dissolve
    pause 1

    stop music fadeout 2
    scene black with dissolve
    pause 2.5


    play music ["<silence 1.0>", "audio/puzzle.ogg"] volume 1

    pause 2.5

    define case07_02_desc = "El Intelsat 1 fue el primer satélite geoestacionario enviado al espacio. Lanzado en 1965, su principal cometido era proporcionar servicio de telefonía a través del océano Atlántico. Si realizaba un MCU con una velocidad de 9500 kilómetros por hora, ¿qué distancia angular recorría en 5 años? Exprésala en grados."

    python:
        if persistent.font_label == "global":
            background_case07_02_path = "images/backgrounds/bg_case07-02_alt.png"
        else:
            background_case07_02_path = "images/backgrounds/bg_case07-02.png"

    call screen case(bg_case=background_case07_02_path,
        case_number="7b", true_answer=657000, case_desc=case07_02_desc) with dissolve

    stop music

    play sound "audio/correct_answer.ogg"

    $ renpy.pause(3, hard=True)
    play music "audio/tramo_final.ogg"
    pause 1

    show ch_professor_smile at center_resize
    with dissolve
    professor "¡Es correcto!"

    pause 1
    hide ch_professor_smile with dissolve
    pause 1

    scene black with dissolve
    pause 1
    scene bg_blackboard with dissolve
    pause 1

    professor "Os voy a mostrar mi resolución a continuación."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case07-02_solution_1_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case07-02_solution_1", at_list=[scene_delay])

    professor2 "Como siempre, escribo los datos y la ecuación del movimiento; en este caso, la del {b}MCU{/b}."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case07-02_solution_2_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case07-02_solution_2", at_list=[scene_delay])

    professor2 "Del mismo modo que en el apartado anterior, conocemos la velocidad angular."
    professor2 "En este momento, prefiero expresarla con unidades de grados por día."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case07-02_solution_3_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case07-02_solution_3", at_list=[scene_delay])

    professor2 "Paso el intervalo de tiempo a unidades de días."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case07-02_solution_4_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case07-02_solution_4", at_list=[scene_delay])

    professor2 "Ahora puedo sustituir en la ecuación de movimiento sin problema."

    pause 1
    show ch_professor_grin at center_resize with dissolve
    pause 1

    professor2 "¿Vamos con el último apartado?"
    professor2 "¡Ya casi está!"

    pause 1
    hide ch_professor_grin with dissolve
    pause 1

    stop music fadeout 2
    scene black with dissolve
    pause 2.5

    play music ["<silence 1.0>", "audio/puzzle.ogg"] volume 1

    pause 2.5

    define case07_03_desc = "El Intelsat 1 fue el primer satélite geoestacionario enviado al espacio. Lanzado en 1965, su principal cometido era proporcionar servicio de telefonía a través del océano Atlántico. Si realizaba un MCU con una velocidad de 9500 kilómetros por hora, ¿a qué velocidad tendría que ir para continuar siendo geoestacionario si su distancia a la Tierra se duplicara? Exprésala en kilómetros por hora."

    python:
        if persistent.font_label == "global":
            background_case07_03_path = "images/backgrounds/bg_case07-03_alt.png"
        else:
            background_case07_03_path = "images/backgrounds/bg_case07-03.png"

    call screen case(bg_case=background_case07_03_path,
        case_number="7c", true_answer=19000, case_desc=case07_03_desc) with dissolve

    stop music

    play sound "audio/correct_answer.ogg"

    $ renpy.pause(3, hard=True)

    play music "audio/celebration.ogg" noloop
    image celebration = Movie(play="videos/confetti.webm", size=(1920, 1080), loop=False)
    show celebration with flash_shorter

    pause 1

    show ch_professor_happy at center_resize
    with dissolve
    professor "¡Bien!{w} ¡Sabía que podías hacerlo!{w} ¡Siempre confié!"

    pause 1
    hide ch_professor_happy with dissolve
    pause 1

    scene black with dissolve
    play music "audio/tramo_final.ogg"
    pause 1
    scene bg_blackboard with dissolve
    pause 1

    professor2 "Puedo ver vuestras caras rebosantes de alegría."
    professor2 "Permitidme unos segundos para que os muestre mi última resolución."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case07-03_solution_1_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case07-03_solution_1", at_list=[scene_delay])

    professor2 "En primer lugar, los datos que conocemos."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case07-03_solution_2_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case07-03_solution_2", at_list=[scene_delay])

    professor2 "Trabajo con la ecuación que relaciona las velocidades angular y lineal para el doble del radio de la trayectoria."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case07-03_solution_3_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case07-03_solution_3", at_list=[scene_delay])

    professor2 "Para obtener el resultado, simplemente hay que multiplicar la velocidad lineal actual por 2."
    pause 1

    scene black with dissolve
    stop music fadeout 2
    pause 3

return


