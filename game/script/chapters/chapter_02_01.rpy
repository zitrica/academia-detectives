label chapter_02_01:
    stop music
    stop sound
    stop ambient

    scene black
    pause 2

    #---- Preámbulo capítulo 2 ----#

    play music "audio/welcome.ogg" volume 0.5
    play ambient "audio/sample_crowd.ogg" loop volume 0.5
    pause 2
    
    narrator "Al día siguiente en la {b}Academia [academy_name]{/b}..."

    scene bg_academy_entrance with dissolve

    pause 1

    show ch_student_smile at center_resize with hpunch

    student "¡HOLA!"

    hide ch_student_smile
    show ch_student_happy at center_resize

    student2 "¿Te he asustado, {b}[player_name]{/b}?{w} Jajaja, ¡qué miedica!"

    hide ch_student_happy
    show ch_student_smile at center_resize

    student2 "¿Qué tal estás? No pudimos hablar ayer al final de la mañana."
    student2 "Tuve que marcharme deprisa porque tenía unos recados que hacer."
    student2 "No pude preguntarte cómo te fue tu primer día en la academia."
    student2 "¿Qué te pareció la sesión sobre el {b}movimiento rectilíneo uniforme{/b}?"
    
    menu:
        with dissolve
        "¡Interesantísima!":
            hide ch_student_smile
            show ch_student_happy at center_resize
            student "¡¿Verdad que sí?!"
        "No estuvo mal.":
            hide ch_student_smile
            show ch_student_happy at center_resize
            student "A mí también me pareció interesante."
        "Fue un tostón...":
            hide ch_student_smile
            show ch_student_happy at center_resize
            student "Jajaja, ¿te parecieron demasiado sencillos los casos?"
    
    hide ch_student_happy
    show ch_student_grin at center_resize

    student "He de decir que me dejaste bastante impresionado con las resoluciones que plateaste."
    student2 "Bueno, no solo a mí; a toda la clase."
    student2 "¡Incluso la profesora parecía fascinada!"

    hide ch_student_grin
    show ch_student_annoyed at center_resize

    student2 "..."
    student2 "Estuve pensando ayer por la tarde..."
    student2 "Ese tal {b}Zenón{/b}...{w} El de las paradojas..."

    hide ch_student_annoyed
    show ch_student_happy at center_resize

    student2 "Vaya personaje, ¿verdad?"
    student2 "Nunca me había planteado ningún razonamiento como los que analizamos ayer."
    student2 "Todo lo que afirmaban las {b}paradojas{/b} contradecía claramente las experiencias que tenemos a diario."
    student2 "Sin embargo, las conclusiones a las que conducían los razonamientos de {b}Zenón{/b} parecían ser coherentes."
    student2 "Hubo que darle a la cabeza para poder desenmarañar aquellas situaciones."
    student2 "Ahí se encuentra el mérito de quien desempeña el oficio de detective,{w} supongo..."
    student2 "En encontrar respuestas adecuadas a problemáticas complejas."
    student2 "Recuerdo un caso que salió a la luz hace poco sobre..."

    hide ch_student_happy
    show ch_student_shock at center_resize

    play sound "audio/sample_bell.ogg" volume 1.1

    pause 3

    student2 "Vaya, es hora de entrar a clase."
    student2 "¡Vamos!"

    window hide
    hide ch_student_shock
    with dissolve

    pause 2

    scene bg_black with dissolve

    stop music fadeout 2
    stop ambient fadeout 2

    pause 2

    play music "audio/professor.ogg" volume 0.5

    pause 1

    narrator "En el aula..."

    scene bg_academy_classroom with dissolve
    pause 1
    show ch_professor_happy at center_resize with dissolve

    professor "¡Buenos días!"
    professor2 "Antes de nada me gustaría felicitaros por vuestra participación en la clase de ayer."
    professor2 "Fuisteis capaces de plantear soluciones inteligentes a las paradojas de {b}Zenón{/b} haciendo uso de la teoría del {b}MRU{/b}."
    professor2 "Quiero hacer especial incapié en la actuación de {b}[player_name]{/b}."
    professor2 "Pese a ser su primer día, se adaptó perfectamente a la dinámica de la clase."
    classmates "Como si llevara desde el comienzo del curso...{w} \[...\]{w} ¡Menuda incorporación!{w} \[...\]{w} Quizás pueda echarme una mano con los deberes..."

    hide ch_professor_happy
    show ch_professor_smile at center_resize

    professor "En la sesión de hoy vamos a dar un paso más allá en el estudio del movimiento."
    professor2 "Viajaremos con marcha acelerada al momento y al lugar en el que se produjo la génesis formal de la {b}cinemática{/b}."

    hide ch_professor_smile
    show ch_professor_grin at center_resize

    professor2 "Espero que os guste la pasta..."

    pause 1
    window hide
    hide ch_professor_grin
    with dissolve

    pause 1
    scene bg_black with dissolve
    stop sound fadeout 2
    stop music fadeout 2
    pause 2


    #----- Comienzo del capítulo 2. Trabajo de investigación. -----#

    scene black
    play sound "audio/celesta.ogg" volume 0.7
    
    $ chapter02_desc = "Una nueva travesía te espera, esta vez por la Italia de la Edad Moderna. Haz uso de tus conocimientos sobre movimiento rectilíneo uniformemente acelerado (MRUA) para resolver los nuevos casos que plantee la profesora Conan"

    show screen chapter_cover("2", "Y, sin embargo, se mueve", chapter02_desc)

    pause
    hide screen chapter_cover with dissolve

    play music "audio/pisa.ogg"
    scene bg_black with dissolve

    pause 1

    professor "Pisa, Italia, finales del siglo XVI."

    scene bg_pisa with dissolve
    pause 1
    show ch_professor_smile at left_resize with dissolve

    professor2 "Un joven científico, recién llegado como profesor de matemáticas a la universidad, comienza una serie de experimentos sobre el movimiento de los cuerpos."
    professor2 "Utilizando objetos en caída libre y desplazándose a lo largo de planos inclinados..."
    professor2 "... empezará a esbozar los fundamentos de una teoría que presentará al público 30 años después."
    professor2 "Os hablo del que probablemente sea el personaje que mejor representa el proceso de transición..."
    professor2 "... entre una concepción medieval del mundo y el nacimiento de la ciencia moderna en Europa occidental."
    professor2 "Os presento al gran {b}Galileo Galilei{/b}."

    pause 1
    hide ch_professor_smile with dissolve
    pause 2
    scene black with dissolve

    stop music fadeout 2

    pause 2.5

    play music "audio/inicios_galileo.ogg"
    pause 1
    professor "{b}Galileo{/b} nació en 1564 en la misma ciudad de Pisa."

    pause 1
    scene bg_pope at slowzoom with dissolve

    professor2 "Vivió durante toda su vida en una Europa dominada por una visión del mundo bajo el férreo control de la Iglesia Católica."
    professor2 "La concepción {b}aristotélica{/b} del mundo y del cosmos se mantenía como la principal doctrina de la naturaleza."
    professor2 "{b}Aristóteles{/b} describía un universo finito y eterno en el que la Tierra, una esfera inmóvil, se encontraba {b}en el centro{/b}."
    professor2 "El resto de astros y planetas, incrustrados en esferas transparentes de distintos tamaños, {b}giraban alrededor de la Tierra{/b}."
    professor2 "Seguro que esto os resulta familiar tras haber completado el trabajo de investigación de la sesión anterior."
    pause 1

    show ch_professor_normal at center_resize with dissolve
    professor2 "{b}Galileo{/b}, por el contrario, discrepaba con las ideas de {b}Aristóteles{/b}."
    professor2 "Él era favorable al modelo del cosmos de {b}Nicolás Copérnico{/b}, el iniciador de la nueva astronomía."

    pause 1
    window hide
    hide ch_professor_normal
    with dissolve

    pause 1
    scene black with dissolve
    pause 1

    scene bg_copernicus with dissolve
    pause 1

    professor2 "El modelo {b}copernicano{/b}, publicado en 1543 tras la muerte de su autor, sostenía que {b}el Sol (y no la Tierra) ocupaba el centro del universo{/b}."
    professor2 "Además, {b}Copérnico{/b} afirmaba que la Tierra realizaba un movimiento de rotación sobre sí misma y de traslación circular alrededor del Sol."
    professor2 "Este último realizado también por el resto de planetas."
    professor2 "Su modelo se oponía firmemente a la interpretación cristiana del ser humano como centro de un universo creado por Dios."
    professor2 "Era, por tanto, rechazado por la Iglesia."

    show ch_professor_normal at left_resize with dissolve
    pause 1

    professor2 "{b}Galileo{/b} no creía que se pudieran descifrar las leyes de la naturaleza a partir de principios que no estuvieran fundamentados por hechos físicos constatables."
    professor2 "Él abogaba por determinarlas a partir de la observación del mundo y la construcción de modelos matemáticos que permitiesen obtener y predecir resultados."
    professor2 "De nuevo, estos resultados debían ser verificados con las observaciones."
    professor2 "Por este motivo, {b}Galileo Galilei{/b} es considerado como uno de los principales instauradores del {b}método científico{/b}."

    hide ch_professor_normal with dissolve
    pause 0.5
    scene black with dissolve
    pause 1

    scene bg_telescope-demostration with dissolve
    pause 1

    professor "La forma en la que {b}Galileo{/b} trabajaba la ciencia fue muy positiva para el desarrollo de la disciplina."
    professor2 "Sin embargo, también fue fuente de numerosos conflictos con los defensores del pensamiento dominante en la época."

    professor2 "En esta línea, son especialmente relevantes sus descubrimientos realizados con el {b}telescopio{/b}."
    professor2 "Aunque no fue inventado por él, el uso que hizo {b}Galileo{/b} del {b}telescopio{/b} fue imprescindible..."
    professor2 "... en la revolución experimentada por la astronomía a partir de aquel momento."

    show ch_professor_grin at center_resize with dissolve
    pause 0.5

    professor2 "Mmmm..."
    professor2 "Tengo algo que proponeros..."

    pause 1

    window hide
    hide ch_professor_grin
    with dissolve

    pause 1

    scene black with dissolve

    stop music fadeout 2

    pause 2

    define first_block_research_02 = "A lo largo de la historia, el progreso científico se ha encontrado a menudo con la oposición de las instituciones de poder defensoras de las doctrinas del momento"
    define second_block_research_02 = "¿Qué descubrimientos hizo Galileo con el telescopio?\n¿A qué conclusiones conducían?\n¿Qué repercusiones personales tuvieron para él?"
    define third_block_research_02 = "Investiga sobre estas cuestiones y resume la información más relevante"
    play sound "audio/celesta.ogg"
    show screen research_work_cover(first_block_research_02, second_block_research_02, third_block_research_02)

    pause
    hide screen research_work_cover with dissolve

    scene bg_black with dissolve

    stop music fadeout 2
    pause 2

    return
