label chapter_01_02:
    stop music
    stop sound
    stop ambient
    scene black

    play music "audio/paradox_01_intro.ogg" volume 0.7

    pause 2

    professor "Como os dije antes, {b}{i}Física{/i}{/b}, la obra de {b}Aristóteles{/b}, es considerada como uno de los textos precursores de la {b}física{/b}."

    scene bg_aristotle_statue at slowzoom_to_left with dissolve

    pause 1

    show ch_professor_smile at right_resize with dissolve

    professor2 "El tratado está compuesto por ocho libros que contienen lecciones sobre la naturaleza y el universo físico."
    professor2 "Estas lecciones parten desde un punto de vista que se podría considerar más cercano a la filosofía que a la {b}física{/b} actual."
    professor2 "Por esta razón, se suele hablar de {b}filosofía natural{/b} para hacer referencia a este tipo de investigaciones."
    professor2 "{b}{i}Física{/i}{/b} reúne, entre muchas otras cosas, una serie de problemas filosóficos conocidos como las {b}paradojas de Zenón{/b}."
    professor2 "Estos problemas fueron planteados por {b}Zenón de Elea{/b}, un filósofo de la Antigua Grecia, allá por el siglo V a.C."
    professor2 "{b}Aristóteles{/b} los recogió en su tratado y aportó sus propias soluciones a las situaciones que en ellos se planteban."
    professor2 "Una de las más conocidas es la {b}paradoja de la dicotomía{/b}, descrita en el libro VI de {b}{i}Física{/i}{/b}."
    professor2 "Dice así..."

    pause 0.5
    hide ch_professor_smile with dissolve
    pause 0.5
    scene bg_black with dissolve

    pause 2

    show bg_brown_texture at scene_delay

    show bg_homero_paradox_01-1 at scene_delay

    show bg_homero_paradox_01-2 at scene_delay2

    professor2 "Homero, el famoso autor de poemas épicos griegos como la Ilíada o la Odisea, quiere recorrer un camino hasta el final del mismo."

    window show

    python:
        if persistent.font_label == "global":
            renpy.show("bg_homero_paradox_01-3_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_homero_paradox_01-3", at_list=[scene_delay])

    professor2 "Antes de llegar hasta el lugar deseado, debe recorrer la mitad del camino."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_homero_paradox_01-4_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_homero_paradox_01-4", at_list=[scene_delay])

    professor2 "Del mismo modo, antes de completar la mitad del trayecto, debe caminar una cuarta parte de la distancia total."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_homero_paradox_01-5_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_homero_paradox_01-5", at_list=[scene_delay])

    professor2 "Antes de recorrer la cuarta parte, deberá recorrer la octava, y, del mismo modo, la decimosexta... Y así indefinidamente."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_homero_paradox_01-6_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_homero_paradox_01-6", at_list=[scene_delay])

    professor2 "Homero debería realizar una serie infinita de tareas antes de llegar al final del camino."

    window auto

    show ch_professor_smile at right_resize with dissolve

    professor2 "{b}Zenón{/b} sostenía que, bajo esta premisa, a Homero le resultaría imposible terminar el trayecto."
    professor2 "Le resultaría imposible porque necesitaría un tiempo infinito para recorrer un número infinito de tramos."
    professor2 "Esta {b}paradoja{/b} se denomina {b}{i}de la dicotomía{/i}{/b} porque trata sobre dividir de forma repetida una distancia en dos partes."
    professor2 "En la práctica, sabemos que Homero podrá alcanzar el final del camino sin problema alguno."
    professor2 "Se llama {b}paradoja{/b} a este tipo de planteamientos porque lo que proponen parece contradecir lo observado en la vida cotidiana."
    professor2 "Hay muchas formas desde las que abordar este problema, algunas más sencillas y otras más complejas."

    pause 0.5
    hide ch_professor_smile with dissolve
    pause 0.5
    show black with dissolve
    pause 1
    scene bg_aristotle_statue with dissolve
    pause 1
    show ch_professor_happy at center_resize with dissolve 

    professor2 "¿Qué os parece si intentamos afrontarlo utilizando la teoría del {b}movimiento rectilíneo uniforme{/b}?"

    menu:
        with dissolve
        "¡Adelante!":
            pass
        "Si no hay otro remedio...":
            pass

    pause 0.5
    hide ch_professor_happy with dissolve
    pause 0.5
    scene black with dissolve
    stop music fadeout 2

    pause 3

    play sound "audio/celesta.ogg"
    $ first_block_case01 = "La reflexión y el análisis profundo son dos herramientas esenciales para el desempeño del trabajo de detective."
    $ second_block_case01 = "Utiliza tus conocimientos sobre el movimiento rectilíneo uniforme (MRU) para resolver la paradoja de la dicotomía."
    show screen case_cover(1, first_block_case01, second_block_case01)

    pause
    hide screen case_cover with dissolve

    scene black with dissolve

    play music ["<silence 1.0>", "audio/puzzle.ogg"] volume 1

    pause 2.5

    define case01_desc = "Caso 1. Atenas, siglo 8 antes de Cristo. Era una mañana soleada en la polis griega. Homero partió desde su casa hacia el ágora, acompañado por su lazarillo debido a la ceguera que sufría. Iniciaron el camino a una velocidad constante de 3 kilómetros por hora. Si el ágora se encontraba a 800 metros en línea recta de la casa de Homero, ¿cuántos minutos tardaron en llegar?"

    python:
        if persistent.font_label == "global":
            background_case01_path = "images/backgrounds/bg_case01_alt.png"
        else:
            background_case01_path = "images/backgrounds/bg_case01.png"

    call screen case(bg_case=background_case01_path,
        case_number="1", true_answer=16.06, case_desc=case01_desc) with dissolve

    stop music

    play sound "audio/correct_answer.ogg"

    $ renpy.pause(3, hard=True)

    play music "audio/celebration.ogg" noloop
    image celebration = Movie(play="videos/confetti.webm", size=(1920, 1080), loop=False)
    show celebration with flash_shorter

    pause 1

    show ch_professor_smile at center_resize
    with dissolve
    professor "¡Bien hecho, {b}[player_name]{/b}!"

    pause 1

    hide ch_professor_smile with dissolve

    pause 1

    scene black with dissolve

    play music "audio/paradox_01_intro.ogg" volume 1

    pause 1

    scene bg_blackboard with dissolve

    pause 1

    professor "Ahora os voy a mostrar cómo lo habría hecho yo."
    professor2 "Puede que mi desarrollo tenga algún paso diferente al vuestro, pero no importa."
    professor2 "Diferentes detectives, con sus diferentes maneras de proceder, pueden llegar a la misma solución siguiendo caminos distintos."
    professor2 "Lo importante es que esos caminos sean correctos conceptualmente."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case01_solution_1_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case01_solution_1", at_list=[scene_delay])


    professor2 "Primero, escribo la ecuación del {b}MRU{/b} y los datos que conocemos acompañados de sus unidades."
    professor2 "Tener toda la información disponible a la vista me ayuda mucho a la hora de resolver un caso."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case01_solution_2_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case01_solution_2", at_list=[scene_delay])

    professor2 "Para evitar cometer errores, trabajo con las unidades del {b}Sistema Internacional{/b}."
    professor2 "Por esta razón expreso la velocidad en m/s."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case01_solution_3_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case01_solution_3", at_list=[scene_delay])

    professor2 "Utilizando la ecuación del {b}MRU{/b}, despejo el tiempo y sustituyo los datos de los que disponemos."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case01_solution_4_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case01_solution_4", at_list=[scene_delay])

    professor2 "Finalmente, expreso el tiempo en minutos, tal y como se pedía en la descripción del caso."

    pause 1

    show ch_professor_smile at center_resize with dissolve
    professor2 "Justo como esperábamos, Homero pudo llegar sin ningún problema al ágora."
    professor2 "No necesitó un tiempo infinito, solamente unos pocos minutos.{w} ¡Qué suerte!"

    pause 0.5
    hide ch_professor_smile with dissolve
    pause 1
    scene black with dissolve
    stop music fadeout 2

    pause 3

    return

