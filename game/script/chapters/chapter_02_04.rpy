
label chapter_02_04:
    stop music
    stop sound
    stop ambient
    scene black

    pause 1
    play music "audio/dos-amigos-y-simplicio.ogg" volume 0.6
    pause 1

    professor "Como os dije, {b}Galileo{/b} recopiló sus trabajos sobre el movimiento en una obra bajo el nombre de {b}{i}Discursos acerca de dos nuevas ciencias{/i}{/b}."

    scene bg_galileo_parra at slowzoom with dissolve
    pause 1

    professor2 "En ella no solo incluye sus investigaciones sobre la {b}cinemática{/b}, también estudios sobre la resistencia de materiales y la mecánica en general."
    professor2 "Los {b}{i}Discursos{/i}{/b} introducen las primeras definiciones formales del {b}MRU{/b} y el {b}MRUA{/b}."
    professor2 "La manera de exponer sus investigaciones en este trabajo es algo peculiar."
    professor2 "En los {b}{i}Discursos{/i}{/b}, las explicaciones y resultados de {b}Galileo{/b} se muestran a través del diálogo entre los personajes que intervienen en la obra."
    professor2 "{b}Galileo{/b} adoptó el mismo formato de exposición y los mismos personajes utilizados previamente en su {b}{i}Diálogo sobre los dos máximos sistemas del mundo{/i}{/b},..."
    professor2 "... en la que hacía una defensa del modelo {b}copernicano{/b} del universo."
    professor2 "Los tres personajes que intervienen son {b}Salviati{/b}, {b}Sagredo{/b} y {b}Simplicio{/b}."

    pause 1
    scene black with dissolve
    pause 1

    scene bg_salviati-sagredo-simplicio at vertical_panning2 with dissolve
    pause 1

    professor2 "{b}Salviati{/b} es el científico moderno e inteligente que generalmente representa en esta obra las ideas del propio {b}Galileo{/b}."
    professor2 "El personaje está basado en {b}Filippo Salviati{/b}, hijo de una familia noble florentina y gran amigo de nuestro científico."
    professor2 "Por otro lado, {b}Sagredo{/b} representa a la persona culta, de ideas claras y agudas, pero sin estar especializada en matemáticas."
    professor2 "Desconoce los últimos descubrimientos científicos, pero contribuye al desarrollo de la conversación haciendo observaciones y preguntas inteligentes a {b}Salviati{/b}."
    professor2 "El personaje es un homenaje a {b}Giovanni Fancesco Sagredo{/b}, un antiguo estudiante de {b}Galileo{/b} y amigo muy cercano."
    professor2 "En último lugar tenemos a {b}Simplicio{/b}, que reproduce a menudo ideas propias de los {b}aristotélicos{/b}."
    professor2 "Es refutado en ocasiones por {b}Sagredo{/b}, que evidencia las contradicciones en la que {b}Simplicio{/b} cae."
    professor2 "No representa como tal a una persona real, pero sí a aquellas defensoras de las ideas que eran combatidas por {b}Galileo{/b}."
    professor2 "Este personaje le granjeó muchos enemigos en la época, sobre todo a raíz de la publicación del {b}{i}Diálogo sobre los dos máximos sistemas del mundo{/i}{/b}."

    pause 1
    scene black with dissolve
    pause 1
    scene bg_venezia with dissolve
    pause 1

    professor2 "En los {b}{i}Discursos{/i}{/b}, los viejos amigos {b}Sagredo{/b}, {b}Salviati{/b} y {b}Simplicio{/b} se reúnen tras visitar los arsenales de Venecia."
    professor2 "El encuentro se prolonga durante cuatro días, que utilizan para hablar sobre varios temas acerca de la naturaleza."
    professor2 "Los dos primeros días podrían considerarse como una introducción de algunos temas que se tratarán de manera más extensa en los dos últimos días."
    professor2 "En el segundo día también se recogen las ideas acerca de la resistencia de materiales."
    professor2 "Las dos últimas jornadas se centran en el estudio del movimiento, siendo el tercer día considerado como el más importante."
    professor2 "En él se muestran todos los resultados de las investigaciones sobre el movimiento realizadas por {b}Galileo{/b} en los 30 años previos."
    professor2 "¿Recordáis los manuscritos de {b}{i}De Motu{/i}{/b}?{w} Ahí empezó todo."
    professor2 "El último día está dedicado al estudio de los {i}movimientos violentos{/i}, así llamados por {b}Galileo{/b}."
    professor2 "Se trata de los movimientos descritos por proyectiles como las balas de cañón."

    pause 1
    stop music fadeout 2
    scene black with dissolve
    pause 2.5
    play music "audio/discursos.ogg" volume 0.7
    pause 1
    scene bg_brown_texture with dissolve
    pause 1

    show ch_professor_smile at left_resize with dissolve
    pause 0.5

    professor2 "En sus demostraciones matemáticas, {b}Galileo{/b} no emplea en ningún momento el simbolismo que hoy conocemos."
    professor2 "Y no lo hace pese a ser contemporáneo al máximo desarrollo del álgebra."
    professor2 "Ignorando las nuevas corrientes que se estaban imponiendo en la época, sigue un método expositivo al estilo de su adorado {b}Euclides{/b}."
    professor2 "{b}Euclides{/b} fue un matemático griego que vivió en el siglo III a. C. y que es considerado como el padre de la geometría."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_euclides_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_euclides", at_list=[scene_delay])

    professor2 "El único recurso que utiliza {b}Galileo{/b} es la representación gráfica de las magnitudes espacio, tiempo y velocidad."
    professor2 "Aquí tenéis un ejemplo del diagrama que utilizó para acompañar la demostración de la relación entre el espacio recorrido y el cuadrado del tiempo en el {b}MRUA{/b}."
    professor2 "En esta figura, los segmentos HL, HM, etc., son los espacios recorridos por un cuerpo que parte desde el reposo en los tiempos AD, AE, etc."
    professor2 "Las velocidades en los instantes D, E, etc., son representadas por los segmentos DO, EP, etc."

    pause 1
    hide ch_professor_smile with dissolve
    pause 1
    scene black with dissolve
    pause 1
    scene bg_galileo-publico at slowzoom with dissolve
    pause 1

    professor2 "Es importante que entendamos que muchos de los recursos de los que disponemos hoy en día, tanto matemáticos como instrumentales,..."
    professor2 "... o bien no estaban firmemente instaurados en la época de {b}Galileo{/b}, o directamente no existían."
    professor2 "Los {b}{i}Discursos{/i}{/b} están plagados de experimentos ingeniosos para compensar la falta de instrumentos de precisión en la medición del tiempo."
    professor2 "Por ejemplo, {b}Salviati{/b} cuenta cómo se puede emplear, con este objetivo, un depósito de agua con un pequeño agujero en su parte inferior."
    professor2 "Pesando el agua que salía en dos tramos diferentes del movimiento, se podía calcular la razón entre los intervalos de tiempo correspondientes..."
    professor2 "... solamente dividiendo las masas de agua entre sí."
    professor2 "¡Qué maravilla!"

    pause 1
    show ch_professor_happy at center_resize with dissolve
    pause 0.5

    professor2 "Es necesario aunar inteligencia, ingenio y trabajo para alcanzar por primera vez las conclusiones a las que llegó {b}Galileo{/b}."
    professor2 "Pero el mérito que le debemos atribuir es aún mayor cuando conocemos las limitaciones desde las que partían sus estudios."

    pause 1
    hide ch_professor_happy with dissolve
    pause 1
    scene black with dissolve
    pause 1
    scene bg_elder_galileo with dissolve
    pause 1

    professor2 "En los {b}{i}Discursos{/i}{/b} hay continuas referencias a la necesidad de matematizar la naturaleza para ser capaces de comprenderla."
    professor2 "{b}Galileo{/b} también invirtió muchos esfuerzos en poner en valor la aplicación práctica en el estudio de la ciencia."
    professor2 "Esto era algo que se oponía frontalmente a los razonamientos metafísicos propios de los filósofos de la época."
    professor2 "A partir de entonces,{w} la {b}física{/b}{w} {b}cambió para siempre{/b}."

    pause 1
    show ch_professor_smile at center_resize with dissolve
    pause 0.5

    professor2 "Estamos llegando al final de la clase, pero da tiempo a un último caso."
    professor2 "De nuevo, está dividido en dos partes."

    hide ch_professor_smile
    show ch_professor_grin at center_resize

    professor2 "De conseguir resolverlo, habréis superado el penúltimo de los escalones previos a recibir el título de detectives especialistas en {b}cinemática{/b}." 
    professor2 "Estáis muy cerca de conseguirlo, ¡adelante!" 

    pause 0.5
    
    hide ch_professor_grin with dissolve
    
    pause 1

    stop music fadeout 2
    scene black with dissolve

    pause 2.5

    play sound "audio/celesta.ogg"
    $ first_block_case05 = "La ausencia de tecnologías y herramientas conceptuales dificulta desarrollo científico, pero también agudiza el ingenio."
    $ second_block_case05 = "Utiliza tus conocimientos sobre el movimiento rectilíneo uniformemente acelerado (MRUA) para resolver el siguiente caso sobre el movimiento en un plano inclinado."
    show screen case_cover(5, first_block_case05, second_block_case05)

    pause
    hide screen case_cover with dissolve

    play music ["<silence 1.0>", "audio/puzzle.ogg"] volume 1

    pause 2

    define case05_01_desc = "Galileo se encuentra realizando un experimento sobre movimiento rectilíneo uniformemente acelerado con móviles desplazándose a lo largo de un plano inclinado. Deja caer un móvil en reposo desde la parte más alta del plano inclinado. El móvil llega al final del plano con una velocidad de 27 kilómetros por hora al cabo de 2.5 segundos. ¿Qué aceleración tenía el movimiento? Exprésala en metros por segundo al cuadrado."

    python:
        if persistent.font_label == "global":
            background_case05_01_path = "images/backgrounds/bg_case05-01_alt.png"
        else:
            background_case05_01_path = "images/backgrounds/bg_case05-01.png"

    call screen case(bg_case=background_case05_01_path,
        case_number="5a", true_answer=3, case_desc=case05_01_desc) with dissolve

    stop music

    play sound "audio/correct_answer.ogg"

    pause 2

    play music "audio/discursos.ogg" volume 0.6
    pause 1
    show ch_professor_happy at center_resize
    with dissolve
    professor "¡{b}[player_name]{/b}, has conseguido resolver la primera parte! ¡Estupendo!"

    pause 0.5
    hide ch_professor_happy with dissolve
    pause 0.5
    scene black with dissolve
    pause 0.5

    scene bg_blackboard with dissolve

    pause 1

    professor "Aquí tenéis mi resolución para este apartado."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case05-01_solution_1_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case05-01_solution_1", at_list=[scene_delay])

    professor2 "¿Qué viene primero?"
    professor2 "Exacto, los datos del que conocemos del caso."
    professor2 "De momento solo necesito la ecuación de la velocidad, por eso no escribo la de la posición."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case05-01_solution_2_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case05-01_solution_2", at_list=[scene_delay])

    professor2 "La velocidad inicial no me la proporcionan en unidades del {b}Sistema Internacional{/b}."
    professor2 "Por esta razón hago la conversión."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case05-01_solution_3_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case05-01_solution_3", at_list=[scene_delay])

    professor2 "El móvil parte desde el reposo, así que elimino el término nulo en la ecuación de la velocidad."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case05-01_solution_4_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case05-01_solution_4", at_list=[scene_delay])

    professor2 "Si despejo la incógnita de en esa ecuación, obtengo la aceleración del móvil."

    pause 1

    show ch_professor_smile at center_resize with dissolve
    professor2 "La segunda parte del caso es algo más complicada."

    hide ch_professor_smile
    show ch_professor_grin at center_resize
    professor2 "¿Podréis resolverla?"

    pause 0.5

    hide ch_professor_grin with dissolve

    pause 1

    stop music fadeout 2
    scene black with dissolve

    pause 2

    play music ["<silence 1.0>", "audio/puzzle.ogg"] volume 1

    pause 2

    define case05_02_desc = "Galileo se encuentra realizando un experimento sobre movimiento rectilíneo uniformemente acelerado con móviles desplazándose a lo largo de un plano inclinado. Deja caer un móvil en reposo desde la parte más alta del plano inclinado. El móvil llega al final del plano con una velocidad de 27 kilómetros por hora y continúa moviéndose con una aceleración de -5 metros por segundo al cuadrado hasta pararse. ¿Qué distancia recorre desde que llega al final del plano hasta que se para? Exprésala en metros."

    python:
        if persistent.font_label == "global":
            background_case05_02_path = "images/backgrounds/bg_case05-02_alt.png"
        else:
            background_case05_02_path = "images/backgrounds/bg_case05-02.png"

    call screen case(bg_case=background_case05_02_path,
        case_number="5b", true_answer=5.63, case_desc=case05_02_desc) with dissolve

    stop music

    play sound "audio/correct_answer.ogg"

    $ renpy.pause(3, hard=True)

    play music "audio/celebration.ogg" noloop
    image celebration = Movie(play="videos/confetti.webm", size=(1920, 1080), loop=False)
    show celebration with flash_shorter

    pause 1

    show ch_professor_happy at center_resize
    with dissolve
    professor "¡Lo tienes, {b}[player_name]{/b}! ¡Es increíble!"

    pause 0.5

    hide ch_professor_happy with dissolve

    pause 1

    scene black with dissolve

    play music "audio/discursos.ogg" volume 0.6

    pause 1

    scene bg_blackboard with dissolve

    pause 1

    professor "Sé que no es necesario que os muestre mi resolución, pero lo haré por si habéis seguido un camino igualmente válido pero diferente al mío."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case05-02_solution_1_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case05-02_solution_1", at_list=[scene_delay])

    professor2 "Escribo los datos del caso en primer lugar."
    professor2 "Ahora es necesario emplear tanto la ecuación de la posición como la de la velocidad."
    professor2 "La velocidad final de la primera parte del caso es ahora la inicial en esta segunda parte."
    professor2 "Como ya la convertí a unidades del {b}Sistema Internacional{/b}, utilizo ese mismo valor."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case05-02_solution_2_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case05-02_solution_2", at_list=[scene_delay])

    professor2 "Como el móvil acaba parándose, su velocidad final es nula."
    professor2 "Sabiendo esto, planteo el sistema de ecuaciones y elimino los términos nulos."
    professor2 "Tengo un sistema de dos ecuaciones de dos incógnitas: el tiempo y la posición."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case05-02_solution_3_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case05-02_solution_3", at_list=[scene_delay])

    professor2 "Trabajo con la ecuación de la velocidad para despejar el tiempo."

    hide bg_case05-02_solution_2
    hide bg_case05-02_solution_2_alt
    hide bg_case05-02_solution_3
    hide bg_case05-02_solution_3_alt
    with dissolve
    pause 1


    python:
        if persistent.font_label == "global":
            renpy.show("bg_case05-02_solution_4_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case05-02_solution_4", at_list=[scene_delay])

    professor2 "Si sustituyo la expresión del tiempo en la ecuación de la posición, obtengo una expresión para el desplazamiento del móvil."


    python:
        if persistent.font_label == "global":
            renpy.show("bg_case05-02_solution_5_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case05-02_solution_5", at_list=[scene_delay])

    professor2 "Sustituyendo los datos en esa expresión, puedo calcular lo que se me pedía."

    pause 1

    show ch_professor_smile at center_resize with dissolve
    professor2 "Durante esta clase habéis podido comprobar que la aceleración puede tomar tanto signos positivos como negativos."
    professor2 "El signo depende del criterio que se utilice en cada caso."
    professor2 "En los movimientos verticales, es muy común utilizar el signo negativo para la aceleración de la gravedad."
    professor2 "En los horizontales, la aceleración suele ser positiva si acelera el movimiento y negativa si lo frena."
    professor2 "Sin embargo, esto no son más que convenciones."
    professor2 "Podemos utilizar criterios opuestos a los anteriores siempre que se pueda justificar conceptualmente."

    pause 1

    hide ch_professor_smile
    show ch_professor_happy at center_resize

    professor2 "¡Qué maravilla! ¡Domináis a la perfección el {b}MRUA{/b}!"

    hide ch_professor_happy
    show ch_professor_grin at center_resize
    professor2 "¡Os falta muy poco para conseguir vuestro objetivo!"

    pause 1

    hide ch_professor_grin with dissolve

    pause 1

    stop music fadeout 2
    scene black with dissolve

    pause 2.5

    return
