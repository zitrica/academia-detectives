label chapter_02_02:
    stop music
    stop sound
    stop ambient
    scene black

    play music "audio/pisa_tower.ogg" volume 1.2

    pause 1

    scene bg_florence with dissolve
    pause 1
    show ch_professor_smile at left_resize with dissolve
    pause 0.5

    professor "{b}Galileo{/b} mostró una gran devoción por las matemáticas desde muy joven."
    professor2 "Sin embargo, su afán por los números estaba en conflicto con los deseos de su padre."
    professor2 "Él esperaba de su hijo que se formase para trabajar como médico."
    professor2 "Así fue como el joven {b}Galileo{/b} inició sus estudios de medicina en la universidad de Pisa en 1581."
    professor2 "Sin embargo, dedicaba la mayor parte del tiempo a las matemáticas y la filosofía."
    professor2 "Desatendió la medicina hasta el punto de que, cuatro años después, abandonó la universidad sin conseguir el título."
    professor2 "Regresó a la residencia de sus padres, afincados por entonces en Florencia."
    professor2 "Allí comenzó a ganarse la vida dando clases de matemáticas."
    professor2 "Mientras tanto, continuaba profundizando en el estudio de la disciplina."
    professor2 "Incluso llegó a publicar varios trabajos académicos y a dar alguna conferencia."

    hide ch_professor_smile with dissolve
    pause 1
    scene black with dissolve
    pause 1

    scene bg_pisa_2 at slowzoom with dissolve
    pause 1
    
    professor2 "En 1589 regresó a Pisa para trabajar como profesor de matemáticas en la universidad."
    professor2 "Es en esa época cuando empezó a abordar seriamente el tema del movimiento de los cuerpos."
    professor2 "Los resultados de sus experimentos le condujeron a la redacción de unos manuscritos reunidos bajo el título de {b}{i}De Motu{/i}{/b}."
    professor2 "Apuntad el nombre de esta obra."
    professor2 "{b}Galileo{/b} recopilaba en ella los resultados obtenidos en sus experimentos sobre la caída libre de los cuerpos."
    professor2 "También sobre el movimiento a lo largo de un plano inclinado y el movimiento de los proyectiles."

    pause 0.5
    show ch_professor_smile at center_resize with dissolve
    pause 0.5

    professor2 "{b}{i}De Motu{/i}{/b}, que aunaba observación, medida y demostración matemática, no se publicó en aquel momento porque los resultados no satisfacían a {b}Galileo{/b}."

    professor2 "Esta obra inédita constituye el germen de lo que treinta años después fue su trabajo cumbre acerca del movimiento:"
    professor2 "{b}{i}Discursos acerca de dos nuevas ciencias{i}{/b}."

    pause 0.5
    hide ch_professor_smile with dissolve
    pause 1
    stop music fadeout 2
    scene black with dissolve
    pause 2
    play music "audio/caida_torre.ogg" volume 0.7
    pause 1
    scene bg_galileo with dissolve

    professor2 "Entre los ensayos de caída libre que contribuyeron al desarrollo de {b}{i}De Motu{/i}{/b}, es posible que se encuentre la famosa demostración de la {b}torre inclinada de Pisa{/b}."
    professor2 "Digo que {i}es posible{/i} porque realmente no existe ningún documento escrito por {b}Galileo{/b} describiendo tal acontencimiento."
    professor2 "Pese a ser considerado por algunas personas como una leyenda, está recogido por su biógrafo {b}Vincenzo Viviani{/b}."
    professor2 "En la biografía de {b}Galileo{/b}, {b}Viviani{/b} aseguraba transmitir fielmente lo que le contó el científico durante los últimos años de su vida."
    professor2 "Según el biógrafo, {b}Galileo{/b} sí realizó una demostración de caída libre de objetos frente a sus estudiantes y parte del profesorado de la universidad."
    professor2 "Los antecesores intelectuales de {b}Galileo{/b} aceptaban la tesis {b}aristotélica{/b} de que los cuerpos más pesados caían más deprisa cuanto mayor era su peso."
    professor2 "Esta idea continuaba formando parte del pensamiento dominante en la época."
    professor2 "Sin embargo, {b}Galileo{/b} pensaba que esta afirmación era absurda."
    professor2 "Si fuera así, una bala de cañón de cinco kilos caería diez veces más rápido que una bala de mosquete de medio kilo."
    professor2 "Podemos hacer el ejercicio de imaginar la demostración de la {b}torre de Pisa{/b}, se haya dado realmente o no."

    pause 1
    scene black with dissolve
    pause 1

    scene bg_brown_texture with dissolve
    pause 0.5
    show bg_pisa-experiment_1 at scene_delay with dissolve 
    professor2 "Nos encontramos en la cima de la {b}torre inclinada de Pisa{/b}."
    show bg_pisa-experiment_2 at scene_delay with dissolve 
    professor2 "Disponemos de dos balas, una de cañón con masa de cinco kilos y otra de mosquete con masa de medio kilo."
    professor2 "Ambas están fabricadas además del mismo material."
    show bg_pisa-experiment_3 at scene_delay with dissolve 
    show bg_pisa-experiment_4 at scene_delay2 with dissolve 
    professor2 "En la demostración, estos dos objetos parten en reposo y a la vez desde el mismo punto en lo alto de la torre."
    show bg_pisa-experiment_5 at scene_delay with dissolve 
    professor2 "Bajo estas condiciones, {b}Galileo{/b} sostenía que las balas tocarían el suelo en el mismo momento con la misma velocidad."

    pause 1
    show ch_professor_happy at left_resize with dissolve
    professor2 "Con sus experimentos sobre la caída libre de los cuerpos, {b}Galileo{/b} estuvo merodeando alrededor del concepto de {b}gravedad{/b}."
    professor2 "Sin embargo, no llegó a esbozarlo. A él siempre le interesó más {b}cómo{/b} se producía el movimiento y no su {b}causa{/b}."
    professor2 "No fue hasta los trabajos de {b}Isaac Newton{/b} publicados a finales del siglo XVII cuando se presentó este concepto de la manera en la que hoy lo conocemos."

    hide ch_professor_happy
    show ch_professor_smile at left_resize

    professor2 "La demostración de la {b}torre de Pisa{/b} me ha dado una idea."

    hide ch_professor_smile
    show ch_professor_grin at left_resize

    professor2 "Me pregunto qué tal se os dará el siguiente caso..."

    pause 1
    
    hide ch_professor_grin with dissolve
    
    pause 1

    stop music fadeout 2
    scene black with dissolve
    pause 3

    play sound "audio/celesta.ogg"
    $ first_block_case03 = "La observación y la experimentación son indispensables para el desarrollo de hipótesis y teorías científicas."
    $ second_block_case03 = "Utiliza tus conocimientos sobre el movimiento rectilíneo uniformemente acelerado (MRUA) para afrontar el movimiento la caída libre desde la torre de Pisa."
    show screen case_cover(3, first_block_case03, second_block_case03)

    pause
    hide screen case_cover with dissolve

    scene black with dissolve


    play music ["<silence 1.0>", "audio/puzzle.ogg"] volume 1

    pause 2

    define case03_01_desc = "Galileo, desde la cima de la torre de Pisa, se dispone a realizar uno de las demostraciones que ayudarán a respaldar su teoría sobre el movimiento en caída libre. Con la ayuda de varios estudiantes, deja caer una bala de cañón y otra de mosquete, ambas del mismo material, desde la misma posición, a la vez y en reposo. Las balas tocan el suelo en el mismo instante al cabo de 3.26 segundos, aproximadamente. Si partieron desde una altura de 52 metros, ¿qué aceleración ha tenido el movimiento? Exprésala en metros por segundo al cuadrado."

    python:
        if persistent.font_label == "global":
            background_case03_01_path = "images/backgrounds/bg_case03-01_alt.png"
        else:
            background_case03_01_path = "images/backgrounds/bg_case03-01.png"

    call screen case(bg_case=background_case03_01_path,
        case_number="3", true_answer=-9.78, case_desc=case03_01_desc) with dissolve

    stop music

    play sound "audio/correct_answer.ogg"

    $ renpy.pause(3, hard=True)

    play music "audio/celebration.ogg" noloop
    image celebration = Movie(play="videos/confetti.webm", size=(1920, 1080), loop=False)
    show celebration with flash_shorter

    pause 1

    show ch_professor_happy at center_resize
    with dissolve
    professor "¡Lo has hecho muy bien, {b}[player_name]{/b}! ¡Excelente!"

    pause 0.5

    hide ch_professor_happy with dissolve

    pause 1

    scene black with dissolve

    play music "audio/pisa_tower.ogg" volume 1.2

    pause 1

    scene bg_blackboard with dissolve

    pause 1

    professor "Como siempre, voy a mostrar a continuación mi resolución del caso."
    professor2 "Recordad que el hecho de que mi camino difiera algo del vuestro no quiere decir que el que habéis escogido sea erróneo."
    professor2 "Lo importante es que se comprendan y se apliquen las leyes físicas correctamente."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case03-01_solution_1_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case03-01_solution_1", at_list=[scene_delay])

    professor2 "En primer lugar, escribo la ecuación de la posición del {b}MRUA{/b} y los datos que conocemos."
    professor2 "Como las balas realizan exactamente el mismo movimiento, toda la información de la que disponemos se aplica a ambas."
    professor2 "Si consideramos que la posición del suelo es igual a cero, el cambio en la posición es negativo porque la posición inicial es positiva."

    python:
        if persistent.font_label == "global":
            renpy.show("bg_case03-01_solution_2_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case03-01_solution_2", at_list=[scene_delay])

    professor2 "Como las balas parten desde el reposo, la velocidad inicial es nula."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case03-01_solution_3_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case03-01_solution_3", at_list=[scene_delay])

    professor2 "Eliminamos los términos nulos de la ecuación de la posición."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case03-01_solution_4_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case03-01_solution_4", at_list=[scene_delay])

    professor2 "Por último, despejamos la incógnita de la aceleración y sustituimos los datos."
    professor2 "Como todas las magnitudes están en unidades del {b}Sistema Internacional{/b}, no es necesario hacer ninguna transformación."

    pause 1

    show ch_professor_smile at center_resize with dissolve
    professor2 "El valor de la aceleración que acabamos de obtener es el de la {b}aceleración de la gravedad{/b}."
    professor2 "En realidad, su valor medio sobre la superficie terrestre es de -9.81 m/s2."
    professor2 "Este valor de la aceleración es el mismo para todos los cuerpos, independientemente de su masa."
    professor2 "Por esa razón caen las balas a la misma velocidad."

    pause 0.5

    hide ch_professor_smile with dissolve

    pause 1
    
    scene black with dissolve
    stop music fadeout 2

    pause 2

    return
