label preamble:
    $ quick_menu = False
    stop music
    stop sound
    stop ambient
    play sound "audio/spotlight.ogg"

    scene black

    $ renpy.pause(3, hard=True)
    $ quick_menu = True

    narrator "Hace unas semanas recibiste la noticia que tan ansiosamente estabas esperando."
    narrator "Tras mucho tiempo de esfuerzo y haber superado las pruebas de selección, te fue concedida una beca para estudiar en la célebre y reconocida {b}Academia [academy_name]{/b}."
    narrator "Esta academia ha sido, y sigue siendo, la cuna de las mentes más brillantes del mundo detectivesco."
    narrator "Desde que tienes uso de razón, siempre has soñado con ser detective profesional."
    narrator "Estás a un paso de conseguirlo, pero antes deberás acreditar que tienes los conocimientos necesarios para serlo."
    narrator "Ha llegado el momento de demostrar lo que vales."

    pause 2
    
    play music "audio/welcome.ogg" volume 0.5
    play ambient "audio/sample_crowd.ogg" loop volume 0.5

    pause 2

    narrator "Primer día en la {b}Academia [academy_name]{/b}..."

    scene bg_academy_entrance with dissolve

    pause 1

    show ch_student_happy at center_resize with hpunch
    unknown "¡HOLA!"
    unknown2 "Es tu primer día, ¿verdad?"

    hide ch_student_happy
    show ch_student_smile at center_resize

    unknown2 "Lo suponía, no te he visto antes por aquí."
    unknown2 "La profesora nos dijo que alguien iba a unirse a la academia esta semana."
    unknown2 "Debes de ser tú."
    unknown2 "¿Cómo te llamas?"

    python:
        player_name = renpy.input("Utiliza el teclado para escribir tu nombre:", length=32)
        player_name = player_name.strip()

        while player_name == "":
            player_name = renpy.input("Utiliza el teclado para escribir tu nombre:", length=32)
            player_name = player_name.strip()

    unknown2 "¡Encantado de conocerte, [player_name]!"

    hide ch_student_smile
    show ch_student_happy at center_resize

    student "Mi nombre es {b}[student_name]{/b}. Estamos en la misma clase."
    student2 "Te doy la bienvenida a la {b}Academia [academy_name]{/b}, el centro de formación de detectives más prestigioso del mundo."
    student2 "Estoy seguro de que..."

    hide ch_student_happy
    show ch_student_shock at center_resize

    play sound "audio/sample_bell.ogg" volume 1.1

    pause 3

    student2 "Oh, es la hora de entrar a clase."
    student2 "¡Rápido, no debemos llegar tarde!"

    window hide
    hide ch_student_shock
    with dissolve

    pause 2

    scene bg_black with dissolve

    stop music fadeout 2
    stop ambient fadeout 2

    pause 2

    play music "audio/professor.ogg" volume 0.5

    pause 1

    narrator "En el aula..."

    scene bg_academy_classroom with dissolve

    pause 1

    show ch_professor_happy at center_resize with dissolve
    professor "¡Buenos días a todo el mundo!"
    professor2 "¡Comienza una nueva jornada en la {b}Academia [academy_name]{/b}!"
    professor2 "Antes de empezar la sesión de hoy me gustaría presentaros a alguien."
    professor2 "Como recordaréis, la semana pasada os dije que habría una nueva incorporación a nuestra clase."
    professor2 "¡Dadle la bienvenida a {b}[player_name]{/b}!"
    classmates "¡Holaaaaa!{w} \[...\]{w} ¡Encantado de conocerte!{w} \[...\]{w} ¡Estábamos impacientes por tu llegada!"

    hide ch_professor_happy
    show ch_professor_smile at center_resize

    professor "Mi nombre es {b}[professor_name_2]{/b}."
    professor2 "Soy la directora de la academia. También me encargo de impartir la asignatura de {b}Física y Química{/b}."
    professor2 "Siempre he sentido una gran curiosidad por la resolución de puzles y problemas desde que era una niña."
    professor2 "Sumergirse en el estudio..."
    professor2 "Aprender nuevas disciplinas..."
    professor2 "Y articular los conocimientos de los que una dispone para encontrar la pieza clave ante un problema que a priori parece irresoluble..."
    professor2 "No encuentro nada más satisfactorio que despejar el sendero de la incertidumbre haciendo uso del razonamiento lógico."
    professor2 "Así lo hicieron nuestros antepasados, utilizando la {b}física{/b} y la {b}química{/b} entre sus herramientas."
    professor2 "Ahora es nuestro momento."
    professor2 "O mejor dicho...{w} Vuestro momento."
    professor2 "¿Qué opinas, [player_name]?"
    menu:
        with dissolve
        "¡Es nuestro momento!":
            pass
        "Estoy de acuerdo.":
            pass

    pause 0.1

    classmates "¡Así se habla!{w} \[...\]{w} ¡Bien dicho!{w} \[...\]{w} ¡Vamoooooooooos!"

    hide ch_professor_smile
    show ch_professor_grin at center_resize

    professor "¡Muy bien, esa es la actitud!"
    professor2 "Esa es la determinación que espero en quienes sueñan con convertirse en detectives."
    professor2 "Estamos a punto de embarcarnos en una expedición a través de la historia de una de las disciplinas más importantes de la {b}física{/b}."
    professor2 "Durante esta aventura podréis construir los cimientos necesarios para resolver multitud de problemas y misterios presentes en nuestro día a día."
    professor2 "De completarla satisfactoriamente, recibiréis el título de detectives especialistas en la materia..."
    professor2 "Aquella que se encarga del estudio del movimiento de los cuerpos."
    professor2 "¡Os doy la bienvenida al apasionante mundo de la {b}cinemática{/b}!"

    pause 1

    window hide
    hide ch_professor_grin
    with dissolve

    pause 1

    scene bg_black with dissolve

    stop sound fadeout 2
    stop music fadeout 2

    pause 3

    return
