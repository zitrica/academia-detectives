
label ending:
    stop music
    stop sound
    stop ambient
    scene black

    play music "audio/ending.ogg" volume 0.7

    pause 3

    scene bg_academy_classroom with dissolve
    pause 2
    show ch_professor_happy at center_resize with dissolve
    pause 1

    professor2 "¡Lo habéis logrado!"
    professor2 "Desde a la Antigua Grecia hasta la actualidad, habéis sido capaces de resolver todo tipo de casos utilizando las teorías del movimiento."

    hide ch_professor_happy
    show ch_professor_grin at center_resize

    professor2 "He de admitir que no estoy impresionada."
    professor2 "Esto es justo lo que esperaba de mis, ahora oficialmente, ¡detectives especialistas en {b}cinemática{/b}!"

    professor2 "¡Disfrutadlo, os lo habéis ganado!"
    pause 1
    hide ch_professor_grin with dissolve
    hide window with dissolve
    pause 2

    play sound "audio/diploma_level.ogg"
    show bg_diploma with flash_shorter

    $ renpy.pause(6, hard=True)

    scene black with dissolve 
    pause 2
    scene bg_brown_texture with dissolve
    python:
        if persistent.font_label == "global":
            renpy.show("bg_ending_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_ending", at_list=[scene_delay])
    pause 1
    $ renpy.pause(5, hard=True)
    scene black with dissolve
    stop music fadeout 2.5
    $ renpy.pause(3, hard=True)

    return

