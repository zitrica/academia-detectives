label chapter_01_03:
    stop music
    stop sound
    stop ambient
    scene black

    play music "audio/andronico.ogg"

    pause 1
    scene bg_aristotle_alexander with dissolve
    pause 1
    show ch_professor_smile at right_resize with dissolve
    pause 1

    professor "La obra que hoy conocemos como {b}{i}Física{/i}{/b} no se trata de un conjunto textos editados por {b}Aristóteles{/b}."
    professor2 "De hecho, estas lecciones ni siquiera fueron concebidas para ser publicadas en formato escrito."
    professor2 "Eran expuestas oralmente por {b}Aristóteles{/b} en el {b}Liceo{/b}, la escuela que fundó en Atenas tras acabar su periodo como maestro de Alejandro Magno."

    pause 1

    hide ch_professor_smile with dissolve

    pause 0.5

    scene black with dissolve

    pause 2 

    scene bg_aristotle_school at slowzoom with dissolve

    pause 1

    professor2 "La lecciones de {b}{i}Física{/i}{/b} fueron conservadas por los discípulos inmediatos del pensador."
    professor2 "En la actualidad, no tenemos plena certeza de que los textos que componen la obra actual procedan de los mismos manuscritos originales."
    professor2 "Recordad que {b}Aristóteles{/b} vivió en el siglo IV a. C., ¡han pasado 2400 años desde entonces!"
    professor2 "Lo que nos ha llegado hasta hoy es el resultado de numerosas modificaciones introducidas por las sucesivas copias de los manuscritos originales."
    professor2 "La primera edición en la que se basan estas copias es una recopilación de textos realizada por {b}Andrónico de Rodas{/b}, en Roma, durante el siglo I a. C."
    professor2 "Desde entonces, los textos han pasado por muchas manos y sus transcripciones han sido objeto de correcciones, conjeturas, adiciones, omisiones, etc."
    professor2 "Pese a todo, aunque existen estudios que cuestionan la autenticidad de la obra, hoy en día son unánimemente rechadados por las voces expertas en la materia."

    pause 1 

    show ch_professor_smile at center_resize with dissolve

    professor2 "Bien, ¿qué os parece si os presento otra de las {b}paradojas{/b} recogidas en {b}{i}Física{/i}{/b}?"
    professor2 "Vamos a ello."

    pause 1
    hide ch_professor_smile with dissolve

    pause 0.5

    scene black with dissolve
    stop music fadeout 2

    pause 2.5

    play music "audio/turtle.ogg" volume 0.7

    pause 1

    scene bg_achilles_statue with dissolve
    professor "Además de la {b}paradoja de la dicotomía{/b}, {b}{i}Física{/i}{/b} recopila otras ocho {b}paradojas{/b} adicionales."
    professor2 "Quizás las más conocida de todas sea la {b}paradoja de Aquiles y la tortuga{/b}, descrita también en el libro VI."

    pause 1

    scene black with dissolve

    pause 1

    show bg_brown_texture at scene_delay
    show bg_achilles_paradox_ground at scene_delay
    show sp_achilles at scene_delay2, achilles_pos1 
    show sp_turtle at scene_delay2, turtle_pos1 
    professor2 "Imaginad que {b}Aquiles{/b}, héroe de la guerra de Troya, disputa una carrera contra una tortuga."
    show bg_achilles_paradox_rule1 at scene_delay
    professor2 "Ambos parten a velocidades diferentes pero constantes, comenzando la tortuga desde una posición más avanzada."
    hide bg_achilles_paradox_rule1 with dissolve
    show sp_achilles at achilles_pos1_to_pos2
    professor2 "Durante un intervalo de tiempo dado, {b}Aquiles{/b} corre hasta alcanzar la posición inicial desde la que partió la tortuga."
    show sp_turtle at turtle_pos1_to_pos2
    professor2 "Sin embargo, durante ese mismo tiempo, la tortuga también avanza, aunque recorriendo una menor longitud."
    show bg_achilles_paradox_rule2 at scene_delay
    professor2 "En este momento, la posición que separa a {b}Aquiles{/b} de la tortuga es menor que al comienzo de la carrera."
    hide bg_achilles_paradox_rule2 with dissolve
    show sp_achilles at achilles_pos2_to_pos3
    professor2 "Por tanto, necesitará menos tiempo para alcanzar la posición actual en la que se encuentra el animal."
    show sp_turtle at turtle_pos2_to_pos3
    show bg_achilles_paradox_rule3 at scene_delay2
    professor2 "Pero, igual que antes, ese mismo tiempo es utilizado por la tortuga para continuar avanzando un poco más."
    professor2 "Veis por dónde voy, ¿verdad?"
    pause 1
    show ch_professor_smile at left_resize with dissolve

    professor2 "Por más que nuestro héroe alcance en cada instante la posición desde la que partía la tortuga en el instante anterior..."
    professor2 "... la tortuga, habiendo avanzado durante ese tiempo, se encontrará siempre por delante de {b}Aquiles{/b}."
    professor2 "{b}Aquiles{/b} se irá aproximando cada vez más a la tortuga, pero siempre existirá una pequeña distancia entre ambos que evitará que el guerrero logre alcanzarla."
    professor2 "Tal y como sucedía con la anterior {b}paradoja{/b}, la experiencia práctica nos indica que {b}Aquiles{/b} terminará alcanzando a la tortuga tarde o temprano."
    professor2 "Del mismo modo, no hay un único camino para resolver esta aparente contradicción."
    professor2 "¿Qué podemos hacer con los conocimientos de los que disponemos?"

    menu:
        with dissolve
        "¡Afrotarla desde el punto de vista del MRU!":
            professor2 "¡Efectivamente!"
        "...":
            professor2 "Igual que en la situación anterior, podemos utilizar lo que sabemos sobre el {b}MRU{/b}."

    professor2 "Os voy a plantear el siguiente caso."
    professor2 "Es un poco más largo que el anterior. Se compone de tres partes."
    hide ch_professor_smile
    show ch_professor_grin at left_resize
    professor2 "A ver de lo que sois capaces..."

    pause 0.5
    hide ch_professor_grin with dissolve
    pause 0.5
    scene black with dissolve
    stop music fadeout 2

    pause 3

    play sound "audio/celesta.ogg"
    $ first_block_case02 = "Dividir un caso complejo en una serie de pequeños casos más simples facilita mucho la resolución del problema"
    $ second_block_case02 = "Utiliza tus conocimientos sobre el movimiento rectilíneo uniforme (MRU) para resolver la paradoja de Aquiles y la tortuga"
    show screen case_cover(2, first_block_case02, second_block_case02)

    pause
    hide screen case_cover with dissolve

    scene black with dissolve

    play music ["<silence 1.0>", "audio/puzzle.ogg"] volume 1

    pause 2.5

    define case02_01_desc = "Aquiles, héroe de la guerra de Troya y protagonista de la Ilíada de Homero, disputa una carrera contra una tortuga. Ambos parten a velocidades diferentes pero constantes, comenzando la tortuga desde una posición más avanzada. Si Aquiles necesita un tiempo de 9 segundos para recorrer 70 metros, ¿cuál es la velocidad de nuestro guerrero? Exprésala en kilómetros por hora."

    python:
        if persistent.font_label == "global":
            background_case02_01_path = "images/backgrounds/bg_case02-01_alt.png"
        else:
            background_case02_01_path = "images/backgrounds/bg_case02-01.png"

    call screen case(bg_case=background_case02_01_path,
        case_number="2a", true_answer=28, case_desc=case02_01_desc) with dissolve

    stop music

    play sound "audio/correct_answer.ogg"

    $ renpy.pause(3, hard=True)

    play music "audio/turtle.ogg" volume 0.7
    pause 1
    show ch_professor_grin at center_resize with dissolve

    professor "Excelente, {b}[player_name]{/b}."

    pause 1
    hide ch_professor_grin with dissolve
    pause 1
    scene black with dissolve
    pause 1
    scene bg_blackboard with dissolve
    pause 1

    professor "Antes de continuar con la segunda parte del caso, voy a mostraros cómo lo habría resuelto yo."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case02-01_solution_1_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case02-01_solution_1", at_list=[scene_delay])
    professor2 "En primer lugar, escribo la información de la que dispongo para tenerla bien a la vista."
    professor2 "El subíndice A indica que las magnitudes corresponden al movimiento de {b}Aquiles{/b}."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case02-01_solution_2_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case02-01_solution_2", at_list=[scene_delay])
    professor2 "Para calcular la velocidad, aplico la ecuación del {b}MRU{/b} y despejo la incógnita."
    professor2 "Las unidades con las que trabajo ya son las del {b}Sistema Internacional{/b}, de modo que no tengo que preocuparme por ellas."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case02-01_solution_3_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case02-01_solution_3", at_list=[scene_delay])
    professor2 "Por último, expreso el resultado final en las unidades solicitadas."

    show ch_professor_grin at center_resize with dissolve

    professor2 "Fácil, ¿verdad?"
    professor2 "Muy bien, es el momento de continuar con la segunda de las tres partes que componen este caso."

    pause 0.5
    hide ch_professor_grin with dissolve
    pause 0.5

    stop music fadeout 2
    scene black with dissolve

    pause 2

    play music ["<silence 1.0>", "audio/puzzle.ogg"] volume 1

    pause 2

    define case02_02_desc = "Aquiles, héroe de la guerra de Troya y protagonista de la Ilíada de Homero, disputa una carrera contra una tortuga. Ambos parten a velocidades diferentes pero constantes, comenzando la tortuga desde una posición más avanzada. Si la tortuga se desplaza con una velocidad de 1 kilómetro por hora, ¿qué distancia recorrerá en los mismos 9 segundos en los que Aquiles recorre 70 metros? Exprésala en metros."

    python:
        if persistent.font_label == "global":
            background_case02_02_path = "images/backgrounds/bg_case02-02_alt.png"
        else:
            background_case02_02_path = "images/backgrounds/bg_case02-02.png"

    call screen case(bg_case=background_case02_02_path,
        case_number="2b", true_answer=2.5, case_desc=case02_02_desc) with dissolve

    stop music

    play sound "audio/correct_answer.ogg"

    $ renpy.pause(3, hard=True)

    play music "audio/turtle.ogg" volume 0.7

    pause 1
    show ch_professor_grin at center_resize
    with dissolve
    professor "Bien hecho, {b}[player_name]{/b}."

    pause 1

    hide ch_professor_grin with dissolve

    pause 1

    scene black with dissolve

    pause 1

    scene bg_blackboard with dissolve

    pause 1

    professor "De nuevo, voy a mostraros cómo habría procedido yo antes de presentaros la última parte del caso."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case02-02_solution_1_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case02-02_solution_1", at_list=[scene_delay])
    professor2 "Como siempre, escribo los datos de los que dispongo en primer lugar."
    professor2 "En este caso, el subíndice T indica que las magnitudes corresponden al movimiento de la tortuga."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case02-02_solution_2_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case02-02_solution_2", at_list=[scene_delay])
    professor2 "Recordad la importancia de expresar las magnitudes en unidades del {b}Sistema Internacional{/b} para trabajar con las ecuaciones."
    professor2 "En esta ocasión, hay que hacerlo con la velocidad."
    professor2 "No es estrictamente necesario, pero puede ahorrarnos errores en la resolución de un caso que son fácilmente evitables."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case02-02_solution_3_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case02-02_solution_3", at_list=[scene_delay])
    professor2 "Por último, aplico la ecuación del {b}MRU{/b} para calcular la distancia que recorre la tortuga."
    professor2 "¡Qué lenta es!"

    pause 1

    show ch_professor_grin at center_resize with dissolve
    professor2 "Bien, ha llegado el momento."
    professor2 "A continuación, os presentaré la última parte del caso."
    professor2 "Esta parte solo podrá ser resuelta por aquellas personas que dominen al completo la teoría del {b}MRU{/b}."
    professor2 "Quienes sean capaces de superar esta prueba, estarán avanzando a pasos agigantados hacia la meta de convertirse en detectives especialistas en {b}cinemática{/b}."

    pause 0.5
    hide ch_professor_grin with dissolve
    pause 0.5

    stop music fadeout 2
    scene black with dissolve

    pause 2

    play music ["<silence 1.0>", "audio/puzzle.ogg"] volume 1

    pause 2

    define case02_03_desc = "Aquiles, héroe de la guerra de Troya y protagonista de la Ilíada de Homero, disputa una carrera contra una tortuga. Ambos parten a velocidades diferentes pero constantes, comenzando la tortuga desde una posición más avanzada. En el instante inicial, la tortuga se encuentra 150 metros por delante de Aquiles. Teniendo en cuenta las velocidades a las que se mueven ambos, ¿qué distancia debe recorrer Aquiles para alcanzar al animal? Exprésala en metros."

    python:
        if persistent.font_label == "global":
            background_case02_03_path = "images/backgrounds/bg_case02-03_alt.png"
        else:
            background_case02_03_path = "images/backgrounds/bg_case02-03.png"

    call screen case(bg_case=background_case02_03_path,
        case_number="2c", true_answer=155.6, case_desc=case02_03_desc) with dissolve

    stop music

    play sound "audio/correct_answer.ogg"

    $ renpy.pause(3, hard=True)

    play music "audio/celebration.ogg" noloop
    image celebration = Movie(play="videos/confetti.webm", size=(1920, 1080), loop=False)
    show celebration with flash_shorter

    pause 1

    show ch_professor_happy at center_resize
    with dissolve
    professor "¡Es correcto! ¡Estoy muy impresionada, {b}[player_name]{/b}!"

    pause 0.5
    hide ch_professor_happy with dissolve
    pause 0.5
    scene black with dissolve

    play music "audio/turtle.ogg" volume 0.7

    pause 1
    scene bg_blackboard with dissolve
    pause 1

    professor "Para terminar, os mostraré como lo habría resuelto yo."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case02-03_solution_1_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case02-03_solution_1", at_list=[scene_delay])
    professor2 "Como de costumbre, escribo los datos del caso en primer lugar."

    professor2 "Vamos a trabajar a la vez con ecuaciones que corresponden a dos móviles diferentes: {b}Aquiles{/b} y la tortuga."
    professor2 "Por esta razón, utilizo los mismos subíndices A y T empleados en las partes anteriores del caso."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case02-03_solution_2_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case02-03_solution_2", at_list=[scene_delay])
    professor2 "Hay que trabajar con este sistema de ecuaciones."
    professor2 "La primera de las ecuaciones corresponde al movimiento de {b}Aquiles{/b}, mientras que la segunda es para la tortuga."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case02-03_solution_3_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case02-03_solution_3", at_list=[scene_delay])
    professor2 "En el momento en el que el guerrero alcance a la tortuga, sus posiciones serán las mismas."
    professor2 "Por esta razón igualo las posiciones."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case02-03_solution_4_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case02-03_solution_4", at_list=[scene_delay])
    professor2 "A partir de esta igualdad puedo calcular el tiempo que tarda {b}Aquiles{/b} en alcanzar a la tortuga."
    professor2 "El tiempo podré utilizarlo ahora para calcular la posición que queremos conocer."
    python:
        if persistent.font_label == "global":
            renpy.show("bg_case02-03_solution_5_alt", at_list=[scene_delay])
        else:
            renpy.show("bg_case02-03_solution_5", at_list=[scene_delay])
    professor2 "Como las posiciones son iguales en el momento del alcance, puedo utilizar tanto la ecuación de {b}Aquiles{/b} como la de la tortuga."
    professor2 "En esta resolución he optado por la de {b}Aquiles{/b}."

    pause 1

    show ch_professor_happy at center_resize with dissolve
    professor2 "{b}Aquiles{/b} podrá alcanzar a la tortuga, no hay duda alguna."
    professor2 "Esto sucederá siempre y cuando {b}Aquiles{/b} lleve una velocidad mayor, tal y como ocurre en este caso."

    pause 1

    hide ch_professor_happy
    show ch_professor_smile at center_resize

    professor2 "¡Habéis demostrado dominar la teoría del {b}MRU{/b}!"

    hide ch_professor_smile
    show ch_professor_grin at center_resize
    professor2 "Estáis en el camino correcto, ¡seguid así!"

    pause 1

    hide ch_professor_grin with dissolve

    pause 1

    stop music fadeout 2
    scene black with dissolve

    pause 3

    return

