label chapter_01_01:
    stop music
    stop sound
    stop ambient

    scene black

    play sound "audio/celesta.ogg"
    
    $ chapter01_desc = "Sigue a la profesora Conan a lo largo de un viaje por la Antigua Grecia. Resuelve los casos que te plantee utilizando tus conocimientos sobre movimiento rectilíneo uniforme (MRU)."

    show screen chapter_cover("1", "Entre paradojas", chapter01_desc)

    pause
    hide screen chapter_cover with dissolve

    scene bg_black with dissolve

    play music "audio/atenas.ogg" volume 1.25

    pause 2

    professor "Atenas, siglo IV a. C."

    scene bg_acropolis with dissolve

    pause 1

    show ch_professor_smile at left_resize with dissolve

    professor2 "En los jardines de Academo, más allá de los muros de la ciudad, se sitúa uno de los lugares en los que se concentra gran parte del saber de la época."
    professor2 "Rodeada de olivos y plátanos, junto a un gimnasio dedicado al prestigioso héroe Academo, se encuentra la {b}Academia{/b}."

    pause 0.5
    hide ch_professor_smile with dissolve
    pause 0.5
    scene black with dissolve
    pause 0.5
    scene bg_scuola with dissolve
    pause 0.5

    show ch_professor_smile at right_resize with dissolve

    professor2 "La {b}Academia{/b} es la escuela filosófica fundada por {b}Platón{/b}, el filósofo griego, a la edad de 40 años."
    professor2 "Las enseñanzas que allí se impartían estaban centradas en materias como la filosofía, las matemáticas o la astronomía."
    professor2 "La {b}Academia{/b} se consideraba, sobre todo, un lugar para formar a políticos profesionales."
    professor2 "Fue allí donde estudió uno de los discípulos más importantes de {b}Platón{/b}."
    professor2 "Alguien que iba a llevar a cabo numerosos estudios en muchas disciplinas diferentes, como la biología, la zoología, las matemáticas o..."
    professor2 "¡La {b}física{/b}!"
    professor2 "Os hablo de {b}Aristóteles{/b}."
    professor2 "Seguro que sus nombre os suena asociado principalmente a la filosofía."

    pause 0.5
    hide ch_professor_smile with dissolve
    pause 0.5
    scene bg_black with dissolve
    pause 1
    scene bg_aristotle_rembrandt at slowzoom with dissolve

    professor2 "Sus tan diversos aportes al conocimiento le permitieron labrarse un enorme prestigio en la época."
    professor2 "¡Incluso llegó a convertirse en maestro del mismísimo Alejandro Magno!"
    professor2 "Una de sus obras más reconocidas es {b}{i}Física o Lecciones orales sobre la naturaleza{/i}{/b}."
    professor2 "Es considerada como uno de los tratados precursores de lo que hoy conocemos como {b}física{/b}."
    
    show ch_professor_smile at center_resize with dissolve

    pause 0.5

    professor2 "La {b}física{/b}, como toda ciencia, es una disciplina que ha ido experimentando continuos cambios a lo largo del tiempo."
    professor2 "Durante el transcurso de la historia, las investigaciones en el campo de la {b}física{/b} han ido dando a luz a nuevas hipótesis y teorías."
    professor2 "En ocasiones, estas nuevas propuestas han extendido o complementado el conocimiento del que se disponía entonces."
    professor2 "Otras veces, este conocimiento previo ha sido directamente sustituido por nuevas interpretaciones capaces de explicar mejor los sucesos del mundo físico."
    professor2 "Teniendo esto en cuenta, alguien podría preguntarse qué queda de la {b}física{/b} de {b}Aristóteles{/b} en nuestra ciencia actual."
    professor2 "¿En qué consistía la {b}física aristotélica{/b}?{w} ¿Es realmente tan diferente a la nuestra?{w} Y si lo es, ¿hasta cuándo estuvo vigente?"

    hide ch_professor_smile
    show ch_professor_grin at center_resize

    professor2 "Alguien que aspire a convertirse en detective debería ser capaz de encontrar la respuesta a estas preguntas."

    pause 1

    window hide dissolve
    hide ch_professor_grin
    with dissolve

    pause 1

    scene bg_black with dissolve

    stop music fadeout 2

    pause 2

    define first_block_research_01 = "El trabajo de detective implica investigar, analizar y establecer conclusiones sobre los enigmas ante los que nos encontramos"
    define second_block_research_01 = "¿En qué consistió la física aristotélica?\n¿Qué diferencias puedes encontrar con la física actual?\n¿Hasta cuándo estuvo vigente?"
    define third_block_research_01 = "Indaga sobre estas cuestiones y recopila información que consideres relevante"
    play sound "audio/celesta.ogg"
    show screen research_work_cover(first_block_research_01, second_block_research_01, third_block_research_01)

    pause
    hide screen research_work_cover with dissolve

    scene bg_black with dissolve

    pause 2

    return
