﻿
# Sonido de máquina de escribir
define type_sounds = ['audio/typewriter/long.ogg',
    'audio/typewriter/medium.ogg','audio/typewriter/medium.ogg','audio/typewriter/medium.ogg',
    'audio/typewriter/short.ogg','audio/typewriter/short.ogg','audio/typewriter/short.ogg',
    'audio/typewriter/short.ogg','audio/typewriter/short.ogg','audio/typewriter/short.ogg',
    'audio/typewriter/short.ogg','audio/typewriter/short.ogg','audio/typewriter/short.ogg']

init python:
    # Sonido de máquina de escribir
    renpy.music.register_channel("typewriter", "sfx")

    def play_type_sound(event, interact=True, **kwargs):
        if interact is not True:
            return

        if event == "show":
            renpy.music.play(renpy.random.choice(type_sounds), channel="typewriter")
            for _ in range(100):
                renpy.music.queue(renpy.random.choice(type_sounds), channel="typewriter")
        elif event == "slow_done" or event == "end":
            renpy.music.stop(channel="typewriter")

    # Se define un canal de audio para sonido ambiente
    renpy.music.register_channel("ambient", "sfx", loop=True, tight=True)

    # Se especifican las fuentes para los casos negrita y cursiva
    config.font_replacement_map["CourierPrime-Regular.ttf", False, True] = ("CourierPrime-Italic.ttf", False, False)
    config.font_replacement_map["CourierPrime-Regular.ttf", True, False] = ("CourierPrime-Bold.ttf", False, False)
    config.font_replacement_map["CourierPrime-Regular.ttf", True, True] = ("CourierPrime-BoldItalic.ttf", False, False)

# Personajes
define student_name = "Lucas"
define professor_name_1 = "Profesora Conan"
define professor_name_2 = "Irene Conan"
define academy_name = "Doyle"
define student = Character("[student_name]", callback=play_type_sound)
define student2 = Character("{noalt}[student_name]{/noalt}", callback=play_type_sound)
define professor = Character("[professor_name_1]", callback=play_type_sound)
define professor2 = Character("{noalt}[professor_name_1]{/noalt}", callback=play_type_sound)
define unknown = Character("Desconocido", callback=play_type_sound)
define unknown2 = Character("{noalt}Desconocido{/noalt}", callback=play_type_sound)
define unknown3 = Character("{noalt}¿?{/noalt}", callback=play_type_sound)
define classmates = Character("Clase", callback=play_type_sound)
define narrator = Character("", callback=play_type_sound)

transform my_left:
    xalign .1 yalign 1.0

transform my_right:
    xalign .9 yalign 1.0

transform center_resize: 
    zoom 0.75 #adjust as required
    center

transform left_resize: 
    zoom 0.75 #adjust as required
    my_left

transform right_resize: 
    zoom 0.75 #adjust as required
    my_right

transform slowzoom:
     xalign 0.5
     yalign 0.5
     zoom 1.2
     linear 20.0 zoom 1

transform slowzoom_to_left:
     xalign 0.7
     yalign 0.5
     zoom 1.3
     linear 40.0 zoom 1 xalign 0.5

transform vertical_panning:
    xalign 0.5
    yalign 0.5
    linear 15.0 yalign 0.35

transform vertical_panning2:
    xalign 0.5
    yalign 0.3
    linear 15.0 yalign 0.0

transform scene_delay:
    alpha 0.0
    pause 1.0
    linear 0.5 alpha 1.0

transform scene_delay_pos1:
    zoom 0.8
    xalign 0.65
    yalign 0.1
    alpha 0.0
    pause 1.0
    linear 0.5 alpha 1.0

transform scene_delay_pos2:
    zoom 0.8
    xalign 0.65
    yalign 0.58
    alpha 0.0
    pause 1.0
    linear 0.5 alpha 1.0

transform scene_delay_pos3:
    zoom 0.7
    xalign 0.55
    yalign 0.25
    alpha 0.0
    pause 1.0
    linear 0.5 alpha 1.0

transform scene_delay2:
    alpha 0.0
    pause 3.0
    linear 0.5 alpha 1.0

transform scene_delay3:
    alpha 0.0
    pause 5.0
    linear 0.5 alpha 1.0

transform achilles_pos1:
    xpos 325
    ypos 410

transform achilles_pos2:
    xpos 800
    ypos 410

transform achilles_pos3:
    xpos 1000
    ypos 410

transform turtle_pos1:
    xpos 1000
    ypos 500

transform turtle_pos2:
    xpos 1200
    ypos 500

transform turtle_pos3:
    xpos 1280
    ypos 500

transform achilles_pos1_to_pos2:
    achilles_pos1
    pause 1
    linear 1.5 achilles_pos2

transform achilles_pos2_to_pos3:
    achilles_pos2
    pause 1
    linear 1 achilles_pos3

transform turtle_pos1_to_pos2:
    turtle_pos1
    pause 1
    linear 1.5 turtle_pos2

transform turtle_pos2_to_pos3:
    turtle_pos2
    pause 1
    linear 1 turtle_pos3


transform spotlight_search:
    xalign 0.8
    yalign 0.4
    pause 1 
    easein 0.5 xalign 0.7 yalign 0.9
    pause 1 
    easein 1.5 xalign 0.6 yalign 0.8
    pause 0.5
    easein 1 xalign 0.4 yalign 0.77
    pause 0.5
    easein 1.5 xalign 0.35 yalign 0.7
    easein 0.1 xalign 0.6 yalign 0.4
    pause 2
    easein 2.5 xalign 0.5 yalign 0.5

transform from_right_to_center:
    zoom 0.75
    my_right
    linear 1 xalign 0.5

# Pantalla de presentación
image splash_image_01 = "images/splash/splash_01.png"
image splash_image_02 = "images/splash/splash_02.png"
image menu_background_01 = "images/splash/menu_background_01.png"
image menu_background_02 = "images/splash/menu_background_02.png"
image spotlight = "images/splash/spotlight.png"
define flash = Fade(.1, 0.5, 1.5, color="#fff")
define flash_shorter = Fade(.1, 0.0, 1.0, color="#fff")

label splashscreen:
    $ _skipping = False
    scene black

    $ renpy.pause(1.5, hard=True)

    play music "audio/intro_accesibility.ogg"
    call screen accessibility with dissolve
    stop music

    $ renpy.pause(2, hard=True)

    scene splash_image_01
    play sound "audio/splash_screen.ogg" volume 1.2
    $ renpy.pause(1, hard=True)
    show screen skip_splashscreen with dissolve
    $ renpy.pause(2.5, hard=True)
    scene splash_image_02 with dissolve
    $ renpy.pause(3, hard=True)

    scene black with dissolve

    $ renpy.pause(2, hard=True)

    scene menu_background_01
    play sound "audio/light_switch.ogg"
    show spotlight at spotlight_search with hpunch

    $ renpy.pause(12.5, hard=True)

    play sound "audio/spotlight.ogg"
    hide screen skip_splashscreen
    scene menu_background_01 with flash

    $ renpy.pause(0.25, hard=True)

    scene menu_background_02 with dissolve

    return

label before_main_menu:
    stop sound 
    return

# Comienza el videojuego
label start:
    $ _skipping = True
    call preamble from _call_preamble
    call chapter_01_01 from _call_chapter_01_01
    call chapter_01_02 from _call_chapter_01_02
    call chapter_01_03 from _call_chapter_01_03
    call chapter_02_01 from _call_chapter_02_01
    call chapter_02_02 from _call_chapter_02_02
    call chapter_02_03 from _call_chapter_02_03
    call chapter_02_04 from _call_chapter_02_04
    call chapter_03_01 from _call_chapter_03_01
    call chapter_03_02 from _call_chapter_03_02
    call chapter_03_03 from _call_chapter_03_03
    call ending from _call_ending

# Finaliza el juego:
label end:
    return

